package com.navimatics.base;

import static org.junit.Assert.*;
import java.util.Arrays;
import java.util.List;
import java.util.Set;
import org.junit.Test;

public class NativeSetTest
{
    @Test
    public void testNativeSet()
    {
        new NativeSet();
        System.gc();
    }
    @Test
    public void testNativeSetCollectionOfQextendsNativeObject()
    {
        // TODO
    }
    @Test
    public void testAdd()
    {
        NativeObject obj0 = new NativeObject();
        NativeObject obj1 = new NativeObject();
        NativeObject obj2 = new NativeObject();
        Set s = new NativeSet();
        try
        {
            s.add(null);
            fail();
        }
        catch (NullPointerException ex)
        {
        }
        assertEquals(0, s.size());
        assertTrue(s.add(obj0));
        assertTrue(s.add(obj1));
        assertFalse(s.add(obj1));
        assertEquals(2, s.size());
        assertTrue(s.contains(obj0));
        assertTrue(s.contains(obj1));
        assertFalse(s.contains(obj2));
        s.add(obj2);
        assertEquals(3, s.size());
        assertTrue(s.contains(obj2));
    }
    @Test
    public void testAddAll()
    {
        // TODO
    }
    @Test
    public void testClear()
    {
        NativeObject obj0 = new NativeObject();
        NativeObject obj1 = new NativeObject();
        Set s = new NativeSet();
        s.add(obj0);
        s.add(obj1);
        assertEquals(2, s.size());
        s.clear();
        assertEquals(0, s.size());
    }
    @Test
    public void testContains()
    {
        NativeObject obj0 = new NativeObject();
        NativeObject obj1 = new NativeObject();
        NativeObject obj2 = new NativeObject();
        Set s = new NativeSet();
        try
        {
            s.contains(null);
            fail();
        }
        catch (NullPointerException ex)
        {
        }
        s.add(obj0);
        s.add(obj1);
        s.add(obj1);
        assertEquals(2, s.size());
        assertFalse(s.contains(new Object()));
        assertFalse(s.contains(new NativeObject()));
        assertTrue(s.contains(obj0));
        assertTrue(s.contains(obj1));
        assertFalse(s.contains(obj2));
        s.add(obj2);
        assertTrue(s.contains(obj2));
    }
    @Test
    public void testContainsAll()
    {
        // TODO
    }
    @Test
    public void testIsEmpty()
    {
        // TODO
    }
    @Test
    public void testIterator()
    {
        // TODO
    }
    @Test
    public void testRemove()
    {
        NativeObject obj0 = new NativeObject();
        NativeObject obj1 = new NativeObject();
        NativeObject obj2 = new NativeObject();
        Set s = new NativeSet();
        try
        {
            s.remove(null);
            fail();
        }
        catch (NullPointerException ex)
        {
        }
        s.add(obj0);
        s.add(obj1);
        s.add(obj2);
        assertEquals(3, s.size());
        assertFalse(s.remove(new Object()));
        assertFalse(s.remove(new NativeObject()));
        assertTrue(s.remove(obj0));
        assertTrue(s.remove(obj1));
        assertTrue(s.remove(obj2));
        assertEquals(0, s.size());
    }
    @Test
    public void testRemoveAll()
    {
        // TODO
    }
    @Test
    public void testRetainAll()
    {
        // TODO
    }
    @Test
    public void testSize()
    {
        NativeObject obj0 = new NativeObject();
        NativeObject obj1 = new NativeObject();
        Set s = new NativeSet();
        assertEquals(0, s.size());
        s.add(obj0);
        s.add(obj1);
        assertEquals(2, s.size());
    }
    @Test
    public void testToArray()
    {
        NativeObject obj0 = new NativeObject();
        NativeObject obj1 = new NativeObject();
        Set s = new NativeSet();
        s.add(obj0);
        s.add(obj1);
        s.add(obj1);
        Object[] a = s.toArray();
        assertEquals(2, a.length);
        List l = Arrays.asList(a);
        assertTrue(l.contains(obj0));
        assertTrue(l.contains(obj1));
    }
    @Test
    public void testToArrayTArray()
    {
        NativeObject obj0 = new NativeObject();
        NativeObject obj1 = new NativeObject();
        Set s = new NativeSet();
        s.add(obj0);
        s.add(obj1);
        s.add(obj1);
        NativeObject[] a = new NativeObject[2];
        a = (NativeObject[])s.toArray(a);
        assertEquals(2, a.length);
        List l = Arrays.asList(a);
        assertTrue(l.contains(obj0));
        assertTrue(l.contains(obj1));
    }
}
