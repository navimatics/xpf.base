package com.navimatics.base;

import static org.junit.Assert.*;
import java.util.List;
import org.junit.Test;

public class NativeArrayTest
{
    @Test
    public void testNativeArray()
    {
        new NativeArray();
        System.gc();
    }
    @Test
    public void testNativeArrayCollectionOfQextendsNativeObject()
    {
        NativeObject obj0 = new NativeObject();
        NativeObject obj1 = new NativeObject();
        List l0 = new NativeArray();
        l0.add(obj0);
        l0.add(obj1);
        List l1 = new NativeArray(l0);
        assertEquals(2, l1.size());
        assertEquals(obj0, l1.get(0));
        assertEquals(obj1, l1.get(1));
    }
    @Test
    public void testAddNativeObject()
    {
        NativeObject obj0 = new NativeObject();
        NativeObject obj1 = new NativeObject();
        List l = new NativeArray();
        l.add(obj0);
        l.add(obj1);
        assertEquals(2, l.size());
        assertEquals(obj0, l.get(0));
        assertEquals(obj1, l.get(1));
    }
    @Test
    public void testAddIntNativeObject()
    {
        NativeObject obj0 = new NativeObject();
        NativeObject obj1 = new NativeObject();
        List l = new NativeArray();
        try
        {
            l.add(-1, obj0);
            fail();
        }
        catch (IndexOutOfBoundsException ex)
        {
        }
        try
        {
            l.add(1, obj0);
            fail();
        }
        catch (IndexOutOfBoundsException ex)
        {
        }
        l.add(0, obj0);
        l.add(0, obj1);
        assertEquals(2, l.size());
        assertEquals(obj0, l.get(1));
        assertEquals(obj1, l.get(0));
    }
    @Test
    public void testAddAllCollectionOfQextendsNativeObject()
    {
        NativeObject obj0 = new NativeObject();
        NativeObject obj1 = new NativeObject();
        NativeObject obj2 = new NativeObject();
        List l0 = new NativeArray();
        l0.add(obj0);
        l0.add(obj1);
        List l1 = new NativeArray();
        l1.add(obj2);
        l1.addAll(l0);
        assertEquals(3, l1.size());
        assertEquals(obj0, l1.get(1));
        assertEquals(obj1, l1.get(2));
        assertEquals(obj2, l1.get(0));
    }
    @Test
    public void testAddAllIntCollectionOfQextendsNativeObject()
    {
        NativeObject obj0 = new NativeObject();
        NativeObject obj1 = new NativeObject();
        NativeObject obj2 = new NativeObject();
        List l0 = new NativeArray();
        l0.add(obj0);
        l0.add(obj1);
        List l1 = new NativeArray();
        l1.add(obj2);
        l1.addAll(0, l0);
        assertEquals(3, l1.size());
        assertEquals(obj0, l1.get(0));
        assertEquals(obj1, l1.get(1));
        assertEquals(obj2, l1.get(2));
    }
    @Test
    public void testClear()
    {
        NativeObject obj0 = new NativeObject();
        NativeObject obj1 = new NativeObject();
        List l = new NativeArray();
        l.add(obj0);
        l.add(obj1);
        assertEquals(2, l.size());
        l.clear();
        assertEquals(0, l.size());
    }
    @Test
    public void testContains()
    {
        NativeObject obj0 = new NativeObject();
        NativeObject obj1 = new NativeObject();
        List l = new NativeArray();
        l.add(obj0);
        l.add(obj1);
        assertFalse(l.contains(null));
        assertFalse(l.contains(new Object()));
        assertFalse(l.contains(new NativeObject()));
        assertTrue(l.contains(obj0));
        assertTrue(l.contains(obj1));
    }
    @Test
    public void testContainsAll()
    {
        NativeObject obj0 = new NativeObject();
        NativeObject obj1 = new NativeObject();
        List l0 = new NativeArray();
        l0.add(obj0);
        l0.add(obj1);
        List l1 = new NativeArray(l0);
        assertTrue(l0.containsAll(l1));
        l1.add(new NativeObject());
        assertFalse(l0.containsAll(l1));
    }
    @Test
    public void testGet()
    {
        NativeObject obj0 = new NativeObject();
        NativeObject obj1 = new NativeObject();
        List l = new NativeArray();
        try
        {
            l.get(-1);
            fail();
        }
        catch (IndexOutOfBoundsException ex)
        {
        }
        try
        {
            l.get(0);
            fail();
        }
        catch (IndexOutOfBoundsException ex)
        {
        }
        l.add(obj0);
        l.add(obj1);
        assertEquals(2, l.size());
        assertEquals(obj0, l.get(0));
        assertEquals(obj1, l.get(1));
        try
        {
            l.get(2);
            fail();
        }
        catch (IndexOutOfBoundsException ex)
        {
        }
        l.clear();
        try
        {
            l.get(0);
            fail();
        }
        catch (IndexOutOfBoundsException ex)
        {
        }
    }
    @Test
    public void testIndexOf()
    {
        NativeObject obj0 = new NativeObject();
        NativeObject obj1 = new NativeObject();
        List l = new NativeArray();
        l.add(obj0);
        l.add(obj1);
        l.add(obj1);
        assertEquals(-1, l.indexOf(null));
        assertEquals(-1, l.indexOf(new Object()));
        assertEquals(-1, l.indexOf(new NativeObject()));
        assertEquals(0, l.indexOf(obj0));
        assertEquals(1, l.indexOf(obj1));
    }
    @Test
    public void testIsEmpty()
    {
        NativeObject obj0 = new NativeObject();
        NativeObject obj1 = new NativeObject();
        List l = new NativeArray();
        assertTrue(l.isEmpty());
        l.add(obj0);
        l.add(obj1);
        assertFalse(l.isEmpty());
    }
    @Test
    public void testIterator()
    {
        // TODO
    }
    @Test
    public void testLastIndexOf()
    {
        NativeObject obj0 = new NativeObject();
        NativeObject obj1 = new NativeObject();
        List l = new NativeArray();
        l.add(obj0);
        l.add(obj1);
        l.add(obj1);
        assertEquals(-1, l.lastIndexOf(null));
        assertEquals(-1, l.lastIndexOf(new Object()));
        assertEquals(-1, l.lastIndexOf(new NativeObject()));
        assertEquals(0, l.lastIndexOf(obj0));
        assertEquals(2, l.lastIndexOf(obj1));
    }
    @Test
    public void testListIterator()
    {
        // TODO
    }
    @Test
    public void testListIteratorInt()
    {
        // TODO
    }
    @Test
    public void testRemoveInt()
    {
        NativeObject obj0 = new NativeObject();
        NativeObject obj1 = new NativeObject();
        List l = new NativeArray();
        try
        {
            l.remove(-1);
            fail();
        }
        catch (IndexOutOfBoundsException ex)
        {
        }
        try
        {
            l.remove(0);
            fail();
        }
        catch (IndexOutOfBoundsException ex)
        {
        }
        l.add(obj0);
        l.add(obj1);
        try
        {
            l.remove(2);
            fail();
        }
        catch (IndexOutOfBoundsException ex)
        {
        }
        assertEquals(2, l.size());
        assertEquals(obj0, l.get(0));
        assertEquals(obj0, l.remove(0));
        assertEquals(1, l.size());
        assertEquals(obj1, l.get(0));
        assertEquals(obj1, l.remove(0));
        assertEquals(0, l.size());
    }
    @Test
    public void testRemoveObject()
    {
        NativeObject obj0 = new NativeObject();
        NativeObject obj1 = new NativeObject();
        List l = new NativeArray();
        l.add(obj0);
        l.add(obj1);
        l.add(obj1);
        assertFalse(l.remove(null));
        assertFalse(l.remove(new Object()));
        assertFalse(l.remove(new NativeObject()));
        assertTrue(l.remove(obj0));
        assertTrue(l.remove(obj1));
        assertEquals(1, l.size());
        assertTrue(l.remove(obj1));
        assertEquals(0, l.size());
    }
    @Test
    public void testRemoveAll()
    {
        NativeObject obj0 = new NativeObject();
        NativeObject obj1 = new NativeObject();
        List l0 = new NativeArray();
        l0.add(obj0);
        l0.add(obj1);
        l0.add(obj1);
        List l1 = new NativeArray(l0);
        NativeObject obj2 = new NativeObject();
        l0.add(obj2);
        assertTrue(l0.removeAll(l1));
        assertEquals(1, l0.size());
        assertTrue(l0.contains(obj2));
    }
    @Test
    public void testRetainAll()
    {
        // TODO
    }
    @Test
    public void testSet()
    {
        NativeObject obj0 = new NativeObject();
        NativeObject obj1 = new NativeObject();
        List l = new NativeArray();
        try
        {
            l.set(-1, obj0);
            fail();
        }
        catch (IndexOutOfBoundsException ex)
        {
        }
        try
        {
            l.set(0, obj0);
            fail();
        }
        catch (IndexOutOfBoundsException ex)
        {
        }
        l.add(obj0);
        l.add(obj1);
        try
        {
            l.set(2, new NativeObject());
            fail();
        }
        catch (IndexOutOfBoundsException ex)
        {
        }
        assertEquals(2, l.size());
        NativeObject obj2 = new NativeObject();
        NativeObject obj3 = new NativeObject();
        assertEquals(obj0, l.set(0, obj2));
        assertEquals(obj1, l.set(1, obj3));
        assertEquals(2, l.size());
        assertEquals(obj2, l.get(0));
        assertEquals(obj3, l.get(1));
    }
    @Test
    public void testSize()
    {
        NativeObject obj0 = new NativeObject();
        NativeObject obj1 = new NativeObject();
        List l = new NativeArray();
        assertEquals(0, l.size());
        l.add(obj0);
        l.add(obj1);
        assertEquals(2, l.size());
    }
    @Test
    public void testSubList()
    {
        // TODO
    }
    @Test
    public void testToArray()
    {
        NativeObject obj0 = new NativeObject();
        NativeObject obj1 = new NativeObject();
        List l = new NativeArray();
        l.add(obj0);
        l.add(obj1);
        Object[] a = l.toArray();
        assertEquals(2, a.length);
        assertEquals(obj0, a[0]);
        assertEquals(obj1, a[1]);
    }
    @Test
    public void testToArrayTArray()
    {
        NativeObject obj0 = new NativeObject();
        NativeObject obj1 = new NativeObject();
        List l = new NativeArray();
        l.add(obj0);
        l.add(obj1);
        NativeObject[] a = new NativeObject[2];
        a = (NativeObject[])l.toArray(a);
        assertEquals(2, a.length);
        assertEquals(obj0, a[0]);
        assertEquals(obj1, a[1]);
    }
}
