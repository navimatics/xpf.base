/*
 * com/navimatics/base/NativeString.java
 *
 * Copyright 2010-2013 Navimatics Corporation. All rights reserved.
 */

package com.navimatics.base;

public final class NativeString extends NativeObject
{
    public static NativeString create(String s)
    {
        NativeString result = _create(s);
        result._string = s;
        return result;
    }
    protected NativeString(skip_ctor_t s)
    {
        super(s);
    }
    public String toString()
    {
        if (null == _string)
            _string = _toString();
        return _string;
    }
    private static native NativeString _create(String s);
    private native String _toString();
    private String _string;
}
