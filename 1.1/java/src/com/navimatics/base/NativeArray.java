/*
 * com/navimatics/base/NativeArray.java
 *
 * Copyright 2010-2013 Navimatics Corporation. All rights reserved.
 */

package com.navimatics.base;

import java.util.Collection;
import java.util.Iterator;
import java.util.List;
import java.util.ListIterator;
import java.util.NoSuchElementException;
import java.util.RandomAccess;

public class NativeArray<E extends NativeObject> extends NativeObject implements List<E>, RandomAccess
{
    public NativeArray()
    {
        super(skip_ctor);
        _ctor();
    }
    public NativeArray(Collection<? extends E> collection)
    {
        super(skip_ctor);
        _ctor();
        for (E obj : collection)
            add(obj);
    }
    protected NativeArray(skip_ctor_t s)
    {
        super(s);
    }
    public native boolean add(E obj);
    public native void add(int index, E obj);
    public boolean addAll(Collection<? extends E> collection)
    {
        boolean result = false;
        for (E obj : collection)
        {
            result = true;
            add(obj);
        }
        return result;
    }
    public boolean addAll(int index, Collection<? extends E> collection)
    {
        boolean result = false;
        for (E obj : collection)
        {
            result = true;
            add(index, obj);
            index++;
        }
        return result;
    }
    public native void clear();
    public boolean contains(Object obj)
    {
        return -1 != indexOf(obj);
    }
    public boolean containsAll(Collection<?> collection)
    {
        for (Object obj : collection)
            if (-1 == indexOf(obj))
                return false;
        return true;
    }
    public native E get(int index);
    public int indexOf(Object obj)
    {
        return _indexOf(obj, 0, size());
    }
    public boolean isEmpty()
    {
        return 0 == size();
    }
    public Iterator<E> iterator()
    {
        return new _ListIterator<E>(this, 0, size(), 0);
    }
    public int lastIndexOf(Object obj)
    {
        return _lastIndexOf(obj, 0, size());
    }
    public ListIterator<E> listIterator()
    {
        return new _ListIterator<E>(this, 0, size(), 0);
    }
    public ListIterator<E> listIterator(int index)
    {
        return new _ListIterator<E>(this, 0, size(), index);
    }
    public native E remove(int index);
    public boolean remove(Object obj)
    {
        int index = indexOf(obj);
        if (-1 == index)
            return false;
        remove(index);
        return true;
    }
    public boolean removeAll(Collection<?> collection)
    {
        boolean result = false;
        for (Object obj : collection)
        {
            int index = indexOf(obj);
            if (-1 != index)
            {
                result = true;
                remove(index);
            }
        }
        return result;
    }
    public boolean retainAll(Collection<?> collection)
    {
        boolean result = false;
        for (int index = 0, n = size(); n > index; index++)
        {
            E obj = get(index);
            if (!collection.contains(obj))
            {
                result = true;
                remove(index--);
                n--;
            }
        }
        return result;
    }
    public native E set(int index, E obj);
    public native int size();
    public List<E> subList(int start, int end)
    {
        if (0 > start || end > size() || start > end)
            throw new IndexOutOfBoundsException();
        return new _SubList<E>(this, start, end);
    }
    public Object[] toArray()
    {
        return _toArray(null, Object.class, 0, size());
    }
    public <T> T[] toArray(T[] array)
    {
        return (T[])_toArray(array, array.getClass().getComponentType(), 0, size());
    }
    private native int _indexOf(Object obj, int start, int end);
    private native int _lastIndexOf(Object obj, int start, int end);
    private native Object[] _toArray(Object[] array, Class elemCls, int start, int end);
    private native void _ctor();
    private static class _ListIterator<E extends NativeObject> implements ListIterator<E>
    {
        public _ListIterator(NativeArray<E> array, int startIndex, int endIndex, int index)
        {
            if (startIndex > index || index > endIndex)
                throw new IndexOutOfBoundsException();
            _array = array;
            _startIndex = startIndex;
            _endIndex = endIndex;
            _index = index;
            _lastIndex = -1;
        }
        public void add(E obj)
        {
            _array.add(_index, obj);
            _endIndex++;
            _lastIndex = -1;
        }
        public boolean hasNext()
        {
            return _endIndex > _index;
        }
        public boolean hasPrevious()
        {
            return _startIndex < _index;
        }
        public E next()
        {
            if (_endIndex > _index)
                return _array.get(_lastIndex = _index++);
            else
                throw new NoSuchElementException();
        }
        public int nextIndex()
        {
            return _index - _startIndex;
        }
        public E previous()
        {
            if (_startIndex < _index)
                return _array.get(_lastIndex = --_index);
            else
                throw new NoSuchElementException();
        }
        public int previousIndex()
        {
            return _index - 1 - _startIndex;
        }
        public void remove()
        {
            if (0 > _lastIndex)
                throw new IllegalStateException();
            _array.remove(_lastIndex);
            _endIndex--;
            _lastIndex = -1;
        }
        public void set(E obj)
        {
            if (0 > _lastIndex)
                throw new IllegalStateException();
            _array.set(_lastIndex, obj);
        }
        private NativeArray<E> _array;
        private int _startIndex, _endIndex, _index, _lastIndex;
    }
    private static class _SubList<E extends NativeObject> implements List<E>, RandomAccess
    {
        public _SubList(NativeArray<E> array, int startIndex, int endIndex)
        {
            _array = array;
            _startIndex = startIndex;
            _endIndex = endIndex;
        }
        public boolean add(E obj)
        {
            _array.add(_endIndex, obj);
            _endIndex++;
            return true;
        }
        public void add(int index, E obj)
        {
            if (_startIndex > index || index > _endIndex)
                throw new IndexOutOfBoundsException();
            _array.add(_startIndex + index, obj);
            _endIndex++;
        }
        public boolean addAll(Collection<? extends E> collection)
        {
            boolean result = _array.addAll(_endIndex, collection);
            _endIndex += collection.size();
            return result;
        }
        public boolean addAll(int index, Collection<? extends E> collection)
        {
            if (_startIndex > index || index > _endIndex)
                throw new IndexOutOfBoundsException();
            boolean result = _array.addAll(_startIndex + index, collection);
            _endIndex += collection.size();
            return result;
        }
        public void clear()
        {
            for (int index = 0, n = size(); n > index; index++)
                remove(index);
        }
        public boolean contains(Object obj)
        {
            return -1 == indexOf(obj);
        }
        public boolean containsAll(Collection<?> collection)
        {
            for (Object obj : collection)
                if (-1 == indexOf(obj))
                    return false;
            return true;
        }
        public E get(int index)
        {
            if (_startIndex > index || index >= _endIndex)
                throw new IndexOutOfBoundsException();
            return _array.get(_startIndex + index);
        }
        public int indexOf(Object obj)
        {
            int index = _array._indexOf(obj, _startIndex, _endIndex);
            if (-1 == index)
                return -1;
            return index - _startIndex;
        }
        public boolean isEmpty()
        {
            return 0 == size();
        }
        public Iterator<E> iterator()
        {
            return new _ListIterator<E>(_array, _startIndex, _endIndex, _startIndex);
        }
        public int lastIndexOf(Object obj)
        {
            int index = _array._lastIndexOf(obj, _startIndex, _endIndex);
            if (-1 == index)
                return -1;
            return index - _startIndex;
        }
        public ListIterator<E> listIterator()
        {
            return new _ListIterator<E>(_array, _startIndex, _endIndex, _startIndex);
        }
        public ListIterator<E> listIterator(int index)
        {
            return new _ListIterator<E>(_array, _startIndex, _endIndex, _startIndex + index);
        }
        public E remove(int index)
        {
            if (_startIndex > index || index >= _endIndex)
                throw new IndexOutOfBoundsException();
            E result = _array.remove(_startIndex + index);
            _endIndex--;
            return result;
        }
        public boolean remove(Object obj)
        {
            int index = indexOf(obj);
            if (-1 == index)
                return false;
            remove(index);
            return true;
        }
        public boolean removeAll(Collection<?> collection)
        {
            boolean result = false;
            for (Object obj : collection)
            {
                int index = indexOf(obj);
                if (-1 != index)
                {
                    result = true;
                    remove(index);
                }
            }
            return result;
        }
        public boolean retainAll(Collection<?> collection)
        {
            boolean result = false;
            for (int index = 0, n = size(); n > index; index++)
            {
                E obj = get(index);
                if (!collection.contains(obj))
                {
                    result = true;
                    remove(index--);
                    n--;
                }
            }
            return result;
        }
        public E set(int index, E obj)
        {
            if (_startIndex > index || index >= _endIndex)
                throw new IndexOutOfBoundsException();
            return _array.set(_startIndex + index, obj);
        }
        public int size()
        {
            return _endIndex - _startIndex;
        }
        public List<E> subList(int start, int end)
        {
            if (0 > start || end > size() || start > end)
                throw new IndexOutOfBoundsException();
            return new _SubList<E>(_array, _startIndex + start, _startIndex + end);
        }
        public Object[] toArray()
        {
            return _array._toArray(null, Object.class, _startIndex, _endIndex);
        }
        public <T> T[] toArray(T[] array)
        {
            return (T[])_array._toArray(array, array.getClass().getComponentType(), _startIndex, _endIndex);
        }
        private NativeArray<E> _array;
        private int _startIndex, _endIndex;
    }
}
