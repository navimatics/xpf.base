/*
 * com/navimatics/base/DateTime.java
 *
 * Copyright 2010-2013 Navimatics Corporation. All rights reserved.
 */

package com.navimatics.base;

public class DateTime extends NativeObject
{
    public static final int Sunday = 0;
    public static final int Monday = 1;
    public static final int Tuesday = 2;
    public static final int Wednesday = 3;
    public static final int Thursday = 4;
    public static final int Friday = 5;
    public static final int Saturday = 6;
    public static class Components
    {
        public int year, month, day;
        public int hour, min, sec, msec;
        public int dayOfWeek;           /* output only */
    }
    public DateTime()
    {
        super(skip_ctor);
        _ctor();
    }
    public DateTime(Components c)
    {
        super(skip_ctor);
        _ctor(c);
    }
    public DateTime(int year, int month, int day)
    {
        super(skip_ctor);
        _ctor(year, month, day, 0, 0, 0, 0);
    }
    public DateTime(int year, int month, int day,
        int hour, int min, int sec)
    {
        super(skip_ctor);
        _ctor(year, month, day, hour, min, sec, 0);
    }
    public DateTime(int year, int month, int day,
        int hour, int min, int sec, int msec)
    {
        super(skip_ctor);
        _ctor(year, month, day, hour, min, sec, msec);
    }
    public DateTime(double julianDate)
    {
        super(skip_ctor);
        _ctor(julianDate);
    }
    public DateTime(long unixTime)
    {
        super(skip_ctor);
        _ctor(unixTime);
    }
    protected DateTime(skip_ctor_t s)
    {
        super(s);
    }
    public native Components components();
    public native double julianDate();
    public native long unixTime();
    private native void _ctor();
    private native void _ctor(Components c);
    private native void _ctor(int year, int month, int day,
        int hour, int min, int sec, int msec);
    private native void _ctor(double julianDate);
    private native void _ctor(long unixTime);
}
