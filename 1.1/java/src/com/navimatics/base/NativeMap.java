/*
 * com/navimatics/base/NativeMap.java
 *
 * Copyright 2010-2013 Navimatics Corporation. All rights reserved.
 */

package com.navimatics.base;

import java.util.Arrays;
import java.util.Collection;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

public class NativeMap<K extends NativeObject, V extends NativeObject> extends NativeObject implements Map<K, V>
{
    public NativeMap()
    {
        super(skip_ctor);
        _ctor();
    }
    public NativeMap(Map<? extends K, ? extends V> map)
    {
        super(skip_ctor);
        _ctor();
        for (Entry<? extends K, ? extends V> entry : map.entrySet())
            put(entry.getKey(), entry.getValue());
    }
    protected NativeMap(skip_ctor_t s)
    {
        super(s);
    }
    public native void clear();
    public native boolean containsKey(Object key);
    public boolean containsValue(Object value)
    {
        V[] values = _values();
        if (null != value)
        {
            for (int i = 0, n = values.length; n > i; i++)
                if (value.equals(values[i]))
                    return true;
        }
        else
        {
            for (int i = 0, n = values.length; n > i; i++)
                if (value == values[i])
                    return true;
        }
        return false;
    }
    public Set<Map.Entry<K, V>> entrySet()
    {
        K[] keys = _keys();
        V[] values = _values();
        Set<Entry<K, V>> set = new HashSet<Entry<K, V>>();
        for (int i = 0, n = keys.length; n > i; i++)
            set.add(new _Entry(keys[i], values[i]));
        return set;
    }
    public native V get(Object key);
    public boolean isEmpty()
    {
        return 0 == size();
    }
    public Set<K> keySet()
    {
        return new HashSet<K>(Arrays.asList(_keys()));
    }
    public native V put(K key, V value);
    public void putAll(Map<? extends K, ? extends V> map)
    {
        for (Entry<? extends K, ? extends V> entry : map.entrySet())
            put(entry.getKey(), entry.getValue());
    }
    public native V remove(Object key);
    public native int size();
    public Collection<V> values()
    {
        return Arrays.asList(_values());
    }
    private native K[] _keys();
    private native V[] _values();
    private native void _ctor();
    private static class _Entry<K extends NativeObject, V extends NativeObject> implements Entry<K, V>
    {
        public _Entry(K key, V value)
        {
            _key = key;
            _value = value;
        }
        public K getKey()
        {
            return _key;
        }
        public V getValue()
        {
            return _value;
        }
        public V setValue(V value)
        {
            throw new UnsupportedOperationException();
        }
        private K _key;
        private V _value;
    }
}
