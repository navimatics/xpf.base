/*
 * com/navimatics/base/NativeObject.java
 *
 * Copyright 2010-2013 Navimatics Corporation. All rights reserved.
 */

package com.navimatics.base;

import java.lang.ref.WeakReference;
import java.lang.ref.ReferenceQueue;

public class NativeObject
{
    /*
     * A NativeObject instance is a peer of a Base::Object instance.
     * There exists a one-to-one mapping between NativeObject and Base::Object
     * called the peer mapping.
     *
     * We have the following requirements:
     *     1. When a NativeObject gets instantiated, the peer Base::Object gets constructed
     * and the peer mapping is updated.
     *     2. When a NativeObject gets collected, the peer Base::Object gets released
     * and the peer mapping is updated.
     *     3. The peer mapping should not inhibit garbage collection of instances of NativeObject.
     *     4. A Base::Object that must be accessed by Java code and does not have
     * a peer NativeObject (either because the peer NativeObject was collected or
     * because there was never a peer NativeObject) will have a new peer NativeObject
     * instantiated and the peer mapping updated.
     *
     * The peer mapping is implemented as follows:
     *     NativeObject->Base::Object
     *         Implemented through NativeObject._object field.
     *     Base::Object->NativeObject
     *         Implemented inside the native runtime using a native (hash) map that
     *         maps Base::Object instances to weak references (requirement 3) of NativeObject.
     * Requirement 4 is implemented by a type mapping that maps C++ types to Java classes.
     *
     * A first attempt to implement requirements 2-3 using finalize() and
     * JNI weak references failed, because JNI weak references can resurrect objects
     * even *after* finalization.
     * See bug 4627651:
     *     http://bugs.sun.com/bugdatabase/view_bug.do?bug_id=4627651
     *
     * Because of this problem we now implement our own finalization technique.
     * We introduce a private class FinalReference which extends WeakReference.
     * There is one and only one instance of FinalReference for every NativeObject.
     * Every FinalReference instance contains an _object field that allows access
     * to the peer Base::Object.
     *
     * Because a FinalReference is a WeakReference it has a number of desirable properties:
     *     1. It is impossible to resurrect an object that has become weakly reachable through it.
     *     2. It is automatically enqueued by the garbage collector to a ReferenceQueue when its
     * referent NativeObject becomes weakly reachable.
     *
     * We also introduce a "NativeObject Finalizer" thread, whose responsibility is to process
     * all FinalReference's enqueued by the garbage collector and release the peer Base::Object's.
     */
    protected static class skip_ctor_t {}
    protected static skip_ctor_t skip_ctor = new skip_ctor_t();
    public NativeObject()
    {
        _ctor();
    }
    protected NativeObject(skip_ctor_t s)
    {
    }
    public native void dispose();
    public native boolean equals(Object obj);
    public native int hashCode();
    public native String toString();
    private native void _ctor();
    private long _object;
    /* finalization */
    private static ReferenceQueue _queue = new ReferenceQueue();
    private static class FinalReference extends WeakReference
    {
        public FinalReference(NativeObject obj)
        {
            super(obj, _queue);
        }
        public native void dispose();
        private long _object;
    }
    private static class Finalizer extends Thread
    {
        public Finalizer(ThreadGroup group)
        {
            super(group, "NativeObject Finalizer");
        }
        public void run()
        {
            for (;;)
                try
                {
                    for (;;)
                    {
                        FinalReference ref = (FinalReference)_queue.remove();
                        ref.dispose();
                        ref = null;
                    }
                }
                catch (Throwable ex)
                {
                }
        }
    }
    static
    {
        try
        {
            System.loadLibrary("gnustl_shared");
        }
        catch (UnsatisfiedLinkError ex)
        {
        }
        System.loadLibrary("Base");
        ThreadGroup group = Thread.currentThread().getThreadGroup();
        for (ThreadGroup next = group;
            null != next;
            group = next, next = group.getParent())
            ;
        Thread finalizer = new Finalizer(group);
        finalizer.setPriority(Thread.MAX_PRIORITY - 2);
        finalizer.setDaemon(true);
        finalizer.start();
    }
}
