/*
 * Base/impl/FileSystemMonitor.cpp
 *
 * Copyright 2010-2015 Navimatics Corporation. All rights reserved.
 */

#include <Base/impl/FileSystemMonitor.hpp>
#include <Base/impl/CArray.hpp>
#include <Base/impl/Error.hpp>
#include <Base/impl/Log.hpp>
#include <Base/impl/Synchronization.hpp>
#include <Base/impl/ThreadLoop.hpp>
#if defined(BASE_CONFIG_WINDOWS)
#include <Base/impl/UString.hpp>
#include <windows.h>
#elif defined(BASE_CONFIG_DARWIN)
#include <CoreServices/CoreServices.h>
#else
#error FileSystemMonitor.cpp not implemented for this platform
#endif

namespace Base {
namespace impl {

class FileSystemMonitorMultiEvent : public Object
{
public:
    FileSystemMonitorMultiEvent(size_t capacity, bool retainPaths);
    ~FileSystemMonitorMultiEvent();
    FileSystemMonitorEvent *events();
    void _addEvent(unsigned action, const char *path);
private:
    bool _retainPaths;
    FileSystemMonitorEvent *_events;
};
FileSystemMonitorMultiEvent::FileSystemMonitorMultiEvent(size_t capacity, bool retainPaths)
{
    threadsafe();
    _retainPaths = retainPaths;
    _events = (FileSystemMonitorEvent *)carr_newWithCapacity(capacity, sizeof(FileSystemMonitorEvent));
    carr_obj(_events)->threadsafe();
}
FileSystemMonitorMultiEvent::~FileSystemMonitorMultiEvent()
{
    if (_retainPaths)
        for (size_t i = 0, n = carr_length(_events); n > i; i++)
            cstr_release(_events[i].path);
    carr_release(_events);
}
FileSystemMonitorEvent *FileSystemMonitorMultiEvent::events()
{
    return _events;
}
void FileSystemMonitorMultiEvent::_addEvent(unsigned action, const char *path)
{
    if (_retainPaths)
    {
        cstr_retain(path);
        cstr_obj(path)->threadsafe();
    }
    FileSystemMonitorEvent event = { action, path };
    _events = (FileSystemMonitorEvent *)carr_concat(_events, &event, 1, sizeof(FileSystemMonitorEvent));
}

class FileSystemMonitorThread : public Thread
{
public:
    FileSystemMonitorThread(FileSystemMonitor::Delegate delegate,
        Thread *eventThread, OrderedSet *paths);
    ~FileSystemMonitorThread();
    void quit();
    Semaphore *semaphone();
    Error *error();
protected:
    Thread *eventThread();
    void triggerEvent(Object *event);
    void run();
    void _quit();
#if defined(BASE_CONFIG_WINDOWS)
    struct MONITOR
    {
        const char *path;
        HANDLE handle;
        LPVOID buffer;
        OVERLAPPED overlapped;
        bool pending;
    };
    bool issueReadDirectoryChanges(MONITOR *monitor);
    void cancelReadDirectoryChanges(MONITOR *monitor);
    static VOID CALLBACK FileIOCompletionRoutine(
        DWORD errorCode, DWORD bytesTransfered, LPOVERLAPPED overlapped);
#elif defined(BASE_CONFIG_DARWIN)
    static void threadLoopStop(Object *obj);
    static void FSEventStreamCallback(ConstFSEventStreamRef stream, void *info,
        size_t numEvents, void *eventPaths, const FSEventStreamEventFlags eventFlags[],
        const FSEventStreamEventId eventIds[]);
#endif
private:
    FileSystemMonitor::Delegate _delegate;
    Thread *_eventThread;
    OrderedSet *_paths;
    Semaphore *_semaphone;
    Error *_error;
};
FileSystemMonitorThread::FileSystemMonitorThread(FileSystemMonitor::Delegate delegate,
    Thread *eventThread, OrderedSet *paths)
{
    _delegate = delegate;
    if (0 != eventThread)
    {
        eventThread->retain();
        _eventThread = eventThread;
    }
    _paths = new OrderedSet;
    _paths->addObjects(paths);
    _semaphone = new Semaphore(0);
}
FileSystemMonitorThread::~FileSystemMonitorThread()
{
    if (0 != _error)
        _error->release();
    _semaphone->release();
    _paths->release();
    if (0 != _eventThread)
        _eventThread->release();
}
void FileSystemMonitorThread::quit()
{
    Thread::quit();
    _quit();
    if (0 != _eventThread)
        ThreadLoopCancelInvoke(_eventThread, make_delegate(&FileSystemMonitorThread::triggerEvent, this));
}
Semaphore *FileSystemMonitorThread::semaphone()
{
    return _semaphone;
}
Error *FileSystemMonitorThread::error()
{
    return _error;
}
Thread *FileSystemMonitorThread::eventThread()
{
    return 0 != _eventThread && Thread::currentThread() != _eventThread ? _eventThread : 0;
}
void FileSystemMonitorThread::triggerEvent(Object *event)
{
    if (isQuitting())
        return;
    if (Thread *t = eventThread())
    {
        if (!ThreadLoopInvoke(t, make_delegate(&FileSystemMonitorThread::triggerEvent, this), event))
            LOG("no thread loop on thread %p; event will not be delivered", t);
    }
    else if (_delegate)
        _delegate(((FileSystemMonitorMultiEvent *)event)->events());
}
#if defined(BASE_CONFIG_WINDOWS)
#define MONITOR_BUFFER_SIZE             16384
void FileSystemMonitorThread::run()
{
    const char *msg = 0;
    MONITOR *monitors = 0;
    size_t i = 0, n = _paths->count();
    if (0 == n)
    {
        msg = "No paths to monitor";
        goto end;
    }
    monitors = (MONITOR *)calloc(n, sizeof monitors[0]);
    if (0 == monitors)
    {
        msg = "Cannot allocate memory";
        goto end;
    }
    foreach (CString *path, _paths)
    {
        monitors[i].path = path->chars();
        monitors[i].handle = CreateFileA(
            monitors[i].path,
            FILE_LIST_DIRECTORY,
            FILE_SHARE_READ | FILE_SHARE_WRITE | FILE_SHARE_DELETE,
            0,
            OPEN_EXISTING,
            FILE_FLAG_BACKUP_SEMANTICS | FILE_FLAG_OVERLAPPED,
            0);
        monitors[i].buffer = malloc(MONITOR_BUFFER_SIZE);
        monitors[i].overlapped.hEvent = &monitors[i];
        i++;
    }
    for (i = 0; n > i; i++)
    {
        if (INVALID_HANDLE_VALUE == monitors[i].handle)
        {
            msg = "Cannot create directory handle";
            goto end;
        }
        if (0 == monitors[i].buffer)
        {
            msg = "Cannot allocate memory";
            goto end;
        }
    }
    for (i = 0; n > i; i++)
        if (!issueReadDirectoryChanges(&monitors[i]))
        {
            msg = "Cannot read directory changes";
            goto end;
        }
    assert(0 == msg);
    _semaphone->notify();
    while (!isQuitting())
    {
        SleepEx(1000, TRUE);
        for (i = 0; n > i; i++)
            if (!monitors[i].pending)
                /* retry any failed ReadDirectoryChangesW calls */
                issueReadDirectoryChanges(&monitors[i]);
    }
end:
    if (0 != msg)
    {
        assert(0 == _error);
        _error = new Error(FileSystemMonitor::errorDomain(), 0, msg);
        _error->threadsafe();
    }
    _semaphone->notify();
    if (0 != monitors)
    {
        for (i = 0; n > i; i++)
        {
            if (INVALID_HANDLE_VALUE != monitors[i].handle)
                cancelReadDirectoryChanges(&monitors[i]);
        }
        for (i = 0; n > i; i++)
        {
            if (INVALID_HANDLE_VALUE != monitors[i].handle)
                CloseHandle(monitors[i].handle);
            free(monitors[i].buffer);
        }
        free(monitors);
    }
}
void FileSystemMonitorThread::_quit()
{
    _interrupt();
}
bool FileSystemMonitorThread::issueReadDirectoryChanges(MONITOR *monitor)
{
    DWORD bytes = 0;
    return monitor->pending = !!ReadDirectoryChangesW(monitor->handle,
        monitor->buffer,
        MONITOR_BUFFER_SIZE,
        TRUE,
        FILE_NOTIFY_CHANGE_FILE_NAME | FILE_NOTIFY_CHANGE_DIR_NAME |
            FILE_NOTIFY_CHANGE_ATTRIBUTES | FILE_NOTIFY_CHANGE_SIZE |
            FILE_NOTIFY_CHANGE_LAST_WRITE | FILE_NOTIFY_CHANGE_CREATION |
            FILE_NOTIFY_CHANGE_SECURITY,
        &bytes,
        &monitor->overlapped,
        FileIOCompletionRoutine);
}
void FileSystemMonitorThread::cancelReadDirectoryChanges(MONITOR *monitor)
{
    if (CancelIo(monitor->handle))
    {
        DWORD bytes = 0;
        GetOverlappedResult(monitor->handle, &monitor->overlapped, &bytes, TRUE);
    }
}
VOID CALLBACK FileSystemMonitorThread::FileIOCompletionRoutine(
    DWORD errorCode, DWORD bytesTransfered, LPOVERLAPPED overlapped)
{
    if (ERROR_OPERATION_ABORTED == errorCode)
        return;
    mark_and_collect
    {
        FileSystemMonitorThread *self = (FileSystemMonitorThread *)Thread::currentThread();
        FileSystemMonitorMultiEvent *event = new (collect) FileSystemMonitorMultiEvent(16, true);
        unsigned action;
        const char *path;
        MONITOR *monitor = (MONITOR *)overlapped->hEvent;
        if (0 == bytesTransfered)
        {
            action = FileSystemMonitorEvent::Rescan;
            path = monitor->path;
            event->_addEvent(action, path);
            //DEBUGLOG("OV->%02x %s", action, path);
        }
        else
        {
            FILE_NOTIFY_INFORMATION *info = (FILE_NOTIFY_INFORMATION *)monitor->buffer;
            for (;;)
            {
                switch (info->Action)
                {
                case FILE_ACTION_ADDED:
                    action = FileSystemMonitorEvent::Create;
                    break;
                case FILE_ACTION_REMOVED:
                    action = FileSystemMonitorEvent::Remove;
                    break;
                case FILE_ACTION_MODIFIED:
                    action = FileSystemMonitorEvent::Modify;
                    break;
                case FILE_ACTION_RENAMED_OLD_NAME:
                case FILE_ACTION_RENAMED_NEW_NAME:
                default:
                    action = FileSystemMonitorEvent::Rescan;
                    break;
                }
                size_t len = info->FileNameLength / sizeof(WCHAR);
                path = cstr_path_concat(monitor->path,
                    cstr_from_ustr((const unichar_t *)info->FileName, len));
                if (12 >= len)
                    for (DWORD i = 0; len > i; i++)
                        if ('~' == info->FileName[i])
                        {
                            char buf[MAX_PATH];
                            DWORD n = GetLongPathNameA(path, buf, NELEMS(buf));
                            if (0 < n && n < NELEMS(buf))
                                path = cstring(buf);
                            else
                            {
                                /* rescan parent directory? */
                                action = FileSystemMonitorEvent::Rescan;
                                path = cstr_path_setBasename(path, "");
                                if (cstr_length(path) < cstr_length(monitor->path))
                                    path = monitor->path;
                            }
                            break;
                        }
                event->_addEvent(action, path);
                //DEBUGLOG("%02x->%02x %s", info->Action, action, path);
                if (0 == info->NextEntryOffset)
                    break;
                info = (FILE_NOTIFY_INFORMATION *)((uint8_t *)info + info->NextEntryOffset);
            }
        }
        self->issueReadDirectoryChanges(monitor);
        self->triggerEvent(event);
    }
}
#undef MONITOR_BUFFER_SIZE
#elif defined(BASE_CONFIG_DARWIN)
void FileSystemMonitorThread::run()
{
    const char *msg = 0;
    FSEventStreamRef stream = 0;
    size_t n = _paths->count();
    if (0 == n)
    {
        msg = "No paths to monitor";
        goto end;
    }
    mark_and_collect
    {
        if (CFMutableArrayRef cfpaths = CFArrayCreateMutable(0, n, &kCFTypeArrayCallBacks))
        {
            foreach (CString *path, _paths)
            {
                CFStringRef cfstr = CFStringCreateWithCString(0, path->chars(), kCFStringEncodingUTF8);
                if (0 == cfstr)
                    break;
                CFArrayAppendValue(cfpaths, cfstr);
                CFRelease(cfstr);
            }
            if (CFArrayGetCount(cfpaths) == n)
            {
                FSEventStreamContext context = { 0, this };
                stream = FSEventStreamCreate(0, FSEventStreamCallback, &context,
                    cfpaths, kFSEventStreamEventIdSinceNow, 0.1,
                    kFSEventStreamCreateFlagWatchRoot | kFSEventStreamCreateFlagFileEvents);
            }
            CFRelease(cfpaths);
        }
    }
    if (0 == stream)
    {
        msg = "Cannot create FSEventStream";
        goto end;
    }
    FSEventStreamScheduleWithRunLoop(stream, CFRunLoopGetCurrent(), kCFRunLoopDefaultMode);
    if (!FSEventStreamStart(stream))
    {
        msg = "Cannot start FSEventStream";
        goto end;
    }
    assert(0 == msg);
    ThreadLoopInit();
        /* init our thread loop before signaling the start semaphore */
    _semaphone->notify();
    while (!isQuitting())
        ThreadLoopRun();
    FSEventStreamStop(stream);
end:
    if (0 != msg)
    {
        assert(0 == _error);
        _error = new Error(FileSystemMonitor::errorDomain(), 0, msg);
        _error->threadsafe();
    }
    _semaphone->notify();
    if (0 != stream)
    {
        FSEventStreamUnscheduleFromRunLoop(stream, CFRunLoopGetCurrent(), kCFRunLoopDefaultMode);
        FSEventStreamInvalidate(stream);
        FSEventStreamRelease(stream);
    }
}
void FileSystemMonitorThread::_quit()
{
    ThreadLoopInvoke(this, threadLoopStop, 0);
}
void FileSystemMonitorThread::threadLoopStop(Object *obj)
{
    ThreadLoopStop();
}
void FileSystemMonitorThread::FSEventStreamCallback(ConstFSEventStreamRef stream, void *info,
    size_t numEvents, void *eventPaths, const FSEventStreamEventFlags eventFlags[],
    const FSEventStreamEventId eventIds[])
{
    mark_and_collect
    {
        FileSystemMonitorThread *self = (FileSystemMonitorThread *)info;
        bool retainPaths = 0 != self->eventThread();
        FileSystemMonitorMultiEvent *event = new (collect) FileSystemMonitorMultiEvent(numEvents, retainPaths);
        for (size_t i = 0; numEvents > i; i++)
        {
            if (eventFlags[i] & kFSEventStreamEventFlagHistoryDone)
                continue;
            unsigned action = 0;
            if (eventFlags[i] & (kFSEventStreamEventFlagMustScanSubDirs |
                kFSEventStreamEventFlagMount | kFSEventStreamEventFlagUnmount |
                kFSEventStreamEventFlagItemRenamed))
                action |= FileSystemMonitorEvent::Rescan;
            if (eventFlags[i] & kFSEventStreamEventFlagItemCreated)
                action |= FileSystemMonitorEvent::Create;
            if (eventFlags[i] & kFSEventStreamEventFlagItemRemoved)
                action |= FileSystemMonitorEvent::Remove;
            if (eventFlags[i] & (kFSEventStreamEventFlagItemModified |
                kFSEventStreamEventFlagItemInodeMetaMod | kFSEventStreamEventFlagItemChangeOwner |
                kFSEventStreamEventFlagItemXattrMod | kFSEventStreamEventFlagItemFinderInfoMod))
                action |= FileSystemMonitorEvent::Modify;
            if (0 == action)
                action |= FileSystemMonitorEvent::Rescan;
            const char *path = retainPaths ?
                cstring(((const char **)eventPaths)[i]) : ((const char **)eventPaths)[i];
            event->_addEvent(action, path);
            //DEBUGLOG("%05x->%02x %s", eventFlags[i], action, path);
        }
        self->triggerEvent(event);
    }
}
#endif

const char *FileSystemMonitor::errorDomain()
{
    static const char *s = CStringConstant("Base::impl::FileSystemMonitor")->chars();
    return s;
}
FileSystemMonitor::FileSystemMonitor(Delegate delegate, Thread *eventThread)
{
    _delegate = delegate;
    if (0 != eventThread)
    {
        eventThread->retain();
        _eventThread = eventThread;
    }
    _paths = new OrderedSet;
}
FileSystemMonitor::~FileSystemMonitor()
{
    stop();
    _paths->release();
    if (0 != _eventThread)
        _eventThread->release();
}
void FileSystemMonitor::addPath(const char *path)
{
    CString *str = CString::create(path);
    _paths->addObject(str);
    str->release();
}
void FileSystemMonitor::removePath(const char *path)
{
    _paths->removeObject(BASE_ALLOCA_OBJECT(TransientCString, path));
}
bool FileSystemMonitor::start()
{
    if (0 == _thread)
    {
        FileSystemMonitorThread *thread = new FileSystemMonitorThread(_delegate, _eventThread, _paths);
        thread->start();
        thread->semaphone()->wait();
        Error *error = thread->error();
        Error::setLastError(error);
        if (0 != error)
            return false;
        _thread = thread;
    }
    return true;
}
void FileSystemMonitor::stop()
{
    if (0 != _thread)
    {
        _thread->quit();
        ((FileSystemMonitorThread *)_thread)->semaphone()->wait();
        _thread->release();
        _thread = 0;
    }
}
bool FileSystemMonitor::isStopped()
{
    return 0 == _thread;
}

}
}
