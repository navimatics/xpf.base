/*
 * Base/impl/ObjectCodec.cpp
 *
 * Copyright 2010-2013 Navimatics Corporation. All rights reserved.
 */

#include <Base/impl/ObjectCodec.hpp>
#include <Base/impl/CArray.hpp>
#include <Base/impl/Map.hpp>
#include <Base/impl/Synchronization.hpp>
#include <Base/impl/TypeInfo.hpp>

namespace Base {
namespace impl {

static Lock *GetCodecMapAndLock(Map *&mapref)
{
    static Map *map;
    static Lock *lock;
    execute_once
    {
        map = new Map;
        map->autocollectObjects(false);
        lock = new Lock;
    }
    mapref = map;
    return lock;
}

static int PropertyDescr_cmp(const void *p1, const void *p2)
{
    ObjectCodec::PropertyDescr *prop1 = (ObjectCodec::PropertyDescr *)p1;
    ObjectCodec::PropertyDescr *prop2 = (ObjectCodec::PropertyDescr *)p2;
    return strcmp(prop1->name, prop2->name);
}

Object *ObjectCodec::getObjectProperty(Object *obj, PropertyDescr *prop)
{
    if ('@' != prop->kind)
        return 0;
    if (0 != prop->get)
        return (obj->*(Object *(Object::*)())prop->get)();
    else
        return obj->*(Object *Object::*)prop->p;
}
const char *ObjectCodec::getStringProperty(Object *obj, PropertyDescr *prop)
{
    if ('*' != prop->kind)
        return 0;
    if (0 != prop->get)
        return (obj->*(const char *(Object::*)())prop->get)();
    else
        return obj->*(const char *Object::*)prop->p;
}
bool ObjectCodec::getBooleanProperty(Object *obj, PropertyDescr *prop)
{
    if ('B' != prop->kind)
        return false;
    if (0 != prop->get)
        return (obj->*(bool (Object::*)())prop->get)();
    else
        return obj->*(bool Object::*)prop->p;
}
long ObjectCodec::getIntegerProperty(Object *obj, PropertyDescr *prop)
{
    switch (prop->kind)
    {
    case 'i':
        if (0 != prop->get)
            return (obj->*(int (Object::*)())prop->get)();
        else
            return obj->*(int Object::*)prop->p;
    case 'l':
        if (0 != prop->get)
            return (obj->*(long (Object::*)())prop->get)();
        else
            return obj->*(long Object::*)prop->p;
    default:
        return 0;
    }
}
double ObjectCodec::getRealProperty(Object *obj, PropertyDescr *prop)
{
    switch (prop->kind)
    {
    case 'f':
        if (0 != prop->get)
            return (obj->*(float (Object::*)())prop->get)();
        else
            return obj->*(float Object::*)prop->p;
    case 'd':
        if (0 != prop->get)
            return (obj->*(double (Object::*)())prop->get)();
        else
            return obj->*(double Object::*)prop->p;
    default:
        return 0.0;
    }
}
void ObjectCodec::setObjectProperty(Object *obj, PropertyDescr *prop, Object *value)
{
    if ('@' != prop->kind)
    {
        if ('*' == prop->kind && 0 == value)
            setStringProperty(obj, prop, 0);
        return;
    }
    if (0 != prop->set)
        (obj->*(void (Object::*)(Object *))prop->set)(value);
    else
    {
        if (0 != value)
            value->retain();
        Object *cur = obj->*(Object *Object::*)prop->p;
        if (0 != cur)
            cur->release();
        obj->*(Object *Object::*)prop->p = value;
    }
}
void ObjectCodec::setStringProperty(Object *obj, PropertyDescr *prop, const char *value)
{
    if ('*' != prop->kind)
        return;
    if (0 != prop->set)
        (obj->*(void (Object::*)(const char *))prop->set)(value);
    else
    {
        if (0 != value)
            value = cstr_new(value);
        const char *cur = obj->*(const char *Object::*)prop->p;
        cstr_release(cur);
        obj->*(const char *Object::*)prop->p = value;
    }
}
void ObjectCodec::setBooleanProperty(Object *obj, PropertyDescr *prop, bool value)
{
    if ('B' != prop->kind)
        return;
    if (0 != prop->set)
        (obj->*(void (Object::*)(bool))prop->set)(value);
    else
        obj->*(bool Object::*)prop->p = value;
}
void ObjectCodec::setIntegerProperty(Object *obj, PropertyDescr *prop, long value)
{
    switch (prop->kind)
    {
    case 'i':
        if (0 != prop->set)
            (obj->*(void (Object::*)(int))prop->set)(value);
        else
            obj->*(int Object::*)prop->p = value;
        break;
    case 'l':
        if (0 != prop->set)
            (obj->*(void (Object::*)(long))prop->set)(value);
        else
            obj->*(long Object::*)prop->p = value;
        break;
    default:
        break;
    }
}
void ObjectCodec::setRealProperty(Object *obj, PropertyDescr *prop, double value)
{
    switch (prop->kind)
    {
    case 'f':
        if (0 != prop->set)
            (obj->*(void (Object::*)(float))prop->set)(value);
        else
            obj->*(float Object::*)prop->p = value;
        break;
    case 'd':
        if (0 != prop->set)
            (obj->*(void (Object::*)(double))prop->set)(value);
        else
            obj->*(double Object::*)prop->p = value;
        break;
    default:
        break;
    }
}
void ObjectCodec::registerObjectCodec(ObjectCodec *codec)
{
    Map *map;
    synchronized (GetCodecMapAndLock(map))
        map->setObject(cstr_obj(codec->name()), codec);
}
ObjectCodec *ObjectCodec::getObjectCodecByName(CString *name)
{
    Map *map;
    synchronized (GetCodecMapAndLock(map))
        return (ObjectCodec *)map->object(name);
}
ObjectCodec *ObjectCodec::getObjectCodec(Object *obj)
{
    return getObjectCodecByName(cstr_obj(obj->className()));
}
ObjectCodec::ObjectCodec(const char *name, Object *(*create)())
{
    _init(name, 0, create);
}
ObjectCodec::ObjectCodec(const char *name, ObjectCodec *baseCodec, Object *(*create)())
{
    _init(name, baseCodec, create);
}
ObjectCodec::ObjectCodec(const std::type_info &info, Object *(*create)())
{
    mark_and_collect
        _init(TypeName(info), 0, create);
}
ObjectCodec::ObjectCodec(const std::type_info &info, ObjectCodec *baseCodec, Object *(*create)())
{
    mark_and_collect
        _init(TypeName(info), baseCodec, create);
}
ObjectCodec::~ObjectCodec()
{
    carr_obj(_props)->apply((void (*)(void *))cstr_release);
    carr_release(_props);
    cstr_release(_name);
}
ObjectCodec::PropertyDescr *ObjectCodec::getPropertyDescr(const char *name)
{
    PropertyDescr key = { name };
    return (PropertyDescr *)bsearch(&key, _props, carr_length(_props), sizeof(PropertyDescr), PropertyDescr_cmp);
}
void ObjectCodec::_addPropertyDescr(const char *name,
    int kind, long (Object::*get)(), void (Object::*set)(long),
    PropertyDescr::DefaultValue deflt)
{
    _props = (PropertyDescr *)carr_concat(_props, 0, 1, sizeof(PropertyDescr));
    PropertyDescr *prop = _props + carr_length(_props) - 1;
    prop->name = cstr_new(name);
    prop->kind = kind;
    prop->get = get;
    prop->set = set;
    prop->p = 0;
    prop->deflt = deflt;
    qsort(_props, carr_length(_props), sizeof(PropertyDescr), PropertyDescr_cmp);
}
void ObjectCodec::_addPropertyDescr(const char *name,
    int kind, long Object::*p,
    PropertyDescr::DefaultValue deflt)
{
    _props = (PropertyDescr *)carr_concat(_props, 0, 1, sizeof(PropertyDescr));
    PropertyDescr *prop = _props + carr_length(_props) - 1;
    prop->name = cstr_new(name);
    prop->kind = kind;
    prop->get = 0;
    prop->set = 0;
    prop->p = p;
    prop->deflt = deflt;
    qsort(_props, carr_length(_props), sizeof(PropertyDescr), PropertyDescr_cmp);
}
void ObjectCodec::_init(const char *name, ObjectCodec *baseCodec, Object *(*create)())
{
    threadsafe();
    _name = cstr_new(name);
    _create = create;
    PropertyDescr *baseProps = 0;
    if (0 != baseCodec)
        baseProps = baseCodec->_props;
    _props = (PropertyDescr *)carr_new(baseProps, carr_length(baseProps), sizeof(PropertyDescr));
}

}
}
