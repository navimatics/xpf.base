/*
 * Base/impl/ObjectUtilNative.cpp
 *
 * Copyright 2010-2013 Navimatics Corporation. All rights reserved.
 */

#include <Base/impl/ObjectUtil.hpp>
#include <Base/impl/FileStream.hpp>
#include <Base/impl/ObjectReader.hpp>
#include <Base/impl/ObjectWriter.hpp>

namespace Base {
namespace impl {

Object *ObjectReadNative(const char *path, unsigned compat)
{
    Stream *stream = FileStream::create(path, "rb0");
    if (0 == stream)
        return 0;
    ObjectReader *reader = new ObjectReader(stream);
    if (compat)
        reader->setFlags(compat, 1);
    Object *obj = reader->readObject();
    if (0 != obj)
        obj->autocollect();
    reader->release();
    stream->release();
    return obj;
}
bool ObjectWriteNative(const char *path, Object *obj, unsigned compat)
{
    Stream *stream = FileStream::create(path, "Wb9/");
    if (0 == stream)
        return false;
    ObjectWriter *writer = new ObjectWriter(stream);
    if (compat)
        writer->setFlags(compat, 1);
    bool result = writer->writeObject(obj);
    writer->release();
    result = result && 0 == stream->close();
    stream->release();
    return result;
}

}
}
