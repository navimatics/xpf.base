/*
 * Base/impl/Snprintf.cpp
 *
 * Copyright 2010-2013 Navimatics Corporation. All rights reserved.
 */

#include <Base/impl/Snprintf.hpp>
#include <Base/impl/Log.hpp>
#include "trio.h"

#define RETURN(res)                     \
    if (0 <= res)\
        return res;\
    if (0 < size)\
        buf[0] = '\0';\
    LOG("bad format: %s", format);\
    return 0;

int portable_snprintf(char *buf, size_t size, const char *format, ...)
{
    va_list ap;
    va_start(ap, format);
    int res = trio_vsnprintf(buf, size, format, ap);
    va_end(ap);
    RETURN(res);
}
int portable_vsnprintf(char *buf, size_t size, const char *format, va_list ap)
{
    int res = trio_vsnprintf(buf, size, format, ap);
    RETURN(res);
}
int portable_snprintfv(char *buf, size_t size, const char *format, void **args)
{
    int res = trio_snprintfv(buf, size, format, args);
    RETURN(res);
}
