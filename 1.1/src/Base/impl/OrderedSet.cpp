/*
 * Base/impl/OrderedSet.cpp
 *
 * Copyright 2010-2013 Navimatics Corporation. All rights reserved.
 */

#include <Base/impl/OrderedSet.hpp>
#include <Base/impl/CArray.hpp>
#include <Base/impl/CString.hpp>

namespace Base {
namespace impl {

OrderedSet::OrderedSet(ObjectComparer *comparer) :
    _comparerRef(0 == comparer ? 0 : comparer->self()),
    _set(Object_less_t(comparer))
{
}
void OrderedSet::autocollectObjects(bool ac)
{
    _nac = !ac;
}
size_t OrderedSet::count()
{
    return _set.size();
}
bool OrderedSet::containsObject(Object *obj)
{
    set_t::iterator iter = _set.find(Ref<Object>(obj));
    return iter != _set.end();
}
Object *OrderedSet::anyObject()
{
    set_t::iterator iter = _set.begin();
    if (iter != _set.end())
    {
        Object *obj = *iter;
        if (0 == obj)
            return 0;
        return _nac ? obj : obj->autocollect();
    }
    else
        return 0;
}
void OrderedSet::addObject(Object *obj)
{
    _set.insert(Ref<Object>(obj));
}
void OrderedSet::removeObject(Object *obj)
{
    _set.erase(Ref<Object>(obj));
}
void OrderedSet::removeAllObjects()
{
    _set.clear();
}
void OrderedSet::addObjects(OrderedSet *set)
{
    for (set_t::iterator p = set->_set.begin(), q = set->_set.end(); p != q; ++p)
        _set.insert(*p);
}
Object **OrderedSet::objects()
{
    Object **objbuf = (Object **)carrayWithCapacity(_set.size(), sizeof(Object *));
    carr_obj(objbuf)->setLength(_set.size());
    Object **objs = objbuf;
    for (set_t::iterator p = _set.begin(), q = _set.end(); p != q; ++p, ++objs)
        *objs = *p;
    return objbuf;
}
const char *OrderedSet::strrepr()
{
    return cstringf("<%s %p; count=%u>", className(), this, count());
}
/* Iterable<Object *> */
void *OrderedSet::iterate(void *state, Object **bufp[2])
{
    if (0 == state)
        state = new (collect) Iterator(_set);
    Iterator *iter = (Iterator *)state;
    Object **curp = bufp[0], **endp = bufp[1];
    while (curp < endp && iter->p != iter->q)
    {
        *curp++ = *iter->p;
        ++iter->p;
    }
    bufp[1] = curp;
    return state;
}
/* ObjectCodec use */
Array *OrderedSet::__array()
{
    Array *array = new (collect) Array(_set.size());
    for (set_t::iterator p = _set.begin(), q = _set.end(); p != q; ++p)
        array->addObject(*p);
    return array;
}
void OrderedSet::__setArray(Array *array)
{
    foreach (Object *obj, array)
        _set.insert(Ref<Object>(obj));
}
ObjectCodecBegin(OrderedSet)
    ObjectCodecProperty("_", &OrderedSet::__array, &OrderedSet::__setArray)
ObjectCodecEnd(OrderedSet)

}
}
