/*
 * Base/impl/ObjectUtilJson.cpp
 *
 * Copyright 2010-2013 Navimatics Corporation. All rights reserved.
 */

#include <Base/impl/ObjectUtil.hpp>
#include <Base/impl/FileStream.hpp>
#include <Base/impl/ObjectReader.hpp>
#include <Base/impl/ObjectWriter.hpp>

namespace Base {
namespace impl {

Object *ObjectReadJson(const char *path)
{
    Stream *stream = FileStream::create(path, "rb");
    if (0 == stream)
        return 0;
    ObjectReader *reader = new ObjectReader(stream);
    reader->setFlags(ObjectReader::ObjectCodecExtensions, 0);
    Object *obj = reader->readObject();
    if (0 != obj)
        obj->autocollect();
    reader->release();
    stream->release();
    return obj;
}
bool ObjectWriteJson(const char *path, Object *obj)
{
    Stream *stream = FileStream::create(path, "Wb/");
    if (0 == stream)
        return false;
    ObjectWriter *writer = new ObjectWriter(stream);
    writer->setFlags(ObjectWriter::ObjectCodecExtensions, 0);
    bool result = writer->writeObject(obj);
    writer->release();
    result = result && 0 == stream->close();
    stream->release();
    return result;
}

}
}
