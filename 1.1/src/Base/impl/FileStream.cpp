/*
 * Base/impl/FileStream.cpp
 *
 * Copyright 2010-2013 Navimatics Corporation. All rights reserved.
 */

#include <Base/impl/FileStream.hpp>
#include <Base/impl/CString.hpp>
#include <Base/impl/FileUtil.hpp>
#include <Base/impl/StdioStream.hpp>
#include <Base/impl/ZipFileStream.hpp>
#include <Base/impl/ZlibStream.hpp>
#if defined(BASE_CONFIG_WINDOWS)
#include <windows.h>
#endif

namespace Base {
namespace impl {

FileStream *FileStream::create(const char *path, const char *mode)
{
    char *iomode = (char *)alloca(strlen(mode) + 1), *iomodep = iomode;
    int a = 0, r = 0, w = 0, W = 0, b = 0, plus = 0, g = 0, z = 0, slash = 0, arc = 0, clevel = -1;
    while (*mode)
    {
        char c = *mode++;
        switch (c)
        {
        case 'a':
            *iomodep++ = c;
            a = 1;
            break;
        case 'r':
            *iomodep++ = c;
            r = 1;
            break;
        case 'w':
            *iomodep++ = c;
            w = 1;
            break;
        case 'W':
            *iomodep++ = 'w';
            w = W = 1;
            break;
        case 'b':
            *iomodep++ = c;
            b = 1;
            break;
        case '+':
            *iomodep++ = c;
            plus = 1;
            break;
        case 'g':
            g = 1;
            break;
        case 'z':
            z = 1;
            break;
        case '/':
            slash = 1;
            break;
        case '!':
            arc = 1;
            break;
        case '0': case '1': case '2': case '3': case '4':
        case '5': case '6': case '7': case '8': case '9':
            clevel = c - '0';
            break;
        default:
            *iomodep++ = c;
            break;
        }
    }
    *iomodep = '\0';
    if ((g || z) && -1 == clevel)
        clevel = w ? 9 : 0;
    char zmode[5];
    if (-1 == clevel)
    {
        if (arc)
            if (!b || a || plus || (r && w) || W)
                return 0;
        zmode[0] = '\0';
    }
    else
    {
        if (arc)
            return 0;
        if (!b || a || plus || (r && w))
            return 0;
        zmode[0] = w ? 'w' : 'r';
        zmode[1] = 'b';
        if (g || z)
        {
            zmode[2] = g ? 'g' : 'z';
            zmode[3] = clevel + '0';
            zmode[4] = '\0';
        }
        else
        {
            zmode[2] = clevel + '0';
            zmode[3] = '\0';
        }
    }
    ptrdiff_t arcidx = 0;
    if (arc)
    {
        if (const char *p = strchr(path, '!'))
        {
            if (path == p || '/' != p[1] || '\0' == p[2])
                return 0;
            arcidx = p - path;
        }
        else
            arc = 0;
    }
    FileStream *stream = new FileStream;
    stream->_path = cstr_new(path);
    if (W)
    {
        Uuid uuid; UuidCreate(uuid);
        char uuid_s[40]; UuidUnparseRaw(uuid, uuid_s, sizeof uuid_s);
        stream->_wpath = cstr_newf("%s%s", stream->_path, uuid_s);
        if (!slash || -1 != mkpdirs(stream->_path))
            stream->_stream = StdioStream::create(stream->_wpath, iomode);
    }
    else
    {
        if (!arc)
        {
            if (!slash || -1 != mkpdirs(stream->_path))
                stream->_stream = StdioStream::create(stream->_path, iomode);
        }
        else
        {
            char *fpath = (char *)alloca(arcidx + 1);
            memcpy(fpath, stream->_path, arcidx);
            fpath[arcidx] = '\0';
            if (!slash || -1 != mkpdirs(fpath))
                stream->_stream = ZipFileStream::create(fpath, stream->_path + arcidx + 2, iomode);
        }
    }
    if (0 == stream->_stream)
    {
        stream->release();
        return 0;
    }
    if (-1 != clevel)
    {
        stream->_iostream = stream->_stream;
        stream->_stream = new ZlibStream(stream->_iostream, zmode);
    }
    return stream;
}
FileStream::~FileStream()
{
    close();
    cstr_release(_wpath);
    cstr_release(_path);
}
ssize_t FileStream::read(void *buf, size_t size)
{
    if (0 == _stream)
        return -1;
    return _stream->read(buf, size);
}
ssize_t FileStream::write(const void *buf, size_t size)
{
    if (0 == _stream)
        return -1;
    return _stream->write(buf, size);
}
ssize_t FileStream::seek(ssize_t offset, int whence)
{
    if (0 == _stream)
        return -1;
    return _stream->seek(offset, whence);
}
ssize_t FileStream::close()
{
    if (0 == _stream)
        return -1;
    ssize_t result = _stream->close();
    _stream->release();
    _stream = 0;
    if (0 != _iostream)
    {
        result = _iostream->close();
        _iostream->release();
        _iostream = 0;
    }
    if (0 != _wpath)
    {
#if defined(BASE_CONFIG_WINDOWS)
        if (!MoveFileExA(_wpath, _path, MOVEFILE_REPLACE_EXISTING))
        {
            DeleteFileA(_wpath);
            result = -1;
        }
#else
        if (-1 == rename(_wpath, _path))
        {
            remove(_wpath);
            result = -1;
        }
#endif
    }
    return result;
}
const char *FileStream::path()
{
    cstr_autocollect(_path);
    return _path;
}

}
}
