/*
 * Base/impl/SystemStream.cpp
 *
 * Copyright 2010-2013 Navimatics Corporation. All rights reserved.
 */

#include <Base/impl/SystemStream.hpp>
#include <Base/impl/Atomic.hpp>
#include <Base/impl/Error.hpp>
#include <Base/impl/IndexSet.hpp>
#include <Base/impl/Synchronization.hpp>
#include <Base/impl/Thread.hpp>
#if defined(BASE_CONFIG_WINDOWS)
#include <windows.h>
#if !defined(ECANCELED)
#define ECANCELED                       105
#endif
#elif defined(BASE_CONFIG_POSIX)
#include <fcntl.h>
#include <unistd.h>
#endif

namespace Base {
namespace impl {

static int _objcnt;
static Lock *_objidsLock;
static IndexSet *_objids;
SystemStream *SystemStream::create(const char *path, const char *mode)
{
    int a = 0, r = 0, w = 0, b = 0, plus = 0;
    while (*mode)
    {
        char c = *mode++;
        switch (c)
        {
        case 'a':
            a = 1;
            break;
        case 'r':
            r = 1;
            break;
        case 'w':
            w = 1;
            break;
        case 'b':
            b = 1;
            break;
        case '+':
            plus = 1;
            break;
        default:
            break;
        }
    }
#if defined(BASE_CONFIG_WINDOWS)
    DWORD iomode, crmode;
    if (r)
    {
        iomode = plus ? GENERIC_READ | GENERIC_WRITE : GENERIC_READ;
        crmode = OPEN_EXISTING;
    }
    else if (w)
    {
        iomode = plus ? GENERIC_READ | GENERIC_WRITE : GENERIC_WRITE;
        crmode = CREATE_ALWAYS;
    }
    else if (a)
    {
        iomode = plus ? GENERIC_READ | GENERIC_WRITE : GENERIC_WRITE;
        crmode = OPEN_ALWAYS;
    }
    else
    {
        Error::setLastErrorFromCErrno(EINVAL);
        return 0;
    }
    handle_t handle = (handle_t)CreateFileA(
        path, iomode, FILE_SHARE_READ | FILE_SHARE_WRITE, 0, crmode,
        FILE_ATTRIBUTE_NORMAL | FILE_FLAG_OVERLAPPED, 0);
    if (InvalidHandle == handle)
    {
        Error::setLastErrorFromSystem();
        return 0;
    }
    SystemStream *stream = new SystemStream(handle);
    if (a)
        stream->seek(0, SEEK_END);
    return stream;
#elif defined(BASE_CONFIG_POSIX)
    int iomode;
    if (r)
        iomode = plus ? O_RDWR : O_RDONLY;
    else if (w)
        iomode = (plus ? O_RDWR : O_WRONLY) | O_CREAT | O_TRUNC;
    else if (a)
        iomode = (plus ? O_RDWR : O_WRONLY) | O_CREAT | O_APPEND;
    else
    {
        Error::setLastErrorFromCErrno(EINVAL);
        return 0;
    }
    handle_t handle = ::open(path, iomode, 0777);
    if (InvalidHandle == handle)
    {
        Error::setLastErrorFromSystem();
        return 0;
    }
    return new SystemStream(handle);
#endif
}
SystemStream::SystemStream(handle_t handle)
{
    execute_once
    {
        _objidsLock = new Lock;
        _objids = new IndexSet;
    }
    threadsafe();
    _objid = atomic_inc(_objcnt);
    _handle = handle;
}
SystemStream::~SystemStream()
{
    if (InvalidHandle != _handle)
        close();
    else
    {
        synchronized (_objidsLock)
            _objids->removeIndex(_objid);
    }
}
ssize_t SystemStream::read(void *buf, size_t size)
{
    return rdwr(buf, size, 0);
}
ssize_t SystemStream::write(const void *buf, size_t size)
{
    return rdwr((void *)buf, size, 1);
}
ssize_t SystemStream::close()
{
    if (InvalidHandle == _handle)
    {
        Error::setLastErrorFromCErrno(EINVAL);
        return -1;
    }
    int res = _close(_handle);
    _handle = InvalidHandle;
    synchronized (_objidsLock)
        _objids->removeIndex(_objid);
    if (-1 == res)
        Error::setLastErrorFromSystem();
    return res;
}
void SystemStream::cancel()
{
    _canceled = 1;
    atomic_wmb();
    thr_t thr = _thr;
    // atomic_rmb(); // not needed as synchronized() below issues a full memory barrier
    synchronized (_objidsLock)
        _objids->addIndex(_objid);
    if (thr)
        Thread::_interrupt(thr);
}
#if defined(BASE_CONFIG_WINDOWS)
ssize_t SystemStream::seek(ssize_t offset, int whence)
{
    if (InvalidHandle == _handle)
    {
        Error::setLastErrorFromCErrno(EINVAL);
        return -1;
    }
    int64_t newofst = _offset;
    switch (whence)
    {
    default:
    case SEEK_SET:
        newofst = offset;
        break;
    case SEEK_CUR:
        newofst += offset;
        break;
    case SEEK_END:
        {
            LARGE_INTEGER largeOffset;
            if (!GetFileSizeEx((HANDLE)_handle, &largeOffset))
            {
                Error::setLastErrorFromSystem();
                return -1;
            }
            newofst = largeOffset.QuadPart + offset;
        }
        break;
    }
    if (0 > newofst)
    {
        Error::setLastErrorFromCErrno(EINVAL);
        return -1;
    }
    return _offset = newofst;
}
ssize_t SystemStream::_close(handle_t handle)
{
    return CloseHandle((HANDLE)handle) ? 0 : -1;
}
struct Overlapped : OVERLAPPED
{
    DWORD dwErrorCode;
    DWORD dwNumberOfBytesTransfered;
    int complete;
};
static VOID CALLBACK FileIOComplete(
    DWORD dwErrorCode, DWORD dwNumberOfBytesTransfered, LPOVERLAPPED lpOverlapped)
{
    Overlapped *overlapped = (Overlapped *)lpOverlapped;
    overlapped->dwErrorCode = dwErrorCode;
    overlapped->dwNumberOfBytesTransfered = dwNumberOfBytesTransfered;
    overlapped->complete = 1;
}
ssize_t SystemStream::rdwr(void *buf, size_t size, int flag)
{
    if (InvalidHandle == _handle)
    {
        Error::setLastErrorFromCErrno(EINVAL);
        return -1;
    }
    int canceled = _canceled;
    atomic_rmb();
    if (canceled)
    {
        Error::setLastErrorFromCErrno(ECANCELED);
        return -1;
    }
    Overlapped overlapped;
    memset(&overlapped, 0, sizeof overlapped);
    overlapped.Offset = _offset;
    overlapped.OffsetHigh = ((uint64_t)_offset) >> 32;
    BOOL res = flag ?
        WriteFileEx((HANDLE)_handle, buf, size, &overlapped, FileIOComplete) :
        ReadFileEx((HANDLE)_handle, buf, size, &overlapped, FileIOComplete);
    if (!res)
    {
        Error::setLastErrorFromSystem();
        return -1;
    }
    Thread *thread = Thread::currentThread();
    for (;;)
    {
        _thr = thread->thr();
        atomic_wmb();
        canceled = _canceled;
        atomic_rmb();
        if (!canceled)
            SleepEx(INFINITE, TRUE);
        _thr = 0;
        atomic_wmb();
        /* !!!:
         * We first test if the I/O has completed to avoid the expensive cancelation test.
         * Note that this means that if a cancelation request is processed during the same
         * SleepEx() call as a successful I/O completion, then rdwr() will succeed rather
         * than fail. This is not a problem, because the object has been marked as canceled
         * (by cancel()) and because rdwr() has completed (so it is not blocked waiting for
         * input).
         *
         * Note however that the semantics are slightly different from the current POSIX
         * implementation of rdwr(). In that implementation if a cancelation request arrives at
         * the same time that a read()/write() completes successfully, the cancelation request is
         * reported to the user.
         *
         * Therefore the Windows implementation views a simultaneous cancelation request as
         * arriving slightly *after* I/O completion. The POSIX implementation views a simultaneous
         * cancelation request as arriving slightly *before* I/O completion. This distinction is not
         * important in practice. If this view changes the ``if (overlapped.complete)'' statement
         * should be moved below the cancelation check.
         */
        if (overlapped.complete)
        {
            if (ERROR_SUCCESS != overlapped.dwErrorCode)
            {
                Error::setLastErrorFromSystem(overlapped.dwErrorCode);
                return -1;
            }
            _offset += overlapped.dwNumberOfBytesTransfered;
            return overlapped.dwNumberOfBytesTransfered;
        }
        synchronized (_objidsLock)
            canceled = _objids->containsIndex(_objid);
        if (canceled)
        {
            CancelIo((HANDLE)_handle);
            _canceled = 1;
            Error::setLastErrorFromCErrno(ECANCELED);
            return -1;
        }
    }
}
#elif defined(BASE_CONFIG_POSIX)
ssize_t SystemStream::seek(ssize_t offset, int whence)
{
    if (InvalidHandle == _handle)
    {
        Error::setLastErrorFromCErrno(EINVAL);
        return -1;
    }
    ssize_t res = ::lseek(_handle, offset, whence);
    if (-1 == res)
        Error::setLastErrorFromSystem();
    return res;
}
ssize_t SystemStream::_close(handle_t handle)
{
    return ::close(handle);
}
ssize_t SystemStream::rdwr(void *buf, size_t size, int flag)
{
    if (InvalidHandle == _handle)
    {
        Error::setLastErrorFromCErrno(EINVAL);
        return -1;
    }
    int canceled = _canceled;
    atomic_rmb();
    if (canceled)
    {
        Error::setLastErrorFromCErrno(ECANCELED);
        return -1;
    }
    Thread *thread = Thread::currentThread();
    for (;;)
    {
        sigjmp_buf jmpbuf;
        if (0 == sigsetjmp(jmpbuf, 1))
        {
            int res = -1;
            thread->_setSlot(Thread::_SlotJmpbuf, &jmpbuf);
            _thr = thread->thr();
            atomic_wmb();
            canceled = _canceled;
            atomic_rmb();
            if (!canceled)
                res = flag ?
                    ::write(_handle, buf, size) :
                    ::read(_handle, buf, size);
            _thr = 0;
            atomic_wmb();
            thread->_setSlot(Thread::_SlotJmpbuf, 0);
            if (-1 == res)
                if (canceled)
                    Error::setLastErrorFromCErrno(ECANCELED);
                else
                    Error::setLastErrorFromSystem();
            return res;
        }
        else
        {
            synchronized (_objidsLock)
                canceled = _objids->containsIndex(_objid);
            if (canceled)
            {
                _canceled = 1;
                Error::setLastErrorFromCErrno(ECANCELED);
                return -1;
            }
        }
    }
}
#endif

}
}
