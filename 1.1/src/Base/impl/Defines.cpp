/*
 * Base/impl/Defines.cpp
 *
 * Copyright 2010-2013 Navimatics Corporation. All rights reserved.
 */

#include <Base/impl/Defines.hpp>

namespace Base {
namespace impl {

void MemoryAllocationError()
{
    throw std::bad_alloc();
}

}
}
