/*
 * Base/impl/ZlibStream.cpp
 *
 * Copyright 2010-2013 Navimatics Corporation. All rights reserved.
 */

#include <Base/impl/ZlibStream.hpp>
#include <zlib.h>

#define _z                              (*(z_stream *)_zp)
#define IOBUFSIZ                        16384

namespace Base {
namespace impl {

ZlibStream::ZlibStream(Stream *stream, const char *mode)
{
    _zp = Malloc(sizeof(z_stream));
    memset(_zp, 0, sizeof(z_stream));
    _stream = stream;
    _mode = 'r';
    _head = 0;
    _clevel = 9;
    while (*mode)
    {
        char c = *mode++;
        switch (c)
        {
        case 'r':
        case 'w':
            _mode = c;
            break;
        case 'g':
        case 'z':
            _head = c;
            break;
        case '0': case '1': case '2': case '3': case '4':
        case '5': case '6': case '7': case '8': case '9':
            _clevel = c - '0';
            break;
        case 'b':
        default:
            break;
        }
    }
}
ZlibStream::~ZlibStream()
{
    close();
    free(_zp);
}
ssize_t ZlibStream::read(void *buf, size_t size)
{
    if (_err || 0 == _stream)
        return -1;
    if ('r' != _mode)
    {
        _err = true;
        return -1;
    }
    if (_eof || 0 == buf || 0 == size)
        return 0;
    if (0 == _iobuf)
        init();
    _z.next_out = (Bytef *)buf;
    _z.avail_out = size;
    do
    {
        if (!_eos && 0 == _z.avail_in)
        {
            ssize_t bytes = _stream->read(_iobuf, IOBUFSIZ);
            _err = -1 == bytes;
            if (_err)
                return -1;
            _eos = 0 == bytes;
            _z.next_in = (Bytef *)_iobuf;
            _z.avail_in = bytes;
        }
        if (0 < _clevel)
        {
            /* compress */
            int ret = deflate(&_z, _eos ? Z_FINISH : Z_NO_FLUSH);
            assert(Z_STREAM_ERROR != ret);
            _eof = Z_STREAM_END == ret;
        }
        else
        {
            /* decompress */
            int ret = inflate(&_z, Z_NO_FLUSH);
            assert(Z_STREAM_ERROR != ret);
            switch (ret)
            {
            case Z_MEM_ERROR:
                MemoryAllocationError();
            case Z_NEED_DICT:
            case Z_DATA_ERROR:
                _err = true;
                return -1;
            case Z_BUF_ERROR:
                assert(_eos);
                ret = Z_STREAM_END;
                break;
            }
            _eof = Z_STREAM_END == ret;
        }
    } while (!_eof && 0 != _z.avail_out);
    return size - _z.avail_out;
}
ssize_t ZlibStream::write(const void *buf, size_t size)
{
    if (_err || 0 == _stream)
        return -1;
    if ('w' != _mode)
    {
        _err = true;
        return -1;
    }
    if (_eof || 0 == buf || 0 == size)
        return 0;
    if (0 == _iobuf)
        init();
    _z.next_in = (Bytef *)buf;
    _z.avail_in = size;
    do
    {
        _z.next_out = (Bytef *)_iobuf;
        _z.avail_out = IOBUFSIZ;
        if (0 < _clevel)
        {
            /* compress */
            int ret = deflate(&_z, Z_NO_FLUSH);
            (void)ret;
            assert(Z_STREAM_ERROR != ret);
        }
        else
        {
            /* decompress */
            int ret = inflate(&_z, Z_NO_FLUSH);
            assert(Z_STREAM_ERROR != ret);
            switch (ret)
            {
            case Z_MEM_ERROR:
                MemoryAllocationError();
            case Z_NEED_DICT:
            case Z_DATA_ERROR:
                _err = true;
                return -1;
            }
            _eof = Z_STREAM_END == ret;
        }
        _z.next_out = (Bytef *)_iobuf;
        _z.avail_out = IOBUFSIZ - _z.avail_out;
        while (0 != _z.avail_out)
        {
            ssize_t bytes = _stream->write(_z.next_out, _z.avail_out);
            _err = -1 == bytes;
            if (_err)
                return -1;
            _z.next_out += bytes;
            _z.avail_out -= bytes;
        }
    } while (!_eof && 0 != _z.avail_in);
    return size - _z.avail_in;
}
ssize_t ZlibStream::close()
{
    if (0 == _stream)
        return -1;
    ssize_t result = 0;
    if (0 != _iobuf)
    {
        if (!_err && 'w' == _mode && 0 < _clevel)
        {
            bool done;
            do
            {
                _z.next_in = 0;
                _z.avail_in = 0;
                _z.next_out = (Bytef *)_iobuf;
                _z.avail_out = IOBUFSIZ;
                int ret = deflate(&_z, Z_FINISH);
                assert(Z_STREAM_ERROR != ret);
                done = Z_STREAM_END == ret;
                _z.next_out = (Bytef *)_iobuf;
                _z.avail_out = IOBUFSIZ - _z.avail_out;
                while (0 != _z.avail_out)
                {
                    ssize_t bytes = _stream->write(_z.next_out, _z.avail_out);
                    _err = -1 == bytes;
                    if (_err)
                    {
                        result = -1;
                        goto closestream;
                    }
                    _z.next_out += bytes;
                    _z.avail_out -= bytes;
                }
            } while (!done);
        }
    closestream:
        if (0 < _clevel)
            deflateEnd(&_z);
        else
            inflateEnd(&_z);
        free(_iobuf);
        _iobuf = 0;
    }
    _stream = 0;
    return result;
}
void ZlibStream::init()
{
    void *p = Malloc(IOBUFSIZ);
    _iobuf = p;
    if (0 < _clevel)
    {
        /* compress */
        int wbits;
        switch (_head)
        {
        case 'z':
            wbits = MAX_WBITS + 0;  /* zlib */
            break;
        case 'g':
            wbits = MAX_WBITS + 16; /* gzip */
            break;
        default:
            wbits = MAX_WBITS + 16; /* gzip (default) */
            break;
        }
        int ret = deflateInit2(&_z, _clevel, Z_DEFLATED, wbits, 8, Z_DEFAULT_STRATEGY);
        if (Z_MEM_ERROR == ret)
            MemoryAllocationError();
        assert(Z_OK == ret);
    }
    else
    {
        /* decompress */
        int wbits;
        switch (_head)
        {
        case 'z':
            wbits = MAX_WBITS + 0;  /* zlib */
            break;
        case 'g':
            wbits = MAX_WBITS + 16; /* gzip */
            break;
        default:
            wbits = MAX_WBITS + 32; /* autodetect (default) */
            break;
        }
        int ret = inflateInit2(&_z, wbits);
        if (Z_MEM_ERROR == ret)
            MemoryAllocationError();
        assert(Z_OK == ret);
    }
}

}
}
