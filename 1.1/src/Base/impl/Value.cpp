/*
 * Base/impl/Value.cpp
 *
 * Copyright 2010-2013 Navimatics Corporation. All rights reserved.
 */

#include <Base/impl/Value.hpp>
#include <Base/impl/CString.hpp>
#include <Base/impl/Synchronization.hpp>

namespace Base {
namespace impl {

#define VALUE_AS(t) \
    switch (_kind)\
    {\
    case 'B':\
        return (t)_b;\
    case 'l':\
        return (t)_l;\
    case 'd':\
        return (t)_d;\
    default:\
        return (t)0;\
    }

Value *Value::create(bool B)
{
    static Value *valueF;
    static Value *valueT;
    execute_once
    {
        valueF = Value::_staticValue(false);
        valueT = Value::_staticValue(true);
    }
    return B ? valueT : valueF;
}
Value *Value::create(long l)
{
    static Value *value0;
    static Value *value1;
    execute_once
    {
        value0 = Value::_staticValue(0L);
        value1 = Value::_staticValue(1L);
    }
    if (0 == l)
        return value0;
    if (1 == l)
        return value1;
    Value *self = new Value;
    self->_kind = 'l';
    self->_l = l;
    return self;
}
Value *Value::create(double d)
{
    static Value *value0;
    static Value *value1;
    execute_once
    {
        value0 = Value::_staticValue(0.0);
        value1 = Value::_staticValue(1.0);
    }
    if (0.0 == d)
        return value0;
    if (1.0 == d)
        return value1;
    Value *self = new Value;
    self->_kind = 'd';
    self->_d = d;
    return self;
}
bool Value::booleanValue()
{
    switch (_kind)
    {
    case 'B':
        return _b;
    case 'l':
        return 0 != _l;
    case 'd':
        return 0 != _d;
    default:
        return false;
    }
}
long Value::integerValue()
{
    VALUE_AS(long)
}
double Value::realValue()
{
    VALUE_AS(double)
}
size_t Value::hash()
{
    VALUE_AS(size_t)
}
int Value::cmp(Object *obj)
{
    Value *val = dynamic_cast<Value *>(obj);
    if (0 == val || _kind != val->_kind)
        return (Object *)this - obj;
    switch (_kind)
    {
    case 'B':
        return (int)(_b - val->_b);
    case 'l':
        return (int)(_l - val->_l);
    case 'd':
        return (int)(_d - val->_d);
    default:
        return (int)((Object *)this - obj);
    }
}
const char *Value::strrepr()
{
    switch (_kind)
    {
    case 'B':
        return cstringf("%i", _b);
    case 'l':
        return cstringf("%li", _l);
    case 'd':
        if (_d == floor(_d))
            return cstringf("%.1f", _d);
        else
            return cstringf(DBL_PRECISION_G_FMT, _d);
    default:
        return Object::strrepr();
    }
}

Value *Value::_staticValue(bool B)
{
    Value *self = new Value;
    self->_kind = 'B';
    self->_b = B;
    self->linger();
    return self;
}
Value *Value::_staticValue(long l)
{
    Value *self = new Value;
    self->_kind = 'l';
    self->_l = l;
    self->linger();
    return self;
}
Value *Value::_staticValue(double d)
{
    Value *self = new Value;
    self->_kind = 'd';
    self->_d = d;
    self->linger();
    return self;
}

}
}
