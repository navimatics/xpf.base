/*
 * Base/impl/TextReader.cpp
 *
 * Copyright 2010-2013 Navimatics Corporation. All rights reserved.
 */

#include <Base/impl/TextReader.hpp>
#include <Base/impl/CString.hpp>

namespace Base {
namespace impl {

#define NUMBER_LOOKAHEAD                64

TextReader::TextReader(Stream *stream) : TextParser(stream)
{
}
void TextReader::reset()
{
    _finished = false;
    TextParser::reset();
}
char TextReader::readChar()
{
    for (;;)
    {
        if (_testchar())
        {
            char c = _peekchar();
            skipchar();
            return c;
        }
        if (_finished)
            return 0;
        _finished = 0 >= parse();
    }
}
const char *TextReader::readLine()
{
    for (;;)
    {
        const char *startp = _charptr();
        const char *p = startp;
        while (_testptr(p))
        {
            if ('\n' == *p)
            {
                size_t skip = p - startp + 1;
                if (startp < p && '\r' == p[-1])
                    p--;
                const char *res = cstring(startp, p - startp);
                skipnext(skip);
                return res;
            }
            p++;
        }
        if (_finished)
        {
            if (_testchar())
            {
                const char *res = cstring(_nextp, _freep - _nextp);
                skipnext(_freep - _nextp);
                return res;
            }
            return 0;
        }
        _finished = 0 >= parse();
    }
}
const char *TextReader::readUntil(const char *delim)
{
    ssize_t delimLenM1 = strlen(delim) - 1;
    if (-1 == delimLenM1)
        return 0;
    for (;;)
    {
        const char *startp = _charptr();
        const char *p = startp;
        while (_testptr(p + delimLenM1))
        {
            if (p[0] == delim[0] && 0 == memcmp(p + 1, delim + 1, delimLenM1))
            {
                size_t skip = p - startp + delimLenM1 + 1;
                const char *res = cstring(startp, p - startp);
                skipnext(skip);
                return res;
            }
            p++;
        }
        if (_finished)
        {
            if (_testchar())
            {
                const char *res = cstring(_nextp, _freep - _nextp);
                skipnext(_freep - _nextp);
                return res;
            }
            return 0;
        }
        _finished = 0 >= parse();
    }
}
const char *TextReader::readSpan(const char *chars, bool complement)
{
    for (;;)
    {
        const char *startp = _charptr();
        const char *p = startp;
        if (!complement)
        {
            while (_testptr(p))
            {
                if (0 == strchr(chars, *p))
                {
                    const char *res = cstring(startp, p - startp);
                    skipnext(p - startp);
                    return res;
                }
                p++;
            }
        }
        else
        {
            while (_testptr(p))
            {
                if (0 != strchr(chars, *p))
                {
                    const char *res = cstring(startp, p - startp);
                    skipnext(p - startp);
                    return res;
                }
                p++;
            }
        }
        if (_finished)
        {
            if (_testchar())
            {
                const char *res = cstring(_nextp, _freep - _nextp);
                skipnext(_freep - _nextp);
                return res;
            }
            return 0;
        }
        _finished = 0 >= parse();
    }
}
void TextReader::skipSpan(const char *chars, bool complement)
{
    for (;;)
    {
        const char *startp = _charptr();
        const char *p = startp;
        if (!complement)
        {
            while (_testptr(p))
            {
                if (0 == strchr(chars, *p))
                {
                    skipnext(p - startp);
                    return;
                }
                p++;
            }
        }
        else
        {
            while (_testptr(p))
            {
                if (0 != strchr(chars, *p))
                {
                    skipnext(p - startp);
                    return;
                }
                p++;
            }
        }
        if (_finished)
        {
            if (_testchar())
                skipnext(_freep - _nextp);
            return;
        }
        _finished = 0 >= parse();
    }
}
long TextReader::readIntegerValue()
{
    skipSpan("\t\n\v\f\r ", false);
    for (;;)
    {
        bool lookahead = _testnext(NUMBER_LOOKAHEAD);
        if (lookahead || _finished)
        {
            char buf[NUMBER_LOOKAHEAD + 1];
            if (lookahead)
            {
                memcpy(buf, _charptr(), NUMBER_LOOKAHEAD);
                buf[NUMBER_LOOKAHEAD] = '\0';
            }
            else
            {
                memcpy(buf, _nextp, _freep - _nextp);
                buf[_freep - _nextp] = '\0';
            }
            char *endp;
            long res = strtol(buf, &endp, 0);
            if (buf == endp)
                return None();
            skipnext(endp - buf);
            return res;
        }
        _finished = 0 >= parse();
    }
}
double TextReader::readRealValue()
{
    skipSpan("\t\n\v\f\r ", false);
    for (;;)
    {
        bool lookahead = _testnext(NUMBER_LOOKAHEAD);
        if (lookahead || _finished)
        {
            char buf[NUMBER_LOOKAHEAD + 1];
            if (lookahead)
            {
                memcpy(buf, _charptr(), NUMBER_LOOKAHEAD);
                buf[NUMBER_LOOKAHEAD] = '\0';
            }
            else
            {
                memcpy(buf, _nextp, _freep - _nextp);
                buf[_freep - _nextp] = '\0';
            }
            char *endp;
            double res = strtod(buf, &endp);
            if (buf == endp)
                return None();
            skipnext(endp - buf);
            return res;
        }
        _finished = 0 >= parse();
    }
}
bool TextReader::finished()
{
    return _finished;
}
void TextReader::setFinished(bool value)
{
    _finished = value;
}

}
}
