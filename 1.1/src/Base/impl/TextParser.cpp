/*
 * Base/impl/TextParser.cpp
 *
 * Copyright 2010-2013 Navimatics Corporation. All rights reserved.
 */

#include <Base/impl/TextParser.hpp>
#include <Base/impl/CString.hpp>
#include <Base/impl/Error.hpp>

namespace Base {
namespace impl {

#define MinBufSize                      (_endp - _bufp) / 4

const char *TextParser::errorDomain()
{
    static const char *s = CStringConstant("Base::impl::TextParser")->chars();
    return s;
}
TextParser::TextParser(Stream *stream, size_t bufsiz)
{
    if (0 != stream)
        _stream = (Stream *)stream->retain();
    _bufp = (char *)Malloc(bufsiz);
    _nextp = _freep = _bufp;
    _endp = _bufp + bufsiz;
}
TextParser::~TextParser()
{
    free(_bufp);
    if (0 != _stream)
        _stream->release();
}
void TextParser::reset()
{
    _nextp = _freep = _bufp;
}
ssize_t TextParser::parse(const void *buf, size_t size)
{
    const char *bufp = (const char *)buf;
    ssize_t remain = size;
    do
    {
        ssize_t bytes = _endp - _freep;
        if (bytes < remain)
        {
            bytes += _nextp - _bufp;
            if (bytes < MinBufSize)
            {
                Error::setLastError(_errorDomain(), 'I', "buffer overflow");
                return -1;
            }
            ssize_t next = _freep - _nextp;
            memcpy(_bufp, _nextp, next);
            _nextp = _bufp;
            _freep = _bufp + next;
        }
        if (bytes > remain)
            bytes = remain;
        memcpy(_freep, buf, bytes);
        _freep += bytes; bufp += bytes; remain -= bytes;
        if (-1 == _parseBuffer(0 == bytes))
            return -1;
    } while (0 < remain);
    return size;
}
ssize_t TextParser::parse()
{
    ssize_t bytes = _endp - _freep;
    if (bytes < MinBufSize)
    {
        bytes += _nextp - _bufp;
        if (bytes < MinBufSize)
        {
            Error::setLastError(_errorDomain(), 'I', "buffer overflow");
            return -1;
        }
        ssize_t next = _freep - _nextp;
        memcpy(_bufp, _nextp, next);
        _nextp = _bufp;
        _freep = _bufp + next;
    }
    bytes = readBuffer(_freep, _endp - _freep);
    if (-1 == bytes)
        return -1;
    _freep += bytes;
    if (-1 == _parseBuffer(0 == bytes))
        return -1;
    return bytes;
}
const char *TextParser::_errorDomain()
{
    return TextParser::errorDomain();
}
ssize_t TextParser::readBuffer(void *buf, size_t size)
{
    if (0 == _stream)
        return 0;
    return _stream->read(buf, size);
}
void TextParser::parseBuffer(bool eof)
{
}
void TextParser::raise()
{
    _raisemsg = 0;
#if defined(BASE_CONFIG_POSIX)
    siglongjmp(_raisebuf, 1);
#else
    longjmp(_raisebuf, 1);
#endif
}
void TextParser::raise(const char *format, ...)
{
    va_list ap;
    va_start(ap, format);
    CString *str = CString::vcreatef(format, ap);
    str->autorelease();
    va_end(ap);
    _raisemsg = str->chars();
#if defined(BASE_CONFIG_POSIX)
    siglongjmp(_raisebuf, 1);
#else
    longjmp(_raisebuf, 1);
#endif
}
void TextParser::expect(const char *s, size_t len)
{
    if (_nextp + len > _freep)
        raise();
    for (const char *p = s, *endp = p + len; endp > p; p++)
    {
        char c = *_nextp;
        if (c != *p)
            raise("syntax error: expected \'%s\'", s);
        _nextp++;
    }
}
int TextParser::_parseBuffer(bool eof)
{
#if defined(BASE_CONFIG_POSIX)
    if (0 == sigsetjmp(_raisebuf, 0))
#else
    if (0 == setjmp(_raisebuf))
#endif
    {
        parseBuffer(eof);
        return 0;
    }
    else
    {
        const char *message = _raisemsg;
        if (0 == message)
        {
            if (!eof)
                return 0;
            message = "syntax error: unexpected end of file";
        }
        Error::setLastError(_errorDomain(), 'P', message);
        return -1;
    }
}

}
}
