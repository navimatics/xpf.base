/*
 * Base/impl/CStringFnmatch.cpp
 *
 * Cross-platform fnmatch(3) routine with curly braces ('{') support.
 *
 * The original fnmatch(3) code was taken from FreeBSD:
 *     http://www.freebsd.org/cgi/cvsweb.cgi/src/sys/libkern/fnmatch.c?rev=1.21
 *
 * The original copyright notice follows this one.
 *
 * Copyright 2010-2013 Navimatics Corporation. All rights reserved.
 */
/*-
 * Copyright (c) 1989, 1993, 1994
 *	The Regents of the University of California.  All rights reserved.
 *
 * This code is derived from software contributed to Berkeley by
 * Guido van Rossum.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 * 4. Neither the name of the University nor the names of its contributors
 *    may be used to endorse or promote products derived from this software
 *    without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE REGENTS AND CONTRIBUTORS ``AS IS'' AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED.  IN NO EVENT SHALL THE REGENTS OR CONTRIBUTORS BE LIABLE
 * FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS
 * OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 * HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 * LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY
 * OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF
 * SUCH DAMAGE.
 */

#include <Base/impl/CString.hpp>
#include <Base/impl/CArray.hpp>
#include <Base/impl/Log.hpp>
#if defined(BASE_CONFIG_WINDOWS) || defined(BASE_CONFIG_POSIX)
#else
#error CStringFnmatch.cpp not implemented for this platform
#endif

namespace Base {
namespace impl {

#define	FNM_NOMATCH                     1       /* match failed */

#define	FNM_NOESCAPE                    FnmNoEscape
#define	FNM_PATHNAME                    FnmPathname
#define	FNM_PERIOD                      FnmPeriod
#define	FNM_LEADING_DIR                 FnmLeadingDir
#define	FNM_CASEFOLD                    FnmCaseFold

/*
 * Function fnmatch() as specified in POSIX 1003.2-1992, section B.6.
 * Compares a filename or pathname to a pattern.
 */

#define	EOS	'\0'

#define RANGE_MATCH     1
#define RANGE_NOMATCH   0
#define RANGE_ERROR     (-1)

static int rangematch(const char *, char, int, char **);

int
fnmatch(const char *pattern, const char *string, int flags)
{
	const char *stringstart;
	char *newp;
	char c, test;

	for (stringstart = string;;)
		switch (c = *pattern++) {
		case EOS:
			if ((flags & FNM_LEADING_DIR) && *string == '/')
				return (0);
			return (*string == EOS ? 0 : FNM_NOMATCH);
		case '?':
			if (*string == EOS)
				return (FNM_NOMATCH);
			if (*string == '/' && (flags & FNM_PATHNAME))
				return (FNM_NOMATCH);
			if (*string == '.' && (flags & FNM_PERIOD) &&
			    (string == stringstart ||
			    ((flags & FNM_PATHNAME) && *(string - 1) == '/')))
				return (FNM_NOMATCH);
			++string;
			break;
		case '*':
			c = *pattern;
			/* Collapse multiple stars. */
			while (c == '*')
				c = *++pattern;

			if (*string == '.' && (flags & FNM_PERIOD) &&
			    (string == stringstart ||
			    ((flags & FNM_PATHNAME) && *(string - 1) == '/')))
				return (FNM_NOMATCH);

			/* Optimize for pattern with * at end or before /. */
			if (c == EOS)
				if (flags & FNM_PATHNAME)
					return ((flags & FNM_LEADING_DIR) ||
					    strchr(string, '/') == NULL ?
					    0 : FNM_NOMATCH);
				else
					return (0);
			else if (c == '/' && flags & FNM_PATHNAME) {
				if ((string = strchr(string, '/')) == NULL)
					return (FNM_NOMATCH);
				break;
			}

			/* General case, use recursion. */
			while ((test = *string) != EOS) {
				if (!fnmatch(pattern, string, flags & ~FNM_PERIOD))
					return (0);
				if (test == '/' && flags & FNM_PATHNAME)
					break;
				++string;
			}
			return (FNM_NOMATCH);
		case '[':
			if (*string == EOS)
				return (FNM_NOMATCH);
			if (*string == '/' && (flags & FNM_PATHNAME))
				return (FNM_NOMATCH);
			if (*string == '.' && (flags & FNM_PERIOD) &&
			    (string == stringstart ||
			    ((flags & FNM_PATHNAME) && *(string - 1) == '/')))
				return (FNM_NOMATCH);

			switch (rangematch(pattern, *string, flags, &newp)) {
			case RANGE_ERROR:
				goto norm;
			case RANGE_MATCH:
				pattern = newp;
				break;
			case RANGE_NOMATCH:
				return (FNM_NOMATCH);
			}
			++string;
			break;
		case '\\':
			if (!(flags & FNM_NOESCAPE)) {
				if ((c = *pattern++) == EOS) {
					c = '\\';
					--pattern;
				}
			}
			/* FALLTHROUGH */
		default:
		norm:
			if (c == *string)
				;
			else if ((flags & FNM_CASEFOLD) &&
				 (tolower((unsigned char)c) ==
				  tolower((unsigned char)*string)))
				;
			else
				return (FNM_NOMATCH);
			string++;
			break;
		}
	/* NOTREACHED */
}

static int
rangematch(const char *pattern, char test, int flags, char **newp)
{
	int negate, ok;
	char c, c2;

	/*
	 * A bracket expression starting with an unquoted circumflex
	 * character produces unspecified results (IEEE 1003.2-1992,
	 * 3.13.2).  This implementation treats it like '!', for
	 * consistency with the regular expression syntax.
	 * J.T. Conklin (conklin@ngai.kaleida.com)
	 */
	if ( (negate = (*pattern == '!' || *pattern == '^')) )
		++pattern;

	if (flags & FNM_CASEFOLD)
		test = tolower((unsigned char)test);

	/*
	 * A right bracket shall lose its special meaning and represent
	 * itself in a bracket expression if it occurs first in the list.
	 * -- POSIX.2 2.8.3.2
	 */
	ok = 0;
	c = *pattern++;
	do {
		if (c == '\\' && !(flags & FNM_NOESCAPE))
			c = *pattern++;
		if (c == EOS)
			return (RANGE_ERROR);

		if (c == '/' && (flags & FNM_PATHNAME))
			return (RANGE_NOMATCH);

		if (flags & FNM_CASEFOLD)
			c = tolower((unsigned char)c);

		if (*pattern == '-'
		    && (c2 = *(pattern+1)) != EOS && c2 != ']') {
			pattern += 2;
			if (c2 == '\\' && !(flags & FNM_NOESCAPE))
				c2 = *pattern++;
			if (c2 == EOS)
				return (RANGE_ERROR);

			if (flags & FNM_CASEFOLD)
				c2 = tolower((unsigned char)c2);

			if (c <= test && test <= c2)
				ok = 1;
		} else if (c == test)
			ok = 1;
	} while ((c = *pattern++) != ']');

	*newp = (char *)(uintptr_t)pattern;
	return (ok == negate ? RANGE_NOMATCH : RANGE_MATCH);
}

static inline bool bracepat_p(const char *p, bool esc)
{
    for (const char *q = p;; q++)
        switch (*q)
        {
        case '\\':
            if (esc)
                if (q[1])
                    q++;
            break;
        case '{':
            return true;
        case '\0':
            return false;
        }
}
static inline size_t bracepat_part(const char *p, const char *endp, char ch, bool esc)
{
    unsigned level = 0;
    const char *q = p;
    for (; endp > q; q++)
    {
        if (0 == level && ch == *q)
            return q - p;
        switch (*q)
        {
        case '\\':
            if (esc)
                if (q[1])
                    q++;
            break;
        case '{':
            level++;
            break;
        case '}':
            level--;
            break;
        }
    }
    return q - p;
}
static const char **bracepat_combine(const char **a, const char **b)
{
    /*
     * This function returns a new array that combines the strings in arrays ''a'' and ''b'',
     * by concatenating them pair-wise. The returned array and its elements are *not* autoreleased.
     * Furthermore, the ''a'' and the ''b'' array and their elements will be released.
     */
    size_t alen = carr_length(a);
    size_t blen = carr_length(b);
    assert(0 < alen * blen);
    const char **expansions = (const char **)carr_newWithCapacity(alen * blen, sizeof(const char *));
    carr_obj(expansions)->setLength(alen * blen);
    for (const char **p = a, **endp = a + alen; endp > p; p++)
    {
        size_t plen = cstr_length(*p);
        for (const char **q = b, **endq = b + blen; endq > q; q++)
        {
            size_t qlen = cstr_length(*q);
            const char *s = cstr_new(0, plen + qlen);
            memcpy((char *)s, *p, plen);
            memcpy((char *)s + plen, *q, qlen);
            expansions[(p - a) * blen + (q - b)] = s;
        }
        cstr_release(*p);
    }
    carr_release(a);
    for (const char **q = b, **endq = b + blen; endq > q; q++)
        cstr_release(*q);
    carr_release(b);
    return expansions;
}
static const char **bracepat_expand(const char *p, const char *endp, bool esc)
{
    /*
     * This function returns an array that contains the expansions of the brace pattern.
     * The returned array and its elements are *not* autoreleased.
     */
    const char **expansions = (const char **)carr_new(0, 1, sizeof(const char *));
    size_t len = bracepat_part(p, endp, '{', esc);
    expansions[0] = cstr_new(p, len);
    if (p + len < endp)
    {
        p += len + 1;
        len = bracepat_part(p, endp, '}', esc);
        if (p + len < endp)
        {
            const char **grpexp = 0;
            const char *grpendp = p + len;
            for (;;)
            {
                len = bracepat_part(p, grpendp, ',', esc);
                const char **prtexp = bracepat_expand(p, p + len, esc);
                if (0 == grpexp)
                    grpexp = prtexp;
                else
                {
                    grpexp = (const char **)carr_concat(grpexp, prtexp, carr_length(prtexp),
                        sizeof(const char *));
                    carr_release(prtexp);
                }
                p += len + 1;
                if (p == grpendp + 1)
                    break;
                assert(p <= grpendp);
            }
            assert(0 == grpexp || 0 < carr_length(grpexp));
            if (0 < carr_length(grpexp))
                expansions = bracepat_combine(expansions, grpexp);
            if (p < endp)
                expansions = bracepat_combine(expansions, bracepat_expand(p, endp, esc));
        }
        else
            expansions[0] = cstr_concat(expansions[0], p, len);
    }
    return expansions;
}
bool cstr_fnmatch(const char *bracepat, const char *string, int flags)
{
#if defined(BASE_CONFIG_WINDOWS)
    char strbuf[_MAX_PATH];
    if (flags & FnmPathname)
    {
        const char *strp = string;
        char *bufp = strbuf;
        while (char c = *strp)
        {
            if ('\\' == c)
                *bufp = '/';
            else
                *bufp = c;
            bufp++; strp++;
        }
        *bufp++ = '\0';
        string = strbuf;
    }
#endif
    bool esc = !(flags & FnmNoEscape);
    bool brc = flags & FnmCurlyBraces;
    flags &= ~FnmCurlyBraces;
    if (brc && bracepat_p(bracepat, esc))
    {
        bool result = false;
        const char **expansions = bracepat_expand(bracepat, bracepat + strlen(bracepat), esc);
#if 0 //!defined(NDEBUG)
        {
            const char *logstr = cstr_new(bracepat);
            for (const char **p = expansions, **endp = p + carr_length(p); endp > p; p++)
            {
                logstr = cstr_concat(logstr, "\n    ");
                logstr = cstr_concat(logstr, *p);
            }
            LOG("%s", logstr);
            cstr_release(logstr);
        }
#endif
        for (const char **p = expansions, **endp = p + carr_length(p); endp > p; p++)
        {
            result = result || 0 == fnmatch(*p, string, flags);
            cstr_release(*p);
        }
        carr_release(expansions);
        return result;
    }
    else
        return 0 == fnmatch(bracepat, string, flags);
}

}
}
