/*
 * Base/impl/Log.cpp
 *
 * Copyright 2010-2013 Navimatics Corporation. All rights reserved.
 */

#include <Base/impl/Log.hpp>
#include <Base/impl/ApplicationInfo.hpp>
#include <Base/impl/Tls.hpp>
#include <Base/impl/hash_map.hpp>
#if defined(BASE_CONFIG_WINDOWS)
#include <Base/impl/Syn.hpp>
#include <windows.h>
#include <dbghelp.h>
#elif defined(BASE_CONFIG_DARWIN)
#include <cxxabi.h>
#include <dlfcn.h>
#include <execinfo.h>
#include <mach/mach_time.h>
#elif defined(BASE_CONFIG_ANDROID)
#include <android/log.h>
#endif
#include "trio.h"
    /* We cannot use <Base/impl/Snprintf.hpp>, because it uses Log facilities to report errors. */

#define LOGBUFSIZ                       16384

namespace Base {
namespace impl {

/*
 * LOG()
 */

#if defined(BASE_CONFIG_DARWIN)
extern "C" {
void *CFStringCreateWithCString(void *alloc, const char *s, unsigned int encoding);
void NSLog(void *format, ...);
}
#endif
static void SysLog(char *buf, char *bufp, size_t bufsiz,
    const char *format, va_list ap, const char *trailer)
{
    assert(0 != buf);
    assert(0 != bufp);
    assert(0 != bufsiz);
    ssize_t len = trio_vsnprintf(bufp, bufsiz - (bufp - buf), format, ap);
    if (0 <= len)
    {
        bufp += len;
        if (0 != trailer)
        {
            len = strlen(trailer);
            if (len > bufsiz - (bufp - buf))
                len = bufsiz - (bufp - buf);
            memcpy(bufp, trailer, len);
            bufp += len;
        }
    }
    else
    {
        len = trio_snprintf(buf, bufsiz, "Base::impl::SysLog() bad format: %s", format);
        assert(0 <= len);
        bufp = buf + len;
    }
    if (buf < bufp)
    {
#if defined(BASE_CONFIG_DARWIN) || defined(BASE_CONFIG_ANDROID)
        /* remove terminating newline (if present) */
        if ('\n' == bufp[-1])
        {
            *--bufp = '\0';
            if ('\r' == bufp[-1])
                *--bufp = '\0';
        }
#elif defined(BASE_CONFIG_WINDOWS) || defined(BASE_CONFIG_POSIX)
        /* append terminating newline (if not present) */
        if ('\n' != bufp[-1] && bufp < buf + bufsiz - 1)
            *bufp++ = '\n';
#endif
    }
    if (bufp < buf + bufsiz)
        *bufp = '\0';
    else
        buf[bufsiz - 1] = '\0';
#if defined(BASE_CONFIG_WINDOWS)
    OutputDebugString(buf);
#elif defined(BASE_CONFIG_DARWIN)
    static void *cfformat = CFStringCreateWithCString(0, "%s", /*kCFStringEncodingUTF8*/0x08000100);
        /* We do not use execute_once, because one day it may use Log facilities to report errors.
         * However the Mac OS X C++ compiler (g++) implements thread-safe statics, so this is ok.
         */
    NSLog(cfformat, buf);
#elif defined(BASE_CONFIG_ANDROID)
    __android_log_print(ANDROID_LOG_INFO, GetApplicationName(), "%s", buf);
#elif defined(BASE_CONFIG_POSIX)
    fwrite(buf, 1, strlen(buf), stderr);
#endif
}

void Log(const char *format, ...)
{
    va_list ap;
    va_start(ap, format);
    Logv(format, ap);
    va_end(ap);
}
void Logv(const char *format, va_list ap)
{
    char buf[LOGBUFSIZ];
    SysLog(buf, buf, sizeof buf, format, ap, 0);
}

static void __LogWithTagv(const char *tag, const char *format, va_list ap, const char *trailer);
void __LogWithTag(const char *tag, const char *format, ...)
{
    va_list ap;
    va_start(ap, format);
    __LogWithTagv(tag, format, ap, 0);
    va_end(ap);
}
void __LogWithTagv(const char *tag, const char *format, va_list ap, const char *trailer)
{
    char buf[LOGBUFSIZ], *bufp = buf;
    const char *p = strchr(tag, '(');
    if (0 != p)
    {
        const char *q = strchr(p + 1, ')');
        if (0 != q && '\0' == q[1])
        {
            /* tag is function signature: remove argument types and return type */
            if (p > tag && ']' == p[-1])
            {
                /* Objective-C++ signature: Type -[Class Method](Type,...)*/
                for (q = p - 1; tag <= q; q--)
                    if ('-' == *q || '+' == *q)
                    {
                        tag = q;
                        break;
                    }
                size_t len = p - tag;
                if (128 < len)
                    len = 128;
                memcpy(buf, tag, len);
                bufp = buf + len;
                *bufp++ = ' ';
            }
            else
            {
                /* C++ signature: Type Class::Method(Type,...) */
                for (q = p - 1; tag <= q; q--)
                    if (!(isalnum(*q) || '_' == *q || '$' == *q || ':' == *q))
                    {
                        tag = q + 1;
                        break;
                    }
                size_t len = p - tag;
                if (128 < len)
                    len = 128;
                memcpy(buf, tag, len);
                bufp = buf + len;
                *bufp++ = '(';
                *bufp++ = ')';
                *bufp++ = ' ';
            }
        }
    }
    if (bufp == buf)
    {
        size_t len = strlen(tag);
        if (128 < len)
            len = 128;
        memcpy(buf, tag, len);
        bufp = buf + len;
        *bufp++ = ' ';
    }
    SysLog(buf, bufp, sizeof buf - (bufp - buf), format, ap, trailer);
}
const char *__LogLastPathComponent(const char *path)
{
#if defined(BASE_CONFIG_WINDOWS)
    const char *p = strrchr(path, '\\');
#else
    const char *p = strrchr(path, '/');
#endif
    return 0 != p ? p + 1 : path;
}

/*
 * LOG_CALLSTACK()
 */

#if defined(BASE_CONFIG_WINDOWS)
#pragma comment(lib, "dbghelp.lib")
#endif
#define MAXADDR                         62 /* max number of frames to capture (Windows restriction) */
#define SYMFMT                          "\n    %-3u %-16s %p %s + %u"
#define ADDRFMT                         "\n    %-3u %p"
void __LogCallStack(const char *tag, const char *format, ...)
{
    char *buf = (char *)Malloc(LOGBUFSIZ), *bufp = buf;
#if defined(BASE_CONFIG_WINDOWS)
    void *addrs[MAXADDR];
    size_t naddrs = CaptureStackBackTrace(0, NELEMS(addrs), addrs, 0);
    if (0 == naddrs)
        bufp += trio_snprintf(bufp, LOGBUFSIZ - (bufp - buf), "\n    missing call stack");
    else
    {
        /* SymInitialize()/SymFromAddr() are not thread-safe. Furthermore SymInitialize() should
         * not be called more than once per process.
         *
         * We use the low level facilities in Syn.hpp for execute_once and lock functionality.
         * We do not use higher-level facilities, because one day they may use Log to report errors.
         */
        HANDLE hproc = GetCurrentProcess();
        static mutex_t mutex;
        static execute_once_control_t SymInitialize_once = 0;
        if (!SymInitialize_once && execute_once_enter(&SymInitialize_once))
        {
            mutex_init(&mutex);
            SymInitialize(hproc, 0, TRUE);
            execute_once_leave(&SymInitialize_once);
        }
        SYMBOL_INFO *syminfo = (SYMBOL_INFO *)Malloc(sizeof(SYMBOL_INFO) + 1024);
        memset(syminfo, 0, sizeof(SYMBOL_INFO));
        syminfo->SizeOfStruct = sizeof(SYMBOL_INFO);
        syminfo->MaxNameLen = 1024 - 1;
        mutex_lock(&mutex);
        for (void **p = addrs + 1/* skip this frame */, **endp = addrs + naddrs; endp > p; p++)
        {
            DWORD64 displacement = 0;
            if (SymFromAddr(hproc, (DWORD64)*p, &displacement, syminfo))
            {
                char filename[MAX_PATH]; filename[0] = '\0';
                GetModuleFileNameA((HMODULE)syminfo->ModBase, filename, MAX_PATH);
                const char *basename = __LogLastPathComponent(filename);
                bufp += trio_snprintf(bufp, LOGBUFSIZ - (bufp - buf), SYMFMT,
                    p - addrs, basename, *p, syminfo->Name, (unsigned)displacement);
            }
            else
                bufp += trio_snprintf(bufp, LOGBUFSIZ - (bufp - buf), ADDRFMT,
                    p - addrs, *p);
        }
        mutex_unlock(&mutex);
        free(syminfo);
    }
#elif defined(BASE_CONFIG_DARWIN)
    void *addrs[MAXADDR];
    size_t naddrs = backtrace(addrs, NELEMS(addrs));
    if (0 == naddrs)
        bufp += trio_snprintf(bufp, LOGBUFSIZ - (bufp - buf), "\n    missing call stack");
    else
    {
        Dl_info syminfo;
        size_t symsiz = 0;
        char *symbuf = 0;
        for (void **p = addrs + 1/* skip this frame */, **endp = addrs + naddrs; endp > p; p++)
        {
            if (dladdr(*p, &syminfo))
            {
                if (0 != syminfo.dli_fname)
                    syminfo.dli_fname = __LogLastPathComponent(syminfo.dli_fname);
                else
                    syminfo.dli_fname = "";
                if (0 != syminfo.dli_sname)
                {
                    int status = 0;
                    if (char *sym = abi::__cxa_demangle(syminfo.dli_sname, symbuf, &symsiz, &status))
                    {
                        syminfo.dli_sname = sym;
                        symbuf = sym;
                    }
                }
                else
                    syminfo.dli_sname = "";
                unsigned displacement = 0;
                if (0 != syminfo.dli_saddr)
                    displacement = (char *)*p - (char *)syminfo.dli_saddr;
                bufp += trio_snprintf(bufp, LOGBUFSIZ - (bufp - buf), SYMFMT,
                    p - addrs, syminfo.dli_fname, *p, syminfo.dli_sname, displacement);
            }
            else
                bufp += trio_snprintf(bufp, LOGBUFSIZ - (bufp - buf), ADDRFMT,
                    p - addrs, *p);
        }
        free(symbuf);
    }
#elif defined(BASE_CONFIG_ANDROID)
    /* don't know how to do this on Android yet! */
    bufp += trio_snprintf(bufp, LOGBUFSIZ - (bufp - buf), "\n    missing call stack");
#elif defined(BASE_CONFIG_POSIX)
    void *addrs[MAXADDR];
    size_t naddrs = backtrace(addrs, NELEMS(addrs));
    if (0 == naddrs)
        bufp += trio_snprintf(bufp, LOGBUFSIZ - (bufp - buf), "\n    missing call stack");
    else if (char **syms = backtrace_symbols(addrs, naddrs))
    {
        for (char **p = syms + 1/* skip this frame */, **endp = syms + naddrs; endp > p; p++)
            bufp += trio_snprintf(bufp, LOGBUFSIZ - (bufp - buf), "\n    %s", *p);
        free(syms);
    }
#else
    bufp += trio_snprintf(bufp, LOGBUFSIZ - (bufp - buf), "\n    missing call stack");
#endif
    va_list ap;
    va_start(ap, format);
    __LogWithTagv(tag, format, ap, buf);
    va_end(ap);
    free(buf);
}

/*
 * LOG_PERF()
 */
 
#if defined(BASE_CONFIG_WINDOWS)
#pragma comment(lib, "winmm.lib")
#endif
struct __LogPerfData
{
#if defined(BASE_CONFIG_WINDOWS)
    DWORD startTime;
    DWORD totalElapsed;
    unsigned long invocations;
#elif defined(BASE_CONFIG_DARWIN)
    uint64_t startTime;
    uint64_t totalElapsed;
    unsigned long invocations;
#elif defined(BASE_CONFIG_POSIX)
    uint64_t startTime;
    uint64_t totalElapsed;
    unsigned long invocations;
#endif
};
typedef hash_map<const char *, __LogPerfData> __LogPerfData_map_t;
static void __LogPerfTlsFree(void *p)
{
    delete (__LogPerfData_map_t *)p;
}
static tlskey_t __LogPerfTlsKey() BASE_FUNCCONST;
static tlskey_t __LogPerfTlsKey()
{
    static tlskey_t tlskey = tlsalloc(__LogPerfTlsFree);
        /* threadsafe: __forcecall__LogPerfTlsKey */
    return tlskey;
}
static tlskey_t __forcecall__LogPerfTlsKey = __LogPerfTlsKey();
void __LogPerfBegin(const char *lit)
{
    tlskey_t tlskey = __LogPerfTlsKey();
    __LogPerfData_map_t *map = (__LogPerfData_map_t *)tlsget(tlskey);
    if (0 == map)
    {
        map = new __LogPerfData_map_t;
        tlsset(tlskey, map);
    }
    __LogPerfData &data = (*map)[lit];
#if defined(BASE_CONFIG_WINDOWS)
    data.startTime = timeGetTime();
#elif defined(BASE_CONFIG_DARWIN)
    data.startTime = mach_absolute_time();
#elif defined(BASE_CONFIG_POSIX)
    struct timespec ts;
    clock_gettime(CLOCK_MONOTONIC, &ts);
    data.startTime = (uint64_t)ts.tv_sec * 1000000000L + (uint64_t)ts.tv_nsec;
#endif
}
void __LogPerfEnd(const char *lit, const char *tag, const char *name)
{
#if defined(BASE_CONFIG_WINDOWS)
    DWORD currentTime = timeGetTime();
#elif defined(BASE_CONFIG_DARWIN)
    uint64_t currentTime = mach_absolute_time();
#elif defined(BASE_CONFIG_POSIX)
    struct timespec ts;
    clock_gettime(CLOCK_MONOTONIC, &ts);
    uint64_t currentTime = (uint64_t)ts.tv_sec * 1000000000L + (uint64_t)ts.tv_nsec;
#endif
    tlskey_t tlskey = __LogPerfTlsKey();
    __LogPerfData_map_t *map = (__LogPerfData_map_t *)tlsget(tlskey);
    if (0 == map)
        return;
    __LogPerfData &data = (*map)[lit];
#if defined(BASE_CONFIG_WINDOWS)
    DWORD elapsed = currentTime - data.startTime;
    data.totalElapsed += elapsed;
    data.invocations++;
    unsigned long thistime = elapsed;
    unsigned long totaltime = data.totalElapsed;
    unsigned long avgtime = totaltime / data.invocations;
#elif defined(BASE_CONFIG_DARWIN)
    static mach_timebase_info_data_t info;
    if (0 == info.denom)
        /* Ok if this gets executed in multiple threads as the result returned is the same. */
        mach_timebase_info(&info);
    uint64_t elapsed = currentTime - data.startTime;
    data.totalElapsed += elapsed;
    data.invocations++;
    unsigned long thistime = (unsigned long)((elapsed * info.numer / info.denom) / 1000000);
    unsigned long totaltime = (unsigned long)((data.totalElapsed * info.numer / info.denom) / 1000000);
    unsigned long avgtime = totaltime / data.invocations;
#elif defined(BASE_CONFIG_POSIX)
    uint64_t elapsed = currentTime - data.startTime;
    data.totalElapsed += elapsed;
    data.invocations++;
    unsigned long thistime = elapsed / 1000000;
    unsigned long totaltime = data.totalElapsed / 1000000;
    unsigned long avgtime = totaltime / data.invocations;
#endif
    if (0 == name || '\0' == name[0])
        __LogWithTag(tag, "this %lums avg %lums", thistime, avgtime);
    else
        __LogWithTag(tag, "%s: this %lums avg %lums", name, thistime, avgtime);
}

}
}
