/*
 * Base/impl/DateTime.cpp
 *
 * Please note: this implementation does not currently support leap seconds.
 *
 * Copyright 2010-2013 Navimatics Corporation. All rights reserved.
 */

#include <Base/impl/DateTime.hpp>
#include <Base/impl/CString.hpp>
#if defined(BASE_CONFIG_WINDOWS)
#include <windows.h>
#elif defined(BASE_CONFIG_POSIX)
#include <sys/time.h>
#endif

#define MSecUnit                        1
#define SecUnit                         1000
#define MinUnit                         (60 * SecUnit)
#define HourUnit                        (3600 * SecUnit)
#define DayUnit                         (86400 * SecUnit)
#define GregorianEpoch                  ((int64_t)198647467200000LL)
#define WindowsEpoch                    ((int64_t)199222286400000LL)
#define UnixEpoch                       ((int64_t)210866760000000LL)

namespace Base {
namespace impl {

static inline int64_t currentDateTime()
{
#if defined(BASE_CONFIG_WINDOWS)
    FILETIME ft;
    GetSystemTimeAsFileTime(&ft);
        /* FILETIME contains a 64-bit value representing the number of
         * 100-nanosecond intervals since January 1, 1601 (UTC).
         */
    return WindowsEpoch +
        (int64_t)((uint64_t)ft.dwLowDateTime | ((uint64_t)ft.dwHighDateTime << 32)) / 10000;
#elif defined(BASE_CONFIG_POSIX)
    struct timeval tv;
    gettimeofday(&tv, 0);
    return UnixEpoch + (int64_t)tv.tv_sec * 1000 + (int64_t)tv.tv_usec / 1000;
#else
    time_t unixTime = time(0);
    return UnixEpoch + (int64_t)unixTime * 1000;
#endif
}

/* Julian date calculations, see http://www.tondering.dk/claus/cal/julperiod.php */
const char *DateTime::format(Components components, bool extended)
{
    return cstringf(extended ? "%04i-%02i-%02iT%02i:%02i:%02i.%03iZ" : "%04i%02i%02iT%02i%02i%02i.%03iZ",
        components.year, components.month, components.day,
        components.hour, components.min, components.sec, components.msec);
}
DateTime::Components DateTime::parse(const char *s, const char **endp)
{
    /* see ISO 8601:2004(E), Sections 4.1, 4.2, 4.3 */
#define PARSESGN(deflt_stmt)            \
    switch (*s)\
    {\
    case '+':\
        sgn = +1;\
        s++;\
        break;\
    case '-':\
        sgn = -1;\
        s++;\
        break;\
    default:\
        deflt_stmt;\
        break;\
    }
#define PARSENUM(count)                 \
    do\
    {\
        num = 0;\
        const char *p = s, *ep = p + count;\
        while (ep > p && isdigit(*p))\
            num = num * 10 + (*p++ - '0');\
        len = p - s;\
        s = p;\
    } while (false);
#define SKIPCH(ch)                      s += *s == ch;
    Components components =
    {
        None(), 1, 1,
        0
    };
    int doy = None(), wkn = None(), dow = 1;
    int hms = 0, tzhour = 0, tzmin = 0;
    int sgn, num, len;
    /* whitespace */
    while (isspace(*s))
        s++;
    /* year / century */
    PARSESGN(sgn = +1);
    PARSENUM(4);
    if (4 == len)
        /* year */
        components.year = sgn * num;
    else if (2 == len)
    {
        /* century */
        components.year = sgn * num * 100;
        goto dateDone;
    }
    else
    {
        s -= len;
        goto error;
    }
    SKIPCH('-');
    /* month, day / ordinal*/
    PARSENUM(4);
    if (4 == len)
    {
        /* month, day */
        components.month = num / 100;
        components.day = num % 100;
    }
    else if (3 == len)
        /* day of year */
        doy = num;
    else if (2 == len)
    {
        /* month */
        components.month = num;
        if ('-' != *s)
            goto dateDone;
        s++;
        /* day */
        PARSENUM(2);
        if (2 == len)
            components.day = num;
        else
        {
            s -= len;
            goto error;
        }
    }
    else if (0 == len)
    {
        if ('W' != *s)
            goto dateDone;
        s++;
        /* week */
        PARSENUM(2);
        if (2 == len)
            /* week number */
            wkn = num;
        else
        {
            s -= len + 1;
            goto error;
        }
        SKIPCH('-');
        /* day of week */
        PARSENUM(1);
        if (1 == len)
            /* day of week */
            dow = num;
        else
            goto dateDone;
    }
    else
    {
        s -= len;
        goto error;
    }
dateDone:
    if ('T' != *s)
        goto done;
    s++;
    /* hour */
    PARSENUM(2);
    if (2 == len)
    {
        components.hour = num;
        hms = 'h';
    }
    else
    {
        s -= len + 1;
        goto error;
    }
    SKIPCH(':');
    /* min */
    PARSENUM(2);
    if (2 == len)
    {
        components.min = num;
        hms = 'm';
    }
    else if (0 == len)
        goto timeDone;
    else
    {
        s -= len;
        goto error;
    }
    SKIPCH(':');
    /* sec */
    PARSENUM(2);
    if (2 == len)
    {
        components.sec = num;
        hms = 's';
    }
    else if (0 == len)
        goto timeDone;
    else
    {
        s -= len;
        goto error;
    }
timeDone:
    if (',' != *s && '.' != *s)
        goto fracDone;
    s++;
    /* fraction */
    PARSENUM(9);
    if (1 <= len)
    {
        double frac = num;
        for (int i = 0; len > i; i++)
            frac *= 0.1;
        switch (hms)
        {
        case 'h':
            components.min = 60 * frac + 0.5;
            break;
        case 'm':
            components.sec = 60 * frac + 0.5;
            break;
        case 's':
            components.msec = 1000 * frac + 0.5;
            break;
        }
    }
    else
    {
        s -= len + 1;
        goto error;
    }
fracDone:
    if ('Z' == *s)
    {
        s++;
        goto done;
    }
    PARSESGN(goto done);
    /* timezone hour diff */
    PARSENUM(2);
    if (2 == len)
        tzhour = sgn * num;
    else
    {
        s -= len + 1;
        goto error;
    }
    SKIPCH(':');
    /* timezone min diff */
    PARSENUM(2);
    if (2 == len)
        tzmin = num;
    else if (0 == len)
        goto done;
    else
    {
        s -= len;
        goto error;
    }
done:
error:
    if (!isNone(components.year))
    {
        double j = julianDateFromComponents(components);
        if (!isNone(doy))
            j += doy - 1;
        else if (!isNone(wkn))
        {
            /*
             * According to ISO 8601:2004 and http://www.tondering.dk/claus/cal/week.php
             * "Week 1 of any year is the week that contains 4 January".
             * We must therefore compute the beginning of the week that contains Jan 4.
             *
             * We have already computed the Julian date of Jan 1 in ''(int)(j + 0.5)'',
             * so ''(int)(j + 0.5) + 3'' is the Julian date of Jan 4. The expression
             * ''((int)(j + 0.5) + 3) % 7 + 1'' yields the day of the week according to
             * ISO 8601:2004.
             *
             * To compute the beginning of the week subtract the day of the week from Jan 4.
             * Note that ISO days of week are 1-based, so we must remember to also subtract 1.
             */
            int isoDayOfWeek = ((int)(j + 0.5) + 3) % 7 + 1;
            j += 3 - (isoDayOfWeek - 1);
            /*
             * Compute the final Julian date keeping in mind that week number and
             * day of week ordinals are all 1-based.
             */
            j += (wkn - 1) * 7 + (dow - 1);
        }
        j -= (double)tzhour / 24 + (double)tzmin / (24 * 60);
        components = componentsFromJulianDate(j);
    }
    if (0 != endp)
        *endp = s;
    return components;
#undef SKIPCH
#undef PARSENUM
#undef PARSESGN
}
double DateTime::julianDateFromComponents(Components components)
{
    double j;
    if (components.year * 400 + components.month * 40 + components.day >= 1582 * 400 + 10 * 40 + 15)
    {
        /* gregorian calendar */
        int a = (14 - components.month) / 12;
        int y = components.year + 4800 - a;
        int m = components.month + 12 * a - 3;
        int JD = components.day + (153 * m + 2) / 5 + 365 * y + y / 4 - y / 100 + y / 400 - 32045;
        j = JD;
    }
    else
    {
        /* julian calendar */
        int a = (14 - components.month) / 12;
        int y = components.year + 4800 - a;
        if (0 > components.year)
            y++;
        int m = components.month + 12 * a - 3;
        int JD = components.day + (153 * m + 2) / 5 + 365 * y + y / 4 - 32083;
        j = JD;
    }
    j += (double)(components.hour - 12) / 24;
    j += (double)components.min / (24 * 60);
    j += (double)components.sec / (24 * 3600);
    j += (double)components.msec / (24 * 3600000);
    return j;
}
DateTime::Components DateTime::componentsFromJulianDate(double j)
{
    Components components;
    int JD = j + 0.5;
    if (2299160.5/* Gregorian Epoch */ <= j)
    {
        /* gregorian calendar */
        int a = JD + 32044;
        int b = (4 * a + 3) / 146097;
        int c = a - 146097 * b / 4;
        int d = (4 * c + 3) / 1461;
        int e = c - 1461 * d / 4;
        int m = (5 * e + 2) / 153;
        components.day = e - (153 * m + 2) / 5 + 1;
        components.month = m + 3 - 12 * (m / 10);
        components.year = 100 * b + d - 4800 + m / 10;
    }
    else
    {
        /* julian calendar */
        int b = 0;
        int c = JD + 32082;
        int d = (4 * c + 3) / 1461;
        int e = c - 1461 * d / 4;
        int m = (5 * e + 2) / 153;
        components.day = e - (153 * m + 2) / 5 + 1;
        components.month = m + 3 - 12 * (m / 10);
        components.year = 100 * b + d - 4800 + m / 10;
        if (0 >= components.year)
            components.year--;
    }
    double dummy;
    int msec = modf(j + 0.5, &dummy) * (24 * 3600000) + 0.5;
    int sec = msec / 1000;
    components.hour = sec / 3600;
    components.min = (sec - components.hour * 3600) / 60;
    components.sec = sec % 60;
    components.msec = msec % 1000;
    components.dayOfWeek = (JD + 1) % 7;
    return components;
}
double DateTime::currentJulianDate()
{
    return (double)currentDateTime() / DayUnit;
}
DateTime::DateTime()
{
    _j = currentDateTime();
}
DateTime::DateTime(Components c)
{
    c2j(c);
}
DateTime::DateTime(int year, int month, int day,
    int hour, int min, int sec, int msec)
{
    Components c =
    {
        year, month, day,
        hour, min, sec, msec
    };
    c2j(c);
}
DateTime::DateTime(double julianDate)
{
    _j = julianDate * DayUnit + 0.5;
}
DateTime::DateTime(time_t unixTime)
{
    _j = UnixEpoch + (int64_t)unixTime * 1000;
}
DateTime::Components DateTime::components()
{
    Components c; j2c(c);
    return c;
}
double DateTime::julianDate()
{
    return (double)_j / DayUnit;
}
time_t DateTime::unixTime()
{
    return (_j - UnixEpoch) / 1000;
}
const char *DateTime::strrepr()
{
    Components c; j2c(c);
    return cstringf("%04i%02i%02iT%02i%02i%02i.%03iZ",
        c.year, c.month, c.day, c.hour, c.min, c.sec, c.msec);
}
void DateTime::c2j(const Components &components)
{
    if (components.year * 400 + components.month * 40 + components.day >= 1582 * 400 + 10 * 40 + 15)
    {
        /* gregorian calendar */
        int a = (14 - components.month) / 12;
        int y = components.year + 4800 - a;
        int m = components.month + 12 * a - 3;
        int JD = components.day + (153 * m + 2) / 5 + 365 * y + y / 4 - y / 100 + y / 400 - 32045;
        _j = (int64_t)JD * DayUnit;
    }
    else
    {
        /* julian calendar */
        int a = (14 - components.month) / 12;
        int y = components.year + 4800 - a;
        if (0 > components.year)
            y++;
        int m = components.month + 12 * a - 3;
        int JD = components.day + (153 * m + 2) / 5 + 365 * y + y / 4 - 32083;
        _j = (int64_t)JD * DayUnit;
    }
    _j += (components.hour - 12) * HourUnit;
    _j += components.min * MinUnit;
    _j += components.sec * SecUnit;
    _j += components.msec * MSecUnit;
}
void DateTime::j2c(Components &components)
{
    int JD = (_j + DayUnit / 2) / DayUnit;
    if (GregorianEpoch <= _j)
    {
        /* gregorian calendar */
        int a = JD + 32044;
        int b = (4 * a + 3) / 146097;
        int c = a - 146097 * b / 4;
        int d = (4 * c + 3) / 1461;
        int e = c - 1461 * d / 4;
        int m = (5 * e + 2) / 153;
        components.day = e - (153 * m + 2) / 5 + 1;
        components.month = m + 3 - 12 * (m / 10);
        components.year = 100 * b + d - 4800 + m / 10;
    }
    else
    {
        /* julian calendar */
        int b = 0;
        int c = JD + 32082;
        int d = (4 * c + 3) / 1461;
        int e = c - 1461 * d / 4;
        int m = (5 * e + 2) / 153;
        components.day = e - (153 * m + 2) / 5 + 1;
        components.month = m + 3 - 12 * (m / 10);
        components.year = 100 * b + d - 4800 + m / 10;
        if (0 >= components.year)
            components.year--;
    }
    int msec = ((_j + DayUnit / 2) % DayUnit) / MSecUnit;
    int sec = msec / SecUnit;
    components.hour = sec / 3600;
    components.min = (sec - components.hour * 3600) / 60;
    components.sec = sec % 60;
    components.msec = msec % 1000;
    components.dayOfWeek = (JD + 1) % 7;
}

}
}
