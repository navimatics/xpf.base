/*
 * Base/impl/FileUtil.cpp
 *
 * Copyright 2010-2013 Navimatics Corporation. All rights reserved.
 */

#include <Base/impl/FileUtil.hpp>
#include <Base/impl/CArray.hpp>
#include <Base/impl/CString.hpp>
#if defined(BASE_CONFIG_WINDOWS)
#include <windows.h>
#elif defined(BASE_CONFIG_POSIX)
#include <dirent.h>
#include <sys/stat.h>
#else
#error FileUtil.cpp not implemented for this platform
#endif

namespace Base {
namespace impl {

#if defined(BASE_CONFIG_WINDOWS)
#define FIND_INVALID_HANDLE             INVALID_HANDLE_VALUE
typedef HANDLE find_handle_t;
typedef WIN32_FIND_DATAA find_data_t;
static inline const char *find_filename(const find_data_t &data)
{
    return data.cFileName;
}
static inline find_handle_t find_first(const char *dirname, find_data_t &data)
{
    return FindFirstFileA(cstr_path_concat(dirname, "*"), &data);
}
static inline bool find_next(find_handle_t h, find_data_t &data)
{
    return FindNextFile(h, &data);
}
static inline void find_close(find_handle_t h)
{
    FindClose(h);
}
#elif defined(BASE_CONFIG_POSIX)
#define FIND_INVALID_HANDLE             0
typedef DIR *find_handle_t;
typedef dirent *find_data_t;
static inline const char *find_filename(const find_data_t &data)
{
    return data->d_name;
}
static inline find_handle_t find_first(const char *dirname, find_data_t &data)
{
    find_handle_t h = opendir(dirname);
    if (0 == h)
        return 0;
    data = readdir(h);
    if (0 == data)
    {
        closedir(h);
        return 0;
    }
    return h;
}
static inline bool find_next(find_handle_t h, find_data_t &data)
{
    return 0 != (data = readdir(h));
}
static inline void find_close(find_handle_t h)
{
    closedir(h);
}
#endif

#if defined(BASE_CONFIG_WINDOWS)
int mkpdirs(const char *path)
{
    size_t len = strlen(path);
    if (MAX_PATH - 1 < len)
    {
        SetLastError(ERROR_BUFFER_OVERFLOW);
        return -1;
    }
    char buf[MAX_PATH], *bufp;
    memcpy(buf, path, len + 1);
    for (bufp = buf + len - 1; buf < bufp; bufp--)
        if ('\\' == *bufp || '/' == *bufp)
        {
            *bufp = '\0';
            BOOL res = CreateDirectoryA(buf, 0);
            *bufp = '\\';
            if (res || ERROR_ALREADY_EXISTS == GetLastError())
            {
                bufp++;
                break;
            }
        }
    for (; *bufp; bufp++)
        if ('\\' == *bufp || '/' == *bufp)
        {
            *bufp = '\0';
            BOOL res = *buf ? CreateDirectoryA(buf, 0) : TRUE;
            *bufp = '\\';
            if (!res)
                return -1;
        }
    return 0;
}
#elif defined(BASE_CONFIG_POSIX)
int mkpdirs(const char *path)
{
    size_t len = strlen(path);
    if (PATH_MAX < len)
    {
        errno = ENAMETOOLONG;
        return -1;
    }
    char buf[PATH_MAX + 1], *bufp;
    memcpy(buf, path, len + 1);
    for (bufp = buf + len - 1; buf < bufp; bufp--)
        if ('/' == *bufp)
        {
            *bufp = '\0';
            int res = mkdir(buf, 0777);
            *bufp = '/';
            if (-1 != res || EEXIST == errno)
            {
                bufp++;
                break;
            }
        }
    for (; *bufp; bufp++)
        if ('/' == *bufp)
        {
            *bufp = '\0';
            int res = *buf ? mkdir(buf, 0777) : 0;
            *bufp = '/';
            if (-1 == res)
                return -1;
        }
    return 0;
}
#endif
const char **listdir(const char *dirname, const char *pattern, int fnmatchFlags)
{
    const char **paths = (const char **)carr_new(0, 0, sizeof(const char *));
    find_data_t data;
    find_handle_t h = find_first(dirname, data);
    if (FIND_INVALID_HANDLE != h)
    {
        do
        {
            const char *filename = find_filename(data);
            if (!('.' == filename[0] && ('\0' == filename[1] || ('.' == filename[1] && '\0' == filename[2]))) &&
                (0 == pattern || cstr_fnmatch(pattern, filename, fnmatchFlags | FnmPathname)))
            {
                const char *s = cstring(filename);
                paths = (const char **)carr_concat(paths, &s, 1, sizeof(const char *));
            }
        } while (find_next(h, data));
        find_close(h);
    }
    carr_autorelease(paths);
    return paths;
}

}
}
