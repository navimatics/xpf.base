/*
 * Base/impl/ErrorFilteredLogger.cpp
 *
 * Copyright 2010-2013 Navimatics Corporation. All rights reserved.
 */

#include <Base/impl/Error.hpp>
#include <Base/impl/CString.hpp>
#include <Base/impl/Log.hpp>
#include <Base/impl/Set.hpp>
#include <Base/impl/Synchronization.hpp>

namespace Base {
namespace impl {

static Lock *GetFilterSetAndLock(Set *&setref)
{
    static Set *set;
    static Lock *lock;
    execute_once
    {
        set = new Set;
        set->autocollectObjects(false);
        lock = new Lock;
    }
    setref = set;
    return lock;
}

void Error::filteredLogger(Error *error)
{
    bool test;
    Set *set;
    synchronized (GetFilterSetAndLock(set))
        test = set->containsObject(cstr_obj(error->_domain));
    if (test)
        Log("Error: %s(%i): %s", error->_domain, error->_code, error->message());
}
void Error::addFilteredLoggerDomain(const char *domain)
{
    Set *set;
    synchronized (GetFilterSetAndLock(set))
    {
        CString *str = CString::create(domain);
        set->addObject(str);
        str->release();
    }
}
void Error::removeFilteredLoggerDomain(const char *domain)
{
    Set *set;
    synchronized (GetFilterSetAndLock(set))
        set->removeObject(BASE_ALLOCA_OBJECT(TransientCString, domain));
}

}
}
