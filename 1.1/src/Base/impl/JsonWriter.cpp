/*
 * Base/impl/JsonWriter.cpp
 *
 * Copyright 2010-2013 Navimatics Corporation. All rights reserved.
 */

#include <Base/impl/JsonWriter.hpp>
#include <Base/impl/CString.hpp>
#include <Base/impl/Snprintf.hpp>

namespace Base {
namespace impl {

enum
{
    /* same as in JsonParser.cpp for symmetry; not all values are used */
    StateValue = 1000,
    StateObjectKey,
    StateObjectColon,
    StateObjectValue,
    StateObjectComma,
    StateArrayValue,
    StateArrayComma,
    StateRestOfString,
};
inline void JsonWriter::_pushstate(int state)
{
    if (_stackendp == _stackp)
    {
        size_t count = _stackp - _stack;
        size_t capacity = 0 == count ? 16 : count * 2; /* initial=16; grow-factor=2.0 */
        void *p = Realloc(_stack, capacity * sizeof(int));
        _stack = (int *)p;
        _stackp = _stack + count;
        _stackendp = _stack + capacity;
    }
    *_stackp++ = state;
}
inline void JsonWriter::_popstate()
{
    --_stackp;
}
inline int JsonWriter::_topstate()
{
    if (_stackp > _stack)
        return _stackp[-1];
    else
        return StateValue;
}
inline void JsonWriter::_topstate(int state)
{
    if (_stackp > _stack)
        _stackp[-1] = state;
}

JsonWriter::JsonWriter(Stream *stream, bool pretty)
{
    if (0 != stream)
        _stream = (Stream *)stream->retain();
    _pretty = pretty;
    _flags = InfinityExtension;
}
JsonWriter::~JsonWriter()
{
    if (0 != _stack)
        free(_stack);
    if (0 != _stream)
        _stream->release();
}
bool JsonWriter::writeRaw(const char *chars, size_t len)
{
    if (_finished)
        return false;
    if ((size_t)-1 == len)
        len = strlen(chars);
    if (0 == len)
        return true;
    _finished = len != (size_t)writeBuffer(chars, len);
    return !_finished;
}
bool JsonWriter::writeStartArray()
{
    _writeDelim();
    _pushstate(StateArrayValue);
    writeRaw("[", 1);
    return !_finished;
}
bool JsonWriter::writeEndArray()
{
    if (!_pretty)
        _popstate();
    else
    {
        int state = _topstate();
        _popstate();
        if (StateArrayValue != state)
            _writeLine();
    }
    writeRaw("]", 1);
    return !_finished;
}
bool JsonWriter::writeStartObject()
{
    _writeDelim();
    _pushstate(StateObjectKey);
    writeRaw("{", 1);
    return !_finished;
}
bool JsonWriter::writeEndObject()
{
    if (!_pretty)
        _popstate();
    else
    {
        int state = _topstate();
        _popstate();
        if (StateObjectKey != state)
            _writeLine();
    }
    writeRaw("}", 1);
    return !_finished;
}
bool JsonWriter::writeNullValue()
{
    _writeDelim();
    writeRaw("null", 4);
    return !_finished;
}
bool JsonWriter::writeBooleanValue(bool B)
{
    _writeDelim();
    if (B)
        writeRaw("true", 4);
    else
        writeRaw("false", 5);
    return !_finished;
}
bool JsonWriter::writeIntegerValue(long l)
{
    _writeDelim();
    char buf[64];
    portable_snprintf(buf, sizeof buf, "%ld", l);
    writeRaw(buf);
    return !_finished;
}
bool JsonWriter::writeRealValue(double d)
{
    _writeDelim();
    char buf[64];
    if (isNone(d))
    {
        if (flags(InfinityExtension))
        {
            if (!flags(FloatNoneCompatibility))
                strcpy(buf, "NaN");
            else /* if FloatNoneCompatibility convert None to -INFINITY */
                strcpy(buf, "-Infinity");
        }
        else
            return _finished = false;
    }
    else if (-INFINITY == d)
    {
        if (flags(InfinityExtension))
            strcpy(buf, "-Infinity");
        else
            return _finished = false;
    }
    else if (+INFINITY == d)
    {
        if (flags(InfinityExtension))
            strcpy(buf, "Infinity");
        else
            return _finished = false;
    }
    else if (d == floor(d))
        portable_snprintf(buf, sizeof buf, "%.1f", d);
    else
        portable_snprintf(buf, sizeof buf, DBL_ROUNDTRIP_G_FMT, d);
    writeRaw(buf);
    return !_finished;
}
bool JsonWriter::writeText(const char *text, size_t len)
{
    int state = _topstate();
    switch (state)
    {
    case StateObjectKey:
        if (_pretty)
            _writeLine();
        _topstate(StateObjectValue);
        break;
    case StateObjectValue:
        _topstate(StateObjectComma);
        break;
    case StateObjectComma:
        writeRaw(",", 1);
        if (_pretty)
            _writeLine();
        _topstate(StateObjectValue);
        break;
    case StateArrayValue:
        if (_pretty)
            _writeLine();
        _topstate(StateArrayComma);
        break;
    case StateArrayComma:
        writeRaw(",", 1);
        if (_pretty)
            _writeLine();
        break;
    }
    if ((size_t)-1 == len)
        len = strlen(text);
    writeRaw("\"", 1);
    const char *q = text, *p = q, *endp = q + len;
    while (endp > p)
    {
        unsigned char c = *p;
        switch (c)
        {
        case '\"':
            break;
        case '\b':
            c = 'b';
            break;
        case '\f':
            c = 'f';
            break;
        case '\n':
            c = 'n';
            break;
        case '\r':
            c = 'r';
            break;
        case '\t':
            c = 't';
            break;
        case '\\':
            if (flags(StringEscapeExtension) && _specialText && '/' == p[1] && '/' == p[2])
                ; /* \// sequence */
            else
                break;
        default:
            unsigned w = 0;
            size_t n;
            if (0x80 > c)
            {
                if (0x20 <= c)
                {
                    ++p;
                    continue;
                }
                else
                {
                    w = c;
                    n = 1;
                }
            }
            else if (0xc0 == (c & 0xe0))
            {
                if (endp >= p + 2)
                    w = ((c & 0x1f) << 6) | (p[1] & 0x3f);
                n = 2;
            }
            else if (0xe0 == (c & 0xf0))
            {
                if (endp >= p + 3)
                    w = ((c & 0x0f) << 12) | ((p[1] & 0x3f) << 6) | (p[2] & 0x3f);
                n = 3;
            }
            else
            {
                writeRaw(q, p - q);
                /* invalid character; ignore */
                q = ++p;
                continue;
            }
            writeRaw(q, p - q);
            if (endp >= p + n) //if (endp > p + n - 1)
            {
                char buf[8];
                portable_snprintf(buf, sizeof buf, "\\u%04x", w);
                writeRaw(buf);
                q = p += n;
            }
            else
                q = p = endp;
            continue;
        }
        writeRaw(q, p - q);
        writeRaw("\\", 1);
        writeRaw((char *)&c, 1);
        q = ++p;
    }
    writeRaw(q, p - q);
    writeRaw("\"", 1);
    switch (state)
    {
    case StateObjectKey:
    case StateObjectComma:
        if (!_pretty)
            writeRaw(":", 1);
        else
            writeRaw(": ", 2);
        break;
    }
    return !_finished;
}
bool JsonWriter::_writeSpecialText(const char *text, size_t len)
{
    _specialText = true;
    writeText(text, len);
    _specialText = false;
    return !_finished;
}
bool JsonWriter::write(JsonEvent *event)
{
    switch (event->kind())
    {
    case '[':
        return writeStartArray();
    case ']':
        return writeEndArray();
    case '{':
        return writeStartObject();
    case '}':
        return writeEndObject();
    case '0':
        return writeNullValue();
    case 'B':
        return writeBooleanValue(event->booleanValue());
    case 'l':
        return writeIntegerValue(event->integerValue());
    case 'd':
        return writeRealValue(event->realValue());
    case 'K':
    case 'T':
        return writeText(event->text(), cstr_length(event->text()));
    default:
        return true;
    }
}
bool JsonWriter::finished()
{
    return _finished;
}
unsigned JsonWriter::flags(unsigned mask)
{
    return _flags & mask;
}
void JsonWriter::setFlags(unsigned mask, unsigned value)
{
    if (value)
        _flags |= mask;
    else
        _flags &= ~mask;
}
ssize_t JsonWriter::writeBuffer(const void *buf, size_t size)
{
    if (0 == _stream)
        return 0;
    return _stream->write(buf, size);
}
void JsonWriter::_writeDelim()
{
    switch (_topstate())
    {
    case StateObjectValue:
        _topstate(StateObjectComma);
        break;
    case StateArrayValue:
        if (_pretty)
            _writeLine();
        _topstate(StateArrayComma);
        break;
    case StateArrayComma:
        writeRaw(",", 1);
        if (_pretty)
            _writeLine();
        break;
    }
}
void JsonWriter::_writeLine()
{
    int indent = _stackp - _stack;
    switch (indent % 4)
    {
    case 0:
        writeRaw("\n", 1);
        break;
    case 1:
        writeRaw("\n  ", 3);
        break;
    case 2:
        writeRaw("\n    ", 5);
        break;
    case 3:
        writeRaw("\n      ", 7);
        break;
    }
    for (int i = 0, n = indent / 4; n > i; i++)
        writeRaw("        ", 8);
}

}
}
