/*
 * Base/impl/Uuid.cpp
 *
 * Copyright 2010-2013 Navimatics Corporation. All rights reserved.
 */
/*
 * Portions of this file are under the following notice:
 *
 * Copyright (C) 1996, 1997, 1998, 1999 Theodore Ts'o.
 *
 * %Begin-Header%
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 * 1. Redistributions of source code must retain the above copyright
 *    notice, and the entire permission notice in its entirety,
 *    including the disclaimer of warranties.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 * 3. The name of the author may not be used to endorse or promote
 *    products derived from this software without specific prior
 *    written permission.
 *
 * THIS SOFTWARE IS PROVIDED ``AS IS'' AND ANY EXPRESS OR IMPLIED
 * WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES
 * OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE, ALL OF
 * WHICH ARE HEREBY DISCLAIMED.  IN NO EVENT SHALL THE AUTHOR BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT
 * OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR
 * BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF
 * LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE
 * USE OF THIS SOFTWARE, EVEN IF NOT ADVISED OF THE POSSIBILITY OF SUCH
 * DAMAGE.
 * %End-Header%
 */

#include <Base/impl/Uuid.hpp>
#include <Base/impl/Snprintf.hpp>
#if defined(BASE_CONFIG_WINDOWS)
#include <windows.h>
#elif defined(BASE_CONFIG_ANDROID)
#include <Base/impl/Synchronization.hpp>
#include <fcntl.h>
#elif defined(BASE_CONFIG_POSIX)
#include <uuid/uuid.h>
#endif

namespace Base {
namespace impl {

struct uuid
{
    uint32_t time_low;
    uint16_t time_mid;
    uint16_t time_hi_and_version;
    uint16_t clock_seq;
    uint8_t node[6];
};
static void uuid_pack(const struct uuid *uu, Uuid ptr)
{
    uint32_t tmp;
    unsigned char *out = ptr;
    tmp = uu->time_low;
    out[3] = (unsigned char) tmp;
    tmp >>= 8;
    out[2] = (unsigned char) tmp;
    tmp >>= 8;
    out[1] = (unsigned char) tmp;
    tmp >>= 8;
    out[0] = (unsigned char) tmp;
    tmp = uu->time_mid;
    out[5] = (unsigned char) tmp;
    tmp >>= 8;
    out[4] = (unsigned char) tmp;
    tmp = uu->time_hi_and_version;
    out[7] = (unsigned char) tmp;
    tmp >>= 8;
    out[6] = (unsigned char) tmp;
    tmp = uu->clock_seq;
    out[9] = (unsigned char) tmp;
    tmp >>= 8;
    out[8] = (unsigned char) tmp;
    memcpy(out+10, uu->node, 6);
}
static void uuid_unpack(const Uuid in, struct uuid *uu)
{
    const uint8_t *ptr = in;
    uint32_t tmp;
    tmp = *ptr++;
    tmp = (tmp << 8) | *ptr++;
    tmp = (tmp << 8) | *ptr++;
    tmp = (tmp << 8) | *ptr++;
    uu->time_low = tmp;
    tmp = *ptr++;
    tmp = (tmp << 8) | *ptr++;
    uu->time_mid = tmp;
    tmp = *ptr++;
    tmp = (tmp << 8) | *ptr++;
    uu->time_hi_and_version = tmp;
    tmp = *ptr++;
    tmp = (tmp << 8) | *ptr++;
    uu->clock_seq = tmp;
    memcpy(uu->node, ptr, 6);
}

#if defined(BASE_CONFIG_WINDOWS)
void UuidCreate(Uuid uu)
{
    UUID uuid;
    ::UuidCreate(&uuid);
    uuid_pack((struct uuid *)&uuid, uu);
}
#elif defined(BASE_CONFIG_ANDROID)
typedef Uuid uuid_t;
static int get_random_fd(void)
{
    struct timeval tv;
    static int fd;
    int i;
    execute_once {
        gettimeofday(&tv, 0);
        fd = open("/dev/urandom", O_RDONLY);
        if (fd == -1)
            fd = open("/dev/random", O_RDONLY | O_NONBLOCK);
        if (fd >= 0) {
            i = fcntl(fd, F_GETFD);
            if (i >= 0)
                fcntl(fd, F_SETFD, i | FD_CLOEXEC);
        }
        srand((getpid() << 16) ^ getuid() ^ tv.tv_sec ^ tv.tv_usec);
    }
    /* Crank the random number generator a few times */
    gettimeofday(&tv, 0);
    for (i = (tv.tv_sec ^ tv.tv_usec) & 0x1F; i > 0; i--)
        rand();
    return fd;
}
static void get_random_bytes(void *buf, int nbytes)
{
    int i, n = nbytes, fd = get_random_fd();
    int lose_counter = 0;
    unsigned char *cp = (unsigned char *)buf;
    unsigned short tmp_seed[3];
    if (fd >= 0) {
        while (n > 0) {
            i = read(fd, cp, n);
            if (i <= 0) {
                if (lose_counter++ > 16)
                    break;
                continue;
            }
            n -= i;
            cp += i;
            lose_counter = 0;
        }
    }
    /*
     * We do this all the time, but this is the only source of
     * randomness if /dev/random/urandom is out to lunch.
     */
    for (cp = (unsigned char *)buf, i = 0; i < nbytes; i++)
        *cp++ ^= (rand() >> 7) & 0xFF;
}
static void uuid_generate_random(uuid_t out)
{
    uuid_t buf;
    struct uuid uu;
    get_random_bytes(buf, sizeof(buf));
    uuid_unpack(buf, &uu);
    uu.clock_seq = (uu.clock_seq & 0x3FFF) | 0x8000;
    uu.time_hi_and_version = (uu.time_hi_and_version & 0x0FFF)
        | 0x4000;
    uuid_pack(&uu, out);
}
void UuidCreate(Uuid uu)
{
    uuid_generate_random(uu);
}
#elif defined(BASE_CONFIG_POSIX)
void UuidCreate(Uuid uu)
{
    uuid_generate(uu);
}
#endif
void UuidCopy(Uuid dst, const Uuid src)
{
    memcpy(dst, src, sizeof(Uuid));
}
void UuidClear(Uuid uu)
{
    memset(uu, 0, sizeof(Uuid));
}
bool UuidIsNull(const Uuid uu)
{
    for (const unsigned char *p = uu, *endp = p + sizeof(Uuid); endp > p; p++)
        if (*p)
            return false;
    return true;
}
bool UuidParse(const char *in, Uuid uu)
{
    size_t len = strlen(in);
    if (32 > len)
        return false;
    bool dashes = '-' == in[8];
    if (dashes)
    {
        if (36 != len)
            return false;
    }
    else
    {
        if (32 != len)
            return false;
    }
    struct uuid uuid;
    if (dashes)
    {
        uint32_t v = 0;
        for (const char *p = in, *endp = in + len + 1; endp > p; p++)
        {
            char x = *p;
            int i = p - in;
            switch (i)
            {
            case 8:
                if ('-' != x)
                    return false;
                uuid.time_low = v;
                v = 0;
                continue;
            case 13:
                if ('-' != x)
                    return false;
                uuid.time_mid = v;
                v = 0;
                continue;
            case 18:
                if ('-' != x)
                    return false;
                uuid.time_hi_and_version = v;
                v = 0;
                continue;
            case 23:
                if ('-' != x)
                    return false;
                uuid.clock_seq = v;
                v = 0;
                continue;
            case 26:
                uuid.node[0] = v;
                v = 0;
                break;
            case 28:
                uuid.node[1] = v;
                v = 0;
                break;
            case 30:
                uuid.node[2] = v;
                v = 0;
                break;
            case 32:
                uuid.node[3] = v;
                v = 0;
                break;
            case 34:
                uuid.node[4] = v;
                v = 0;
                break;
            case 36:
                uuid.node[5] = v;
                v = 0;
                continue;
            default:
                break;
            }
            if ('0' <= x && x <= '9')
                x = x - '0';
            else if ('A' <= x && x <= 'F')
                x = x - 'A' + 10;
            else if ('a' <= x && x <= 'f')
                x = x - 'a' + 10;
            else
                return false;
            v <<= 4;
            v |= x;
        }
    }
    else
    {
        uint32_t v = 0;
        for (const char *p = in, *endp = in + len + 1; endp > p; p++)
        {
            char x = *p;
            int i = p - in;
            switch (i)
            {
            case 8:
                uuid.time_low = v;
                v = 0;
                break;
            case 12:
                uuid.time_mid = v;
                v = 0;
                break;
            case 16:
                uuid.time_hi_and_version = v;
                v = 0;
                break;
            case 20:
                uuid.clock_seq = v;
                v = 0;
                break;
            case 22:
                uuid.node[0] = v;
                v = 0;
                break;
            case 24:
                uuid.node[1] = v;
                v = 0;
                break;
            case 26:
                uuid.node[2] = v;
                v = 0;
                break;
            case 28:
                uuid.node[3] = v;
                v = 0;
                break;
            case 30:
                uuid.node[4] = v;
                v = 0;
                break;
            case 32:
                uuid.node[5] = v;
                v = 0;
                continue;
            default:
                break;
            }
            if ('0' <= x && x <= '9')
                x = x - '0';
            else if ('A' <= x && x <= 'F')
                x = x - 'A' + 10;
            else if ('a' <= x && x <= 'f')
                x = x - 'a' + 10;
            else
                return false;
            v <<= 4;
            v |= x;
        }
    }
    uuid_pack(&uuid, uu);
    return true;
}
bool UuidUnparse(const Uuid uu, char *out, size_t size)
{
    struct uuid uuid;
    uuid_unpack(uu, &uuid);
    int res = portable_snprintf(out, size, "%08x-%04x-%04x-%02x%02x-%02x%02x%02x%02x%02x%02x",
        uuid.time_low, uuid.time_mid, uuid.time_hi_and_version,
        uuid.clock_seq >> 8, uuid.clock_seq & 0xFF,
        uuid.node[0], uuid.node[1], uuid.node[2],
        uuid.node[3], uuid.node[4], uuid.node[5]);
    return res < (ssize_t)size;
}
bool UuidUnparseRaw(const Uuid uu, char *out, size_t size)
{
    struct uuid uuid;
    uuid_unpack(uu, &uuid);
    int res = portable_snprintf(out, size, "%08x%04x%04x%02x%02x%02x%02x%02x%02x%02x%02x",
        uuid.time_low, uuid.time_mid, uuid.time_hi_and_version,
        uuid.clock_seq >> 8, uuid.clock_seq & 0xFF,
        uuid.node[0], uuid.node[1], uuid.node[2],
        uuid.node[3], uuid.node[4], uuid.node[5]);
    return res < (ssize_t)size;
}
int UuidCompare(const Uuid uu1, const Uuid uu2)
{
#define UUCMP(u1,u2) if (u1 != u2) return((u1 < u2) ? -1 : 1)
    struct uuid uuid1, uuid2;
    uuid_unpack(uu1, &uuid1);
    uuid_unpack(uu2, &uuid2);
    UUCMP(uuid1.time_low, uuid2.time_low);
    UUCMP(uuid1.time_mid, uuid2.time_mid);
    UUCMP(uuid1.time_hi_and_version, uuid2.time_hi_and_version);
    UUCMP(uuid1.clock_seq, uuid2.clock_seq);
    return memcmp(uuid1.node, uuid2.node, 6);
#undef UUCMP
}

}
}
