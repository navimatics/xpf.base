/*
 * Base/jimpl/DateTime.cpp
 *
 * Copyright 2010-2013 Navimatics Corporation. All rights reserved.
 */

#include <Base/Jvm.hpp>
#if defined(BASE_CONFIG_JVM)
#include <Base/DateTime.hpp>
#include <Base/Synchronization.hpp>

namespace Base {
namespace jimpl {

static void *ComponentsMapping()
{
    static JvmNativeField fields[] =
    {
        BASE_JVM_NATIVEFIELD(year, "I", DateTime::Components),
        BASE_JVM_NATIVEFIELD(month, "I", DateTime::Components),
        BASE_JVM_NATIVEFIELD(day, "I", DateTime::Components),
        BASE_JVM_NATIVEFIELD(hour, "I", DateTime::Components),
        BASE_JVM_NATIVEFIELD(min, "I", DateTime::Components),
        BASE_JVM_NATIVEFIELD(sec, "I", DateTime::Components),
        BASE_JVM_NATIVEFIELD(msec, "I", DateTime::Components),
        BASE_JVM_NATIVEFIELD(dayOfWeek, "I", DateTime::Components),
    };
    static void *mapping;
    execute_once
        mapping = JvmCreateStructMapping(BASE_JVM_BASEPACKAGE "/DateTime$Components",
            fields, NELEMS(fields));
    return mapping;
}
static jobject JNICALL components(JNIEnv *env, jobject jself)
{
    mark_and_collect
    {
        DateTime *self = (DateTime *)JvmNativeObject(env, jself);
        DateTime::Components c = self->components();
        return JvmJavaObjectFromStruct(env, ComponentsMapping(), &c);
    }
}
static jdouble JNICALL julianDate(JNIEnv *env, jobject jself)
{
    mark_and_collect
    {
        DateTime *self = (DateTime *)JvmNativeObject(env, jself);
        return self->julianDate();
    }
}
static jlong JNICALL unixTime(JNIEnv *env, jobject jself)
{
    mark_and_collect
    {
        DateTime *self = (DateTime *)JvmNativeObject(env, jself);
        return self->unixTime();
    }
}
static void JNICALL _ctor1(JNIEnv *env, jobject jself)
{
    mark_and_collect
    {
        DateTime *self = new DateTime;
        JvmSetNativeObject(env, jself, self);
    }
}
static void JNICALL _ctor2(JNIEnv *env, jobject jself, jobject jcomponents)
{
    mark_and_collect
    {
        DateTime::Components c;
        JvmStructFromJavaObject(env, ComponentsMapping(), jcomponents, &c);
        DateTime *self = new DateTime(c);
        JvmSetNativeObject(env, jself, self);
    }
}
static void JNICALL _ctor3(JNIEnv *env, jobject jself, jint year, jint month, jint day,
    jint hour, jint min, jint sec, jint msec)
{
    mark_and_collect
    {
        DateTime *self = new DateTime(year, month, day, hour, min, sec, msec);
        JvmSetNativeObject(env, jself, self);
    }
}
static void JNICALL _ctor4(JNIEnv *env, jobject jself, jdouble julianDate)
{
    mark_and_collect
    {
        DateTime *self = new DateTime(julianDate);
        JvmSetNativeObject(env, jself, self);
    }
}
static void JNICALL _ctor5(JNIEnv *env, jobject jself, long unixTime)
{
    mark_and_collect
    {
        DateTime *self = new DateTime(unixTime);
        JvmSetNativeObject(env, jself, self);
    }
}

static JNINativeMethod methods[] =
{
    BASE_JVM_NATIVEMETHOD(components, "()L" BASE_JVM_BASEPACKAGE "/DateTime$Components;"),
    BASE_JVM_NATIVEMETHOD(julianDate, "()D"),
    BASE_JVM_NATIVEMETHOD(unixTime, "()J"),
    BASE_JVM_NATIVEMETHOD_EX("_ctor", "()V", _ctor1),
    BASE_JVM_NATIVEMETHOD_EX("_ctor", "(L" BASE_JVM_BASEPACKAGE "/DateTime$Components;)V", _ctor2),
    BASE_JVM_NATIVEMETHOD_EX("_ctor", "(IIIIIII)V", _ctor3),
    BASE_JVM_NATIVEMETHOD_EX("_ctor", "(D)V", _ctor4),
    BASE_JVM_NATIVEMETHOD_EX("_ctor", "(J)V", _ctor5),
};
void DateTimeRegister()
{
    JvmRegisterNativeMethods(BASE_JVM_BASEPACKAGE "/DateTime", methods, NELEMS(methods));
    JvmRegisterPeerTypes(BASE_JVM_BASEPACKAGE "/DateTime", typeid(DateTime));
}

}
}
#endif
