/*
 * Base/jimpl/NativeObjectUtil.cpp
 *
 * Copyright 2010-2013 Navimatics Corporation. All rights reserved.
 */

#include <Base/Jvm.hpp>
#if defined(BASE_CONFIG_JVM)
#include <Base/ObjectUtil.hpp>

namespace Base {
namespace jimpl {

static jobject JNICALL readNative(JNIEnv *env, jclass jcls, jstring jpath)
{
    mark_and_collect
    {
        const char *path = JvmCString(env, jpath);
        jobject result = 0;
        if (0 == path)
            JvmThrowException(env, "java/lang/NullPointerException", "path cannot be null");
        else
            result = JvmJavaObject(env, ObjectReadNative(path));
        return result;
    }
}
static void JNICALL writeNative(JNIEnv *env, jclass jcls, jstring jpath, jobject jobj)
{
    mark_and_collect
    {
        Object *obj = JvmNativeObject(env, jobj);
        const char *path = JvmCString(env, jpath);
        if (0 == path)
            JvmThrowException(env, "java/lang/NullPointerException", "path cannot be null");
        else if (0 == obj)
            JvmThrowException(env, "java/lang/NullPointerException", "object cannot be null");
        else
            ObjectWriteNative(path, obj);
    }
}
static jobject JNICALL readJson(JNIEnv *env, jclass jcls, jstring jpath)
{
    mark_and_collect
    {
        const char *path = JvmCString(env, jpath);
        jobject result = 0;
        if (0 == path)
            JvmThrowException(env, "java/lang/NullPointerException", "path cannot be null");
        else
            result = JvmJavaObject(env, ObjectReadJson(path));
        return result;
    }
}
static void JNICALL writeJson(JNIEnv *env, jclass jcls, jstring jpath, jobject jobj)
{
    mark_and_collect
    {
        Object *obj = JvmNativeObject(env, jobj);
        const char *path = JvmCString(env, jpath);
        if (0 == path)
            JvmThrowException(env, "java/lang/NullPointerException", "path cannot be null");
        else if (0 == obj)
            JvmThrowException(env, "java/lang/NullPointerException", "object cannot be null");
        else
            ObjectWriteJson(path, obj);
    }
}

static JNINativeMethod methods[] =
{
    BASE_JVM_NATIVEMETHOD(readNative, "(Ljava/lang/String;)L" BASE_JVM_JAVAOBJECTBASECLASS ";"),
    BASE_JVM_NATIVEMETHOD(writeNative, "(Ljava/lang/String;L" BASE_JVM_JAVAOBJECTBASECLASS ";)V"),
    BASE_JVM_NATIVEMETHOD(readJson, "(Ljava/lang/String;)L" BASE_JVM_JAVAOBJECTBASECLASS ";"),
    BASE_JVM_NATIVEMETHOD(writeJson, "(Ljava/lang/String;L" BASE_JVM_JAVAOBJECTBASECLASS ";)V"),
};
void NativeObjectUtilRegister()
{
    JvmRegisterNativeMethods(BASE_JVM_BASEPACKAGE "/NativeObjectUtil", methods, NELEMS(methods));
}

}
}
#endif
