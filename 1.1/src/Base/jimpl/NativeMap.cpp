/*
 * Base/jimpl/NativeMap.cpp
 *
 * Copyright 2010-2013 Navimatics Corporation. All rights reserved.
 */

#include <Base/Jvm.hpp>
#if defined(BASE_CONFIG_JVM)
#include <Base/CArray.hpp>
#include <Base/Map.hpp>

namespace Base {
namespace jimpl {

static void JNICALL clear(JNIEnv *env, jobject jself)
{
    mark_and_collect
    {
        Map *self = (Map *)JvmNativeObject(env, jself);
        self->removeAllObjects();
    }
}
static jboolean JNICALL containsKey(JNIEnv *env, jobject jself, jobject jkey)
{
    mark_and_collect
    {
        if (!env->IsInstanceOf(jkey, JvmJavaObjectBaseClass()))
            return false;
        Map *self = (Map *)JvmNativeObject(env, jself);
        Object *key = JvmNativeObject(env, jkey);
        jboolean result = false;
        if (0 == key)
            JvmThrowException(env, "java/lang/NullPointerException", "key cannot be null");
        else
        {
            Object *value;
            result = self->getObject(key, value);
        }
        return result;
    }
}
static jobject JNICALL get(JNIEnv *env, jobject jself, jobject jkey)
{
    mark_and_collect
    {
        if (!env->IsInstanceOf(jkey, JvmJavaObjectBaseClass()))
            return 0;
        Map *self = (Map *)JvmNativeObject(env, jself);
        Object *key = JvmNativeObject(env, jkey);
        jobject result = 0;
        if (0 == key)
            JvmThrowException(env, "java/lang/NullPointerException", "key cannot be null");
        else
            result = JvmJavaObject(env, self->object(key));
        return result;
    }
}
static jobject JNICALL put(JNIEnv *env, jobject jself, jobject jkey, jobject jvalue)
{
    mark_and_collect
    {
        Map *self = (Map *)JvmNativeObject(env, jself);
        Object *key = JvmNativeObject(env, jkey);
        Object *value = JvmNativeObject(env, jvalue);
        jobject result = 0;
        if (0 == key)
            JvmThrowException(env, "java/lang/NullPointerException", "key cannot be null");
        else
        {
            result = JvmJavaObject(env, self->object(key));
            self->setObject(key, value);
        }
        return result;
    }
}
static jobject JNICALL remove(JNIEnv *env, jobject jself, jobject jkey)
{
    mark_and_collect
    {
        if (!env->IsInstanceOf(jkey, JvmJavaObjectBaseClass()))
            return 0;
        Map *self = (Map *)JvmNativeObject(env, jself);
        Object *key = JvmNativeObject(env, jkey);
        jobject result = 0;
        if (0 == key)
            JvmThrowException(env, "java/lang/NullPointerException", "key cannot be null");
        else
        {
            result = JvmJavaObject(env, self->object(key));
            self->removeObject(key);
        }
        return result;
    }
}
static jint JNICALL size(JNIEnv *env, jobject jself)
{
    mark_and_collect
    {
        Map *self = (Map *)JvmNativeObject(env, jself);
        return self->count();
    }
}
static jobjectArray JNICALL _keys(JNIEnv *env, jobject jself)
{
    mark_and_collect
    {
        Map *self = (Map *)JvmNativeObject(env, jself);
        Object **keys = self->keys();
        size_t count = carr_length(keys);
        jobjectArray result = env->NewObjectArray(count, JvmJavaObjectBaseClass(), 0);
        if (0 == result)
            return 0;
        for (size_t index = 0; count > index; index++)
            env->SetObjectArrayElement(result, index, JvmJavaObject(env, keys[index]));
        return result;
    }
}
static jobjectArray JNICALL _values(JNIEnv *env, jobject jself)
{
    mark_and_collect
    {
        Map *self = (Map *)JvmNativeObject(env, jself);
        Object **objs = self->objects();
        size_t count = carr_length(objs);
        jobjectArray result = env->NewObjectArray(count, JvmJavaObjectBaseClass(), 0);
        if (0 == result)
            return 0;
        for (size_t index = 0; count > index; index++)
            env->SetObjectArrayElement(result, index, JvmJavaObject(env, objs[index]));
        return result;
    }
}
static void JNICALL _ctor(JNIEnv *env, jobject jself)
{
    mark_and_collect
    {
        Map *self = new Map;
        JvmSetNativeObject(env, jself, self);
    }
}

static JNINativeMethod methods[] =
{
    BASE_JVM_NATIVEMETHOD(clear, "()V"),
    BASE_JVM_NATIVEMETHOD(containsKey, "(Ljava/lang/Object;)Z"),
    BASE_JVM_NATIVEMETHOD(get, "(Ljava/lang/Object;)L" BASE_JVM_JAVAOBJECTBASECLASS ";"),
    BASE_JVM_NATIVEMETHOD(put, "(L" BASE_JVM_JAVAOBJECTBASECLASS ";L" BASE_JVM_JAVAOBJECTBASECLASS ";)L" BASE_JVM_JAVAOBJECTBASECLASS ";"),
    BASE_JVM_NATIVEMETHOD(remove, "(Ljava/lang/Object;)L" BASE_JVM_JAVAOBJECTBASECLASS ";"),
    BASE_JVM_NATIVEMETHOD(size, "()I"),
    BASE_JVM_NATIVEMETHOD(_keys, "()[L" BASE_JVM_JAVAOBJECTBASECLASS ";"),
    BASE_JVM_NATIVEMETHOD(_values, "()[L" BASE_JVM_JAVAOBJECTBASECLASS ";"),
    BASE_JVM_NATIVEMETHOD(_ctor, "()V"),
};
void NativeMapRegister()
{
    JvmRegisterNativeMethods(BASE_JVM_BASEPACKAGE "/NativeMap", methods, NELEMS(methods));
    JvmRegisterPeerTypes(BASE_JVM_BASEPACKAGE "/NativeMap", typeid(Map));
}

}
}
#endif
