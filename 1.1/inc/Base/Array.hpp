/*
 * Base/Array.hpp
 *
 * Copyright 2010-2013 Navimatics Corporation. All rights reserved.
 */

#ifndef BASE_ARRAY_HPP_INCLUDED
#define BASE_ARRAY_HPP_INCLUDED

#include <Base/Defines.hpp>
#include <Base/Object.hpp>
#include <Base/impl/Array.hpp>

namespace Base {

using Base::impl::Array;
using Base::impl::GenericArray;

}

#endif // BASE_ARRAY_HPP_INCLUDED
