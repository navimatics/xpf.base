/*
 * Base/Map.hpp
 *
 * Copyright 2010-2013 Navimatics Corporation. All rights reserved.
 */

#ifndef BASE_MAP_HPP_INCLUDED
#define BASE_MAP_HPP_INCLUDED

#include <Base/Defines.hpp>
#include <Base/Object.hpp>
#include <Base/impl/Map.hpp>

namespace Base {

using Base::impl::Map;
using Base::impl::GenericMap;

}

#endif // BASE_MAP_HPP_INCLUDED
