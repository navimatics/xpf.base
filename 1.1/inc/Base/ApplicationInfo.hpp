/*
 * Base/ApplicationInfo.hpp
 *
 * Copyright 2010-2013 Navimatics Corporation. All rights reserved.
 */

#ifndef BASE_APPLICATIONINFO_HPP_INCLUDED
#define BASE_APPLICATIONINFO_HPP_INCLUDED

#include <Base/Defines.hpp>
#include <Base/impl/ApplicationInfo.hpp>

namespace Base {

using Base::impl::GetApplicationName;
using Base::impl::GetApplicationResourceDirectory;
using Base::impl::GetApplicationDataDirectory;
using Base::impl::GetDocumentDirectory;
using Base::impl::GetTemporaryDirectory;

using Base::impl::GetWorkingDirectory;

}

#endif // BASE_APPLICATIONINFO_HPP_INCLUDED
