/*
 * Base/Log.hpp
 *
 * Copyright 2010-2013 Navimatics Corporation. All rights reserved.
 */

#ifndef BASE_LOG_HPP_INCLUDED
#define BASE_LOG_HPP_INCLUDED

#include <Base/Defines.hpp>
#include <Base/impl/Log.hpp>

namespace Base {

using Base::impl::Log;
using Base::impl::Logv;

}

#endif // BASE_LOG_HPP_INCLUDED
