/*
 * Base/IndexMap.hpp
 *
 * Copyright 2010-2013 Navimatics Corporation. All rights reserved.
 */

#ifndef BASE_INDEXMAP_HPP_INCLUDED
#define BASE_INDEXMAP_HPP_INCLUDED

#include <Base/Defines.hpp>
#include <Base/Object.hpp>
#include <Base/impl/IndexMap.hpp>

namespace Base {

using Base::impl::IndexMap;
using Base::impl::GenericIndexMap;

}

#endif // BASE_INDEXMAP_HPP_INCLUDED
