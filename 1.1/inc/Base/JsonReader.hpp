/*
 * Base/JsonReader.hpp
 *
 * Copyright 2010-2013 Navimatics Corporation. All rights reserved.
 */

#ifndef BASE_JSONREADER_HPP_INCLUDED
#define BASE_JSONREADER_HPP_INCLUDED

#include <Base/Defines.hpp>
#include <Base/JsonParser.hpp>
#include <Base/impl/JsonReader.hpp>

namespace Base {

using Base::impl::JsonEvent;
using Base::impl::JsonReader;

}

#endif // BASE_JSONREADER_HPP_INCLUDED
