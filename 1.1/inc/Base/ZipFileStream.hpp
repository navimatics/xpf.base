/*
 * Base/ZipFileStream.hpp
 *
 * Copyright 2010-2013 Navimatics Corporation. All rights reserved.
 */

#ifndef BASE_ZIPFILESTREAM_HPP_INCLUDED
#define BASE_ZIPFILESTREAM_HPP_INCLUDED

#include <Base/Defines.hpp>
#include <Base/Stream.hpp>
#include <Base/impl/ZipFileStream.hpp>

namespace Base {

using Base::impl::ZipFileStream;

}

#endif // BASE_ZIPFILESTREAM_HPP_INCLUDED
