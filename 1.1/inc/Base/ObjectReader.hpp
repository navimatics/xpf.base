/*
 * Base/ObjectReader.hpp
 *
 * Copyright 2010-2013 Navimatics Corporation. All rights reserved.
 */

#ifndef BASE_OBJECTREADER_HPP_INCLUDED
#define BASE_OBJECTREADER_HPP_INCLUDED

#include <Base/Defines.hpp>
#include <Base/JsonParser.hpp>
#include <Base/impl/ObjectReader.hpp>

namespace Base {

using Base::impl::ObjectReader;

}

#endif // BASE_OBJECTREADER_HPP_INCLUDED
