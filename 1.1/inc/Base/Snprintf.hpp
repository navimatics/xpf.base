/*
 * Base/Snprintf.hpp
 *
 * Copyright 2010-2013 Navimatics Corporation. All rights reserved.
 */

#ifndef BASE_SNPRINTF_HPP_INCLUDED
#define BASE_SNPRINTF_HPP_INCLUDED

#include <Base/Defines.hpp>
#include <Base/impl/Snprintf.hpp>

namespace Base {
}

#endif // BASE_SNPRINTF_HPP_INCLUDED
