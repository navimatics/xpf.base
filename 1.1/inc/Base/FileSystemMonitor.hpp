/*
 * Base/FileSystemMonitor.hpp
 *
 * Copyright 2010-2015 Navimatics Corporation. All rights reserved.
 */

#ifndef BASE_FILESYSTEMMONITOR_HPP_INCLUDED
#define BASE_FILESYSTEMMONITOR_HPP_INCLUDED

#include <Base/Defines.hpp>
#include <Base/impl/FileSystemMonitor.hpp>

namespace Base {

using Base::impl::FileSystemMonitorEvent;
using Base::impl::FileSystemMonitor;

}

#endif // BASE_FILESYSTEMMONITOR_HPP_INCLUDED
