/*
 * Base/CArray.hpp
 *
 * Copyright 2010-2013 Navimatics Corporation. All rights reserved.
 */

#ifndef BASE_CARRAY_HPP_INCLUDED
#define BASE_CARRAY_HPP_INCLUDED

#include <Base/Defines.hpp>
#include <Base/Object.hpp>
#include <Base/impl/CArray.hpp>

namespace Base {

using Base::impl::CArray;
using Base::impl::carray;
using Base::impl::carrayWithCapacity;
using Base::impl::carr_new;
using Base::impl::carr_newWithCapacity;
using Base::impl::carr_assign;
using Base::impl::carr_assign_copy;
using Base::impl::carr_obj;
using Base::impl::carr_retain;
using Base::impl::carr_release;
using Base::impl::carr_autorelease;
using Base::impl::carr_autocollect;
using Base::impl::carr_length;
using Base::impl::carr_concat;

using Base::impl::carr_pointer_concat;
using Base::impl::carr_pointer_remove;

}

#endif // BASE_CARRAY_HPP_INCLUDED
