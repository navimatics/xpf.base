/*
 * Base/OrderedMap.hpp
 *
 * Copyright 2010-2013 Navimatics Corporation. All rights reserved.
 */

#ifndef BASE_ORDEREDMAP_HPP_INCLUDED
#define BASE_ORDEREDMAP_HPP_INCLUDED

#include <Base/Defines.hpp>
#include <Base/Object.hpp>
#include <Base/impl/OrderedMap.hpp>

namespace Base {

using Base::impl::OrderedMap;
using Base::impl::GenericOrderedMap;

}

#endif // BASE_ORDEREDMAP_HPP_INCLUDED
