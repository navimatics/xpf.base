/*
 * Base/ObjectUtil.hpp
 *
 * Copyright 2010-2013 Navimatics Corporation. All rights reserved.
 */

#ifndef BASE_OBJECTUTIL_HPP_INCLUDED
#define BASE_OBJECTUTIL_HPP_INCLUDED

#include <Base/Defines.hpp>
#include <Base/Object.hpp>
#include <Base/impl/ObjectUtil.hpp>

namespace Base {

using Base::impl::ObjectReadNative;
using Base::impl::ObjectWriteNative;

using Base::impl::ObjectReadJson;
using Base::impl::ObjectWriteJson;

}

#endif // BASE_OBJECTUTIL_HPP_INCLUDED
