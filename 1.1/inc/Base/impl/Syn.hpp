/*
 * Base/impl/Syn.hpp
 *
 * Copyright 2010-2013 Navimatics Corporation. All rights reserved.
 */

#ifndef BASE_IMPL_SYN_HPP_INCLUDED
#define BASE_IMPL_SYN_HPP_INCLUDED

#include <Base/impl/Defines.hpp>
#include <Base/impl/Atomic.hpp>
#if defined(BASE_CONFIG_WINDOWS)
#include <windows.h>
#elif defined(BASE_CONFIG_DARWIN)
#include <mach/mach_init.h>
#include <mach/semaphore.h>
#include <mach/task.h>
#include <pthread.h>
#include <sys/time.h>
#elif defined(BASE_CONFIG_POSIX)
#include <pthread.h>
#include <semaphore.h>
#include <sys/time.h>
#endif

namespace Base {
namespace impl {

/*
 * Mutexes
 */
#if defined(BASE_CONFIG_WINDOWS)
typedef CRITICAL_SECTION mutex_t;
inline void mutex_init(mutex_t *mutex)
{
    InitializeCriticalSection(mutex);
}
inline void mutex_fini(mutex_t *mutex)
{
    DeleteCriticalSection(mutex);
}
inline void mutex_lock(mutex_t *mutex)
{
    EnterCriticalSection(mutex);
}
inline void mutex_unlock(mutex_t *mutex)
{
    LeaveCriticalSection(mutex);
}
#elif defined(BASE_CONFIG_POSIX)
typedef pthread_mutex_t mutex_t;
BASE_APISYM pthread_mutexattr_t *mutex_defaultattr();
inline void mutex_init(mutex_t *mutex)
{
    if (0 != pthread_mutex_init(mutex, mutex_defaultattr()))
        MemoryAllocationError();
}
inline void mutex_fini(mutex_t *mutex)
{
    pthread_mutex_destroy(mutex);
}
inline void mutex_lock(mutex_t *mutex)
{
    pthread_mutex_lock(mutex);
}
inline void mutex_unlock(mutex_t *mutex)
{
    pthread_mutex_unlock(mutex);
}
#endif

/*
 * Read-write locks
 */
struct _rwlock_t
{
    bool fair;
    mutex_t order_mutex;
    mutex_t write_mutex;
    unsigned readers;
};
BASE_APISYM void _rwlock_init(_rwlock_t *rwlock, bool fair);
BASE_APISYM void _rwlock_fini(_rwlock_t *rwlock);
BASE_APISYM void _rwlock_rdlock(_rwlock_t *rwlock);
BASE_APISYM void _rwlock_rdunlock(_rwlock_t *rwlock);
BASE_APISYM void _rwlock_wrlock(_rwlock_t *rwlock);
BASE_APISYM void _rwlock_wrunlock(_rwlock_t *rwlock);
typedef _rwlock_t rwlock_t;
inline void rwlock_init(rwlock_t *rwlock, bool fair = false)
{
    _rwlock_init(rwlock, fair);
}
inline void rwlock_fini(rwlock_t *rwlock)
{
    _rwlock_fini(rwlock);
}
inline void rwlock_rdlock(rwlock_t *rwlock)
{
    _rwlock_rdlock(rwlock);
}
inline void rwlock_rdunlock(rwlock_t *rwlock)
{
    _rwlock_rdunlock(rwlock);
}
inline void rwlock_wrlock(rwlock_t *rwlock)
{
    _rwlock_wrlock(rwlock);
}
inline void rwlock_wrunlock(rwlock_t *rwlock)
{
    _rwlock_wrunlock(rwlock);
}

/*
 * Semaphores
 */
/*
 * This semaphore implementation uses a user-space count variable and a system (kernel) semaphore.
 *
 * The count variable has double-duty:
 *     - When the variable is zero (0) or positive it maintains the semaphore count.
 *     - When the variable is negative the semaphore count is considered zero (0).
 *       In this case the count variable is used to maintain the number of threads waiting on
 *       the system semaphore (-count == number-of-waiting-threads).
 *
 * This design allows the program to remain in user-space when there is no contention for the
 * semaphore.
 *
 * The wait() implementation atomically decrements the count variable and tests the new value.
 * If the new value is zero (0) or positive then this means that the semaphore count was greater
 * than zero (0). In this case wait() can return immediately. If the new value is negative then
 * this means that the semaphore count was zero (0). In this case the thread must wait for the
 * semaphore count to become positive. This is accomplished by issuing a wait() on the system
 * semaphore. The count variable now counts how many wait()'s have been issued on the system
 * semaphore.
 *
 * The notify() implementation atomically increments the count variable and tests the new value.
 * If the new value is positive then this means that there were no waiting threads on the system
 * semaphore to wake up. If the new value is zero (0) or negative then this means that there was
 * at least one waiting thread. We wake this thread up by issuing the appropriate system call
 * on the system semaphore.
 */
#if defined(BASE_CONFIG_WINDOWS)
struct _semaphore_t
{
    int count;
    HANDLE waitobj;
};
typedef struct _semaphore_t semaphore_t;
inline void semaphore_init(semaphore_t *sem, int count)
{
    sem->count = 0 < count ? count : 0;
    if (0 == (sem->waitobj = CreateSemaphore(0, 0, 1000000000, 0)))
        MemoryAllocationError();
}
inline void semaphore_fini(semaphore_t *sem)
{
    CloseHandle(sem->waitobj);
}
inline void semaphore_wait(semaphore_t *sem)
{
    if (0 > atomic_dec(sem->count))
        WaitForSingleObject(sem->waitobj, INFINITE);
}
inline void semaphore_notify(semaphore_t *sem)
{
    if (0 >= atomic_inc(sem->count))
        ReleaseSemaphore(sem->waitobj, 1, 0);
}
#elif defined(BASE_CONFIG_DARWIN)
struct _semaphore_t
{
    int count;
    semaphore_t sem;
};
typedef struct _semaphore_t semaphore_t;
inline void semaphore_init(semaphore_t *sem, int count)
{
    sem->count = 0 < count ? count : 0;
    if (KERN_SUCCESS != ::semaphore_create(mach_task_self(), &sem->sem, SYNC_POLICY_FIFO, 0))
        MemoryAllocationError();
}
inline void semaphore_fini(semaphore_t *sem)
{
    ::semaphore_destroy(mach_task_self(), sem->sem);
}
inline void semaphore_wait(semaphore_t *sem)
{
    if (0 > atomic_dec(sem->count))
        while (KERN_ABORTED == ::semaphore_wait(sem->sem))
            ;
}
inline void semaphore_notify(semaphore_t *sem)
{
    if (0 >= atomic_inc(sem->count))
        ::semaphore_signal(sem->sem);
}
#elif defined(BASE_CONFIG_POSIX)
struct _semaphore_t
{
    int count;
    sem_t sem;
};
typedef struct _semaphore_t semaphore_t;
inline void semaphore_init(semaphore_t *sem, int count)
{
    sem->count = 0 < count ? count : 0;
    if (0 == sem_init(&sem->sem, 0, 0))
        MemoryAllocationError();
}
inline void semaphore_fini(semaphore_t *sem)
{
    sem_destroy(&sem->sem);
}
inline void semaphore_wait(semaphore_t *sem)
{
    if (0 > atomic_dec(sem->count))
        while (-1 == sem_wait(&sem->sem) && EINTR == errno)
            ;
}
inline void semaphore_notify(semaphore_t *sem)
{
    if (0 >= atomic_inc(sem->count))
        sem_post(&sem->sem);
}
#endif

/*
 * Condition variables
 */
#if defined(BASE_CONFIG_WINDOWS)
struct _condvar_listentry_t
{
    _condvar_listentry_t *next, *prev;
};
struct _condvar_t
{
    mutex_t mutex;
    _condvar_listentry_t waitq;
};
BASE_APISYM void _condvar_init(_condvar_t *condvar);
BASE_APISYM void _condvar_fini(_condvar_t *condvar);
BASE_APISYM bool _condvar_wait(_condvar_t *condvar, mutex_t *mutex, wait_timeout_t timeout);
BASE_APISYM void _condvar_notify(_condvar_t *condvar);
BASE_APISYM void _condvar_notify_all(_condvar_t *condvar);
typedef _condvar_t condvar_t;
inline void condvar_init(condvar_t *condvar)
{
    _condvar_init(condvar);
}
inline void condvar_fini(condvar_t *condvar)
{
    _condvar_fini(condvar);
}
inline bool condvar_wait(condvar_t *condvar, mutex_t *mutex, wait_timeout_t timeout = wait_timeout_infinite)
{
    BASE_COMPILE_ASSERT(INFINITE == wait_timeout_infinite);
    return _condvar_wait(condvar, mutex, timeout);
}
inline void condvar_notify(condvar_t *condvar)
{
    _condvar_notify(condvar);
}
inline void condvar_notify_all(condvar_t *condvar)
{
    _condvar_notify_all(condvar);
}
#elif defined(BASE_CONFIG_POSIX)
typedef pthread_cond_t _condvar_t;
BASE_APISYM bool _condvar_timedwait(_condvar_t *condvar, mutex_t *mutex, wait_timeout_t timeout);
typedef _condvar_t condvar_t;
inline void condvar_init(condvar_t *condvar)
{
    if (0 != pthread_cond_init(condvar, 0))
        MemoryAllocationError();
}
inline void condvar_fini(condvar_t *condvar)
{
    pthread_cond_destroy(condvar);
}
inline bool condvar_wait(condvar_t *condvar, mutex_t *mutex, wait_timeout_t timeout = wait_timeout_infinite)
{
    if (wait_timeout_infinite == timeout)
        return 0 == pthread_cond_wait(condvar, mutex);
    else
        return _condvar_timedwait(condvar, mutex, timeout);
}
inline void condvar_notify(condvar_t *condvar)
{
    pthread_cond_signal(condvar);
}
inline void condvar_notify_all(condvar_t *condvar)
{
    pthread_cond_broadcast(condvar);
}
#endif

/*
 * Execute once
 */
typedef int execute_once_control_t;
BASE_APISYM bool execute_once_enter(execute_once_control_t *i);
BASE_APISYM void execute_once_leave(execute_once_control_t *i);

}
}

#endif // BASE_IMPL_SYN_HPP_INCLUDED
