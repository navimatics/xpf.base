/*
 * Base/impl/ApplicationInfo.hpp
 *
 * Copyright 2010-2013 Navimatics Corporation. All rights reserved.
 */

#ifndef BASE_IMPL_APPLICATIONINFO_HPP_INCLUDED
#define BASE_IMPL_APPLICATIONINFO_HPP_INCLUDED

#include <Base/impl/Defines.hpp>

namespace Base {
namespace impl {

BASE_APISYM const char *GetApplicationName();
BASE_APISYM const char *GetApplicationResourceDirectory();
BASE_APISYM const char *GetApplicationDataDirectory();
BASE_APISYM const char *GetDocumentDirectory();
BASE_APISYM const char *GetTemporaryDirectory();

BASE_APISYM const char *GetWorkingDirectory();

}
}

#endif // BASE_IMPL_APPLICATIONINFO_HPP_INCLUDED
