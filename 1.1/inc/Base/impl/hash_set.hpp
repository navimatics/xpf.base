/*
 * Base/impl/hash_set.hpp
 *
 * Copyright 2010-2013 Navimatics Corporation. All rights reserved.
 */

#ifndef BASE_IMPL_HASHSET_HPP_INCLUDED
#define BASE_IMPL_HASHSET_HPP_INCLUDED

#include <Base/impl/Defines.hpp>

#if !defined(BASE_IMPL_HASHSET_USE_STD_UNORDERED_SET)
#if defined(BASE_CONFIG_MSVC)
#define BASE_IMPL_HASHSET_USE_STD_UNORDERED_SET
#elif defined(BASE_CONFIG_GNUC)
#if (!defined(__GLIBCXX__) || __GLIBCXX__ < 20070514) && !defined(_LIBCPP_VERSION)
#undef BASE_IMPL_HASHSET_USE_STD_UNORDERED_SET
#else
#define BASE_IMPL_HASHSET_USE_STD_UNORDERED_SET
#endif
#endif
#endif

#if defined(BASE_IMPL_HASHSET_USE_STD_UNORDERED_SET)
#if defined(BASE_CONFIG_MSVC)
#define BASE_IMPL_HASHSET_STD           std::tr1
#include <unordered_set>
#elif defined(BASE_CONFIG_GNUC)
#if defined(_LIBCPP_VERSION)
#define BASE_IMPL_HASHSET_STD           std
#include <unordered_set>
#else
#define BASE_IMPL_HASHSET_STD           std::tr1
#include <tr1/unordered_set>
#endif
#endif
#else
#if defined(BASE_CONFIG_MSVC)
#include <hash_set>
#elif defined(BASE_CONFIG_GNUC)
#include <ext/hash_set>
#endif
#endif

namespace Base {
namespace impl {

template <typename K>
struct _hash_set_hash_t
{
    size_t operator()(K k) const
    {
        return (size_t)k;
    }
};
template <typename K>
struct _hash_set_pred_t
{
    bool operator()(K lk, K rk) const
    {
        return lk == rk;
    }
};

#if defined(BASE_IMPL_HASHSET_USE_STD_UNORDERED_SET)
template <typename K,
    typename Hash = _hash_set_hash_t<K>,
    typename Pred = _hash_set_pred_t<K> >
class hash_set : public BASE_IMPL_HASHSET_STD::unordered_set<K, Hash, Pred>
{
public:
    hash_set(const Hash &h = Hash(), const Pred &p = Pred())
        : BASE_IMPL_HASHSET_STD::unordered_set<K, Hash, Pred>(8, h, p)
    {
    }
};
#else
#if defined(BASE_CONFIG_MSVC)
template <typename K, typename Hash, typename Pred>
struct _hash_set_traits_t
{
    enum
    {
        bucket_size = 4,
        min_buckets = 8
    };
    size_t operator()(K k) const
    {
        return hash(k);
    }
    bool operator()(K lk, K rk) const
    {
        return !pred(lk, rk);
    }
    _hash_set_traits_t(const Hash &h, const Pred &p) : hash(h), pred(p)
    {
    }
    Hash hash;
    Pred pred;
};
template <typename K,
    typename Hash = _hash_set_hash_t<K>,
    typename Pred = _hash_set_pred_t<K> >
class hash_set : public stdext::hash_set<K, _hash_set_traits_t<K, Hash, Pred> >
{
public:
    hash_set(const Hash &h = Hash(), const Pred &p = Pred())
        : stdext::hash_set<K, _hash_set_traits_t<K, Hash, Pred> >(_hash_set_traits_t<K, Hash, Pred>(h, p))
    {
    }
};
#elif defined(BASE_CONFIG_GNUC)
template <typename K,
    typename Hash = _hash_set_hash_t<K>,
    typename Pred = _hash_set_pred_t<K> >
class hash_set : public __gnu_cxx::hash_set<K, Hash, Pred>
{
public:
    hash_set(const Hash &h = Hash(), const Pred &p = Pred())
        : __gnu_cxx::hash_set<K, Hash, Pred>(8, h, p)
    {
    }
};
#endif
#endif

}
}

#endif // BASE_IMPL_HASHSET_HPP_INCLUDED
