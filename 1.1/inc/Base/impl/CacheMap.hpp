/*
 * Base/impl/CacheMap.hpp
 *
 * Copyright 2010-2013 Navimatics Corporation. All rights reserved.
 */

#ifndef BASE_IMPL_CACHEMAP_HPP_INCLUDED
#define BASE_IMPL_CACHEMAP_HPP_INCLUDED

#include <Base/impl/Defines.hpp>
#include <Base/impl/Iterable.hpp>
#include <Base/impl/Object.hpp>
#include <Base/impl/Ref.hpp>
#include <Base/impl/hash_map.hpp>
#include <list>

namespace Base {
namespace impl {

class BASE_APISYM CacheMap : public Object, public Iterable<Object *>
{
public:
    CacheMap(ObjectComparer *comparer = 0);
    void autocollectObjects(bool ac);
    size_t count();
    Object *object(Object *key);
    bool getObject(Object *key, Object *&obj);
    void setObject(Object *key, Object *obj);
    void removeObject(Object *key);
    void removeAllObjects();
    void addObjects(CacheMap *map);
    Object **keys();
    Object **objects();
    void getKeysAndObjects(Object **&keybuf, Object **&objbuf);
    size_t maxCount();
    void setMaxCount(size_t value);
        /* set to 0 for unbounded cache */
    const char *strrepr();
    /* Iterable<Object *> */
    void *iterate(void *state, Object **bufp[2]);
private:
    typedef std::list<Ref<Object> > list_t;
    typedef std::pair<Ref<Object>, list_t::iterator> entry_t;
    typedef hash_map<Ref<Object>, entry_t, Object_hash_t, Object_equal_t> map_t;
    struct Iterator : public Object
    {
        Iterator(list_t &list) : p(list.begin()), q(list.end())
        {
        }
        list_t::iterator p, q;
    };
private:
    Ref<Object> _comparerRef;
    map_t _map;
    list_t _list;
    size_t _maxcnt;
    bool _nac;
};

template <typename K, typename V>
class BASE_APISYM GenericCacheMap : public CacheMap
{
public:
    V *object(K *key)
    {
        return (V *)inherited::object(key);
    }
    bool getObject(K *key, V *&obj)
    {
        return inherited::getObject(key, (Object *&)obj);
    }
    void setObject(K *key, V *obj)
    {
        inherited::setObject(key, obj);
    }
    void removeObject(K *key)
    {
        inherited::removeObject(key);
    }
    void addObjects(GenericCacheMap<K, V> *map)
    {
        inherited::addObjects(map);
    }
    K **keys()
    {
        return (K **)inherited::keys();
    }
    V **objects()
    {
        return (V **)inherited::objects();
    }
    void getKeysAndObjects(K **&keybuf, V **&objbuf)
    {
        inherited::getKeysAndObjects((Object **&)keybuf, (Object **&)objbuf);
    }
private:
    typedef CacheMap inherited;
};

}
}

#endif // BASE_IMPL_CACHEMAP_HPP_INCLUDED
