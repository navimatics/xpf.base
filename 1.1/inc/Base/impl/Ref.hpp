/*
 * Base/impl/Ref.hpp
 *
 * Copyright 2010-2013 Navimatics Corporation. All rights reserved.
 */

#ifndef BASE_IMPL_REF_HPP_INCLUDED
#define BASE_IMPL_REF_HPP_INCLUDED

#include <Base/impl/Defines.hpp>

namespace Base {
namespace impl {

template <typename T>
class BASE_APISYM Ref
{
private:
    T *_obj;
public:
    Ref() : _obj(0)
    {
    }
    explicit Ref(T *obj) : _obj(obj)
    {
        if (0 != _obj)
            _obj->retain();
    }
    Ref(const Ref& other) : _obj(other._obj)
    {
        if (0 != _obj)
            _obj->retain();
    }
    ~Ref()
    {
        if (0 != _obj)
            _obj->release();
    }
    Ref &operator=(const Ref &other)
    {
        if (0 != other._obj)
            other._obj->retain();
        if (0 != _obj)
            _obj->release();
        _obj = other._obj;
        return *this;
    }
    operator T *() const
    {
        return _obj;
    }
    T *operator->() const
    {
        return _obj;
    }
};

}
}

#endif // BASE_IMPL_REF_HPP_INCLUDED
