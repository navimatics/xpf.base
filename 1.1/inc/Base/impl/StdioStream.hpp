/*
 * Base/impl/StdioStream.hpp
 *
 * Copyright 2010-2013 Navimatics Corporation. All rights reserved.
 */

#ifndef BASE_IMPL_STDIOSTREAM_HPP_INCLUDED
#define BASE_IMPL_STDIOSTREAM_HPP_INCLUDED

#include <Base/impl/Defines.hpp>
#include <Base/impl/Stream.hpp>

namespace Base {
namespace impl {

class BASE_APISYM StdioStream : public Stream
{
public:
    static StdioStream *create(const char *path, const char *mode);
    StdioStream(FILE *file);
    ~StdioStream();
    ssize_t read(void *buf, size_t size);
    ssize_t write(const void *buf, size_t size);
    ssize_t seek(ssize_t offset, int whence = SEEK_SET);
    ssize_t close();
    FILE *file();
private:
    FILE *_file;
};

inline FILE *StdioStream::file()
{
    return _file;
}

}
}

#endif // BASE_IMPL_STDIOSTREAM_HPP_INCLUDED
