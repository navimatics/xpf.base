/*
 * Base/impl/JsonParser.hpp
 *
 * Copyright 2010-2013 Navimatics Corporation. All rights reserved.
 */

#ifndef BASE_IMPL_JSONPARSER_HPP_INCLUDED
#define BASE_IMPL_JSONPARSER_HPP_INCLUDED

#include <Base/impl/Defines.hpp>
#include <Base/impl/Object.hpp>
#include <Base/impl/Stream.hpp>
#include <Base/impl/TextParser.hpp>

namespace Base {
namespace impl {

class BASE_APISYM JsonParser : public TextParser
{
public:
    enum
    {
        InfinityExtension               = 0x00000001,
        StringEscapeExtension           = 0x00000002,
        _ObjectCodecExtension           = 0x00000004,
        ObjectCodecExtensions           =
            InfinityExtension | StringEscapeExtension | _ObjectCodecExtension,
        FloatNoneCompatibility          = 0x00010000,
    };
public:
    static const char *errorDomain();
    JsonParser(Stream *stream = 0);
    ~JsonParser();
    void reset();
    unsigned flags(unsigned mask);
    void setFlags(unsigned mask, unsigned value);
protected:
    virtual void startArray();
    virtual void endArray();
    virtual void startObject();
    virtual void endObject();
    virtual void nullValue();
    virtual void booleanValue(bool B);
    virtual void integerValue(long l);
    virtual void realValue(double d);
    virtual void characterData(const char *s, size_t len, bool isFinal, bool isKey);
    const char *_errorDomain();
    void parseBuffer(bool eof);
private:
    void _skipws();
    void _parseNumber();
    void _parseRestOfString();
    void _pushstate(int state);
    void _popstate();
    int _topstate();
    void _topstate(int state);
    bool _endstate();
private:
    int *_stack, *_stackp, *_stackendp;
    unsigned _flags;
};

}
}

#endif // BASE_IMPL_JSONPARSER_HPP_INCLUDED
