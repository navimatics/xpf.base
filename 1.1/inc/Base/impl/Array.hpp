/*
 * Base/impl/Array.hpp
 *
 * Copyright 2010-2013 Navimatics Corporation. All rights reserved.
 */

#ifndef BASE_IMPL_ARRAY_HPP_INCLUDED
#define BASE_IMPL_ARRAY_HPP_INCLUDED

#include <Base/impl/Defines.hpp>
#include <Base/impl/Iterable.hpp>
#include <Base/impl/Object.hpp>

namespace Base {
namespace impl {

class BASE_APISYM Array : public Object, public Iterable<Object *>
{
public:
    Array(size_t capacity = 0);
    ~Array();
    void autocollectObjects(bool ac);
    size_t count();
    Object *object(ssize_t index);
    ssize_t indexOfObject(Object *obj);
    ssize_t lastIndexOfObject(Object *obj);
    void addObject(Object *obj);
    void addObjects(Object **objs, size_t n);
    void insertObject(ssize_t index, Object *obj);
    void insertObjects(ssize_t index, Object **objs, size_t n);
    void replaceObject(ssize_t index, Object *obj);
    void replaceObjects(ssize_t index, Object **objs, size_t n);
    void moveObject(ssize_t index, ssize_t fromIndex);
    void moveObjects(ssize_t index, ssize_t fromIndex, size_t n);
    void removeObject(ssize_t index);
    void removeObjects(ssize_t index, size_t n);
    void removeAllObjects();
    Object **objects();
    const char *strrepr();
    /* Iterable<Object *> */
    void *iterate(void *state, Object **bufp[2]);
private:
    Object **_objs;
    bool _nac;
};

template <typename T>
class BASE_APISYM GenericArray : public Array
{
public:
    GenericArray(size_t capacity = 0) : inherited(capacity)
    {
    }
    T *object(ssize_t index)
    {
        return (T*)inherited::object(index);
    }
    ssize_t indexOfObject(T *obj)
    {
        return inherited::indexOfObject(obj);
    }
    ssize_t lastIndexOfObject(T *obj)
    {
        return inherited::lastIndexOfObject(obj);
    }
    void addObject(T *obj)
    {
        inherited::addObject(obj);
    }
    void addObjects(T **objs, size_t n)
    {
        inherited::addObjects(objs, n);
    }
    void insertObject(ssize_t index, T *obj)
    {
        inherited::insertObject(index, obj);
    }
    void insertObjects(ssize_t index, T **objs, size_t n)
    {
        inherited::insertObjects(index, objs, n);
    }
    void replaceObject(ssize_t index, T *obj)
    {
        inherited::replaceObject(index, obj);
    }
    void replaceObjects(ssize_t index, T **objs, size_t n)
    {
        inherited::replaceObjects(index, objs, n);
    }
    T **objects()
    {
        return (T **)inherited::objects();
    }
private:
    typedef Array inherited;
};

}
}

#endif // BASE_IMPL_ARRAY_HPP_INCLUDED
