/*
 * Base/impl/Snprintf.hpp
 *
 * Copyright 2010-2013 Navimatics Corporation. All rights reserved.
 */

#ifndef BASE_IMPL_SNPRINTF_HPP_INCLUDED
#define BASE_IMPL_SNPRINTF_HPP_INCLUDED

#include <Base/impl/Defines.hpp>

extern "C" {

BASE_APISYM int portable_snprintf(char *buf, size_t size, const char *format, ...);
BASE_APISYM int portable_vsnprintf(char *buf, size_t size, const char *format, va_list ap);
BASE_APISYM int portable_snprintfv(char *buf, size_t size, const char *format, void **args);

}

#endif // BASE_IMPL_SNPRINTF_HPP_INCLUDED
