/*
 * Base/impl/JsonReader.hpp
 *
 * Copyright 2010-2013 Navimatics Corporation. All rights reserved.
 */

#ifndef BASE_IMPL_JSONREADER_HPP_INCLUDED
#define BASE_IMPL_JSONREADER_HPP_INCLUDED

#include <Base/impl/Defines.hpp>
#include <Base/impl/Array.hpp>
#include <Base/impl/JsonParser.hpp>

namespace Base {
namespace impl {

class BASE_APISYM JsonEvent : public Object
{
public:
    static JsonEvent *create(int kind);
    static JsonEvent *create(int kind, bool B);
    static JsonEvent *create(int kind, long l);
    static JsonEvent *create(int kind, double d);
    static JsonEvent *create(int kind, const char *s, size_t len);
    ~JsonEvent();
    int kind();
    bool booleanValue();
    long integerValue();
    double realValue();
    const char *text();
    void _concat(const char *s, size_t len = -1);
private:
    JsonEvent();
private:
    int _kind;
    union
    {
        bool _b;
        long _l;
        double _d;
        const char *_text;
    };
};

class BASE_APISYM JsonReader : public JsonParser
{
public:
    JsonReader(Stream *stream = 0);
    ~JsonReader();
    void reset();
    JsonEvent *read();
        /* the returned object is valid until the next call to read() or ~JsonReader() */
    bool finished();
    void setFinished(bool value);
protected:
    void startArray();
    void endArray();
    void startObject();
    void endObject();
    void nullValue();
    void booleanValue(bool B);
    void integerValue(long l);
    void realValue(double d);
    void characterData(const char *s, size_t len, bool isFinal, bool isKey);
private:
    bool _finished;
    JsonEvent *_textev;
    Array *_queue;
    size_t _qindex;
};

inline int JsonEvent::kind()
{
    return _kind;
}
inline bool JsonEvent::booleanValue()
{
    return _b;
}
inline long JsonEvent::integerValue()
{
    return _l;
}
inline double JsonEvent::realValue()
{
    return _d;
}
inline const char *JsonEvent::text()
{
    return _text;
}
inline JsonEvent::JsonEvent()
{
}

}
}

#endif // BASE_IMPL_JSONREADER_HPP_INCLUDED
