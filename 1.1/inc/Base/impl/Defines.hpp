/*
 * Base/impl/Defines.hpp
 *
 * Copyright 2010-2013 Navimatics Corporation. All rights reserved.
 */

#ifndef BASE_IMPL_DEFINES_HPP_INCLUDED
#define BASE_IMPL_DEFINES_HPP_INCLUDED

#include <Base/impl/Config.h>

/* stdint.h: C99 notes 220, 221, 224 */
#define __STDC_LIMIT_MACROS
#define __STDC_CONSTANT_MACROS

#include <assert.h>
#include <ctype.h>
#include <errno.h>
#include <float.h>
#include <limits.h>
#include <math.h>
#include <stdarg.h>
#include <stddef.h>
#include <stdio.h>
#include <setjmp.h>
#include <stdlib.h>
#include <string.h>
#include <time.h>
#include <stdint.h>
#if defined(BASE_CONFIG_GNUC)
#include <sys/types.h>
#endif
#include <limits>
#include <new>
#include <typeinfo>

#if defined(BASE_CONFIG_MSVC)
#define BASE_FUNCSIG                    __FUNCSIG__
#define BASE_FUNCCONST
#elif defined(BASE_CONFIG_GNUC)
#define BASE_FUNCSIG                    __PRETTY_FUNCTION__
#define BASE_FUNCCONST                  __attribute__ ((const))
#endif

#if defined(BASE_CONFIG_WINDOWS) || defined(BASE_CONFIG_CYGWIN)
#define BASE_EXPORTSYM                  //__declspec(dllexport)
#define BASE_IMPORTSYM                  //__declspec(dllimport)
#define BASE_HIDDENSYM
#elif defined(BASE_CONFIG_GNUC)
#if __GNUC__ >= 4
#define BASE_EXPORTSYM                  __attribute__ ((visibility("default")))
#define BASE_IMPORTSYM                  __attribute__ ((visibility("default")))
#define BASE_HIDDENSYM                  __attribute__ ((visibility("hidden")))
#else
#define BASE_EXPORTSYM
#define BASE_IMPORTSYM
#define BASE_HIDDENSYM
#endif
#endif

#if defined(BASE_CONFIG_INTERNAL)
#define BASE_APISYM                     BASE_EXPORTSYM
#else
#define BASE_APISYM                     BASE_IMPORTSYM
#endif

#if defined(BASE_CONFIG_WINDOWS)
#define BASE_STDCALL                    __stdcall
#else
#define BASE_STDCALL
#endif

#if defined(BASE_CONFIG_MSVC)
#define BASE_LIKELY(x)                  (!!(x))
#elif defined(BASE_CONFIG_GNUC)
#define BASE_LIKELY(x)                  __builtin_expect(!!(x), 1)
#endif

#define BASE_ALIGN(x, s)                (((x) + (s - 1L)) & ~(s - 1L))
#define BASE_CONCAT_(a, b)              a##b
#define BASE_CONCAT(a, b)               BASE_CONCAT_(a, b)
#define BASE_COMMA_CONCAT(...)          __VA_ARGS__
#define BASE_COMPILE_ASSERT(cond)       ((void)sizeof(char[1 - 2 * !(cond)]))

#define NELEMS(a)                       (sizeof(a) / sizeof((a)[0]))

/*
 * Based on Laurent Deniau's original PP_NARG macro.
 * See http://groups.google.com/group/comp.std.c/browse_thread/thread/77ee8c8f92e4a3fb/ec8c9bf41bd6889d
 *
 * Modified to work under MSVC.
 */
#define BASE_PP_NARGS(...)              BASE_IMPL_PP_NARGS((__VA_ARGS__,\
    63,62,61,60,59,58,57,56,55,54,53,52,51,50,49,48,\
    47,46,45,44,43,42,41,40,39,38,37,36,35,34,33,32,\
    31,30,29,28,27,26,25,24,23,22,21,20,19,18,17,16,\
    15,14,13,12,11,10, 9, 8, 7, 6, 5, 4, 3, 2, 1))
#define BASE_IMPL_PP_NARGS(tuple)       BASE_IMPL_PP_NARGS_ tuple
#define BASE_IMPL_PP_NARGS_(            \
     _1, _2, _3, _4, _5, _6, _7, _8, _9,_10,_11,_12,_13,_14,_15,_16,\
    _17,_18,_19,_20,_21,_22,_23,_24,_25,_26,_27,_28,_29,_30,_31,_32,\
    _33,_34,_35,_36,_37,_38,_39,_40,_41,_42,_43,_44,_45,_46,_47,_48,\
    _49,_50,_51,_52,_53,_54,_55,_56,_57,_58,_59,_60,_61,_62,_63,  N, ...) N

/* Hallvard B Furuseth's IMAX_BITS
 * See http://groups.google.com/group/comp.lang.c/browse_thread/thread/601e6e362022291d/febf8383d810d7df
 *
 * Find the number of bits in <inttype>_MAX,
 * or of any value 2**N-1 where N (the number of bits) <= 2039 (255*8-1)
 *
 * Explanation:
 * - 1st part returns (number of complete 8bit-chunks in M) * 8:
 *    M%255 = 0/1/3/7/../127 returns the remaining bits.
 *    M/(M%255+1) strips off these and returns the 8-bit chunks => 0xffffff..,
 *      / 255 => 0x010101.. = 256**0 + 256**1 .. 256**N for N+1 8-bit chunks,
 *        .. (256**p) % 255 = ((255 + 1)**p) % 255 = (1**p) % 255 = 1.
 * - last part just happens to return #bits in M%255 when M%255=0/1/3/7/../127.
 */
#define IMAX_BITS(m)                    ((m)/((m)%255+1) / 255%255*8 + 7-86/((m)%255+12))

/* Floating point printf formats
 *
 * *_ROUNDTRIP_* formats that allow for conversion to string and back without loss of precision.
 * See http://en.wikipedia.org/wiki/IEEE_floating_point#Character_representation
 *
 * *_PRECISION_* formats that print as much precision as the underlying type has.
 */
#define DBL_ROUNDTRIP_G_FMT             "%.17g"
#define FLT_ROUNDTRIP_G_FMT             "%.9g"
#define DBL_PRECISION_G_FMT             "%.15g"
#define FLT_PRECISION_G_FMT             "%.6g"

#if defined(BASE_CONFIG_MSVC)
typedef signed __int8 int8_t;
typedef signed __int16 int16_t;
typedef signed __int32 int32_t;
typedef signed __int64 int64_t;
typedef unsigned __int8 uint8_t;
typedef unsigned __int16 uint16_t;
typedef unsigned __int32 uint32_t;
typedef unsigned __int64 uint64_t;
#if !defined(INT8_MIN)
#define INT8_MIN                        _I8_MIN
#endif
#if !defined(INT16_MIN)
#define INT16_MIN                       _I16_MIN
#endif
#if !defined(INT32_MIN)
#define INT32_MIN                       _I32_MIN
#endif
#if !defined(INT64_MIN)
#define INT64_MIN                       _I64_MIN
#endif
#if !defined(INT8_MAX)
#define INT8_MAX                        _I8_MAX
#endif
#if !defined(INT16_MAX)
#define INT16_MAX                       _I16_MAX
#endif
#if !defined(INT32_MAX)
#define INT32_MAX                       _I32_MAX
#endif
#if !defined(INT64_MAX)
#define INT64_MAX                       _I64_MAX
#endif
#if !defined(UINT8_MAX)
#define UINT8_MAX                       _UI8_MAX
#endif
#if !defined(UINT16_MAX)
#define UINT16_MAX                      _UI16_MAX
#endif
#if !defined(UINT32_MAX)
#define UINT32_MAX                      _UI32_MAX
#endif
#if !defined(UINT64_MAX)
#define UINT64_MAX                      _UI64_MAX
#endif
#if defined(BASE_CONFIG_ARCH64)
typedef __int64 ssize_t;
typedef __int64 intptr_t;
typedef unsigned __int64 uintptr_t;
#if !defined(SIZE_T_MAX)
#define SIZE_T_MAX                      UINT64_MAX
#endif
#if !defined(INTPTR_MIN)
#define INTPTR_MIN                      INT64_MIN
#endif
#if !defined(INTPTR_MAX)
#define INTPTR_MAX                      INT64_MAX
#endif
#if !defined(UINTPTR_MAX)
#define UINTPTR_MAX                     UINT64_MAX
#endif
#else
typedef int ssize_t;
typedef __int32 intptr_t;
typedef unsigned __int32 uintptr_t;
#if !defined(SIZE_T_MAX)
#define SIZE_T_MAX                      UINT32_MAX
#endif
#if !defined(INTPTR_MIN)
#define INTPTR_MIN                      INT32_MIN
#endif
#if !defined(INTPTR_MAX)
#define INTPTR_MAX                      INT32_MAX
#endif
#if !defined(UINTPTR_MAX)
#define UINTPTR_MAX                     UINT32_MAX
#endif
#endif
#endif

#if defined(BASE_CONFIG_MSVC)
#if !defined(INFINITY)
#define INFINITY                        std::numeric_limits<float>::infinity()
#endif
#if !defined(NAN)
#define NAN                             std::numeric_limits<float>::quiet_NaN()
#endif
#endif

#if defined(BASE_CONFIG_MSVC)
#if !defined(va_copy)
#define va_copy(dst, src)               ((dst) = (src))
#endif
#endif

#if defined(BASE_CONFIG_MSVC) || defined(BASE_CONFIG_ANDROID)
inline char *stpcpy(char *t, const char *s)
{
    while (*t++ = *s++)
        ;
    return t - 1;
}
#endif
#if defined(BASE_CONFIG_MSVC)
inline long long strtoll(const char *p, char **endp, int base)
{
    return _strtoi64(p, endp, base);
}
inline unsigned long long strtoull(const char *p, char **endp, int base)
{
    return _strtoui64(p, endp, base);
}
#endif
#if !defined(BASE_CONFIG_MSVC)
inline int stricmp(const char *p1, const char *p2)
{
    const unsigned char *s1 = (const unsigned char *)p1;
    const unsigned char *s2 = (const unsigned char *)p2;
    unsigned char c1, c2;
    do
    {
        c1 = tolower(*s1++);
        c2 = tolower(*s2++);
        if (int r = c1 - c2)
            return r;
    } while (c1);
    return 0;
}
inline int memicmp(const void *p1, const void *p2, size_t size)
{
    const unsigned char *s1 = (const unsigned char *)p1;
    const unsigned char *e1 = s1 + size;
    const unsigned char *s2 = (const unsigned char *)p2;
    while (e1 > s1)
    {
        unsigned char c1 = tolower(*s1++);
        unsigned char c2 = tolower(*s2++);
        if (int r = c1 - c2)
            return r;
    }
    return 0;
}
#endif

namespace Base {
namespace impl {

typedef uintptr_t index_t;
typedef intptr_t sindex_t;

typedef unsigned wait_timeout_t;
    /* wait timeout in milliseconds */
enum { wait_timeout_infinite = UINT_MAX };

BASE_APISYM bool isFinite(float value);
BASE_APISYM bool isFinite(double value);
BASE_APISYM bool isFinite(long double value);
BASE_APISYM bool isInfinite(float value);
BASE_APISYM bool isInfinite(double value);
BASE_APISYM bool isInfinite(long double value);
BASE_APISYM bool isNan(float value);
BASE_APISYM bool isNan(double value);
BASE_APISYM bool isNan(long double value);

inline bool isFinite(float value)
{
#if defined(BASE_CONFIG_MSVC)
    return !!_finite(value);
#else
    return !!isfinite(value);
#endif
}
inline bool isFinite(double value)
{
#if defined(BASE_CONFIG_MSVC)
    return !!_finite(value);
#else
    return !!isfinite(value);
#endif
}
inline bool isFinite(long double value)
{
#if defined(BASE_CONFIG_MSVC)
    return !!_finite(value);
#else
    return !!isfinite(value);
#endif
}
inline bool isInfinite(float value)
{
#if defined(BASE_CONFIG_MSVC)
    return !_finite(value) && !_isnan(value);
#else
    return !!isinf(value);
#endif
}
inline bool isInfinite(double value)
{
#if defined(BASE_CONFIG_MSVC)
    return !_finite(value) && !_isnan(value);
#else
    return !!isinf(value);
#endif
}
inline bool isInfinite(long double value)
{
#if defined(BASE_CONFIG_MSVC)
    return !_finite(value) && !_isnan(value);
#else
    return !!isinf(value);
#endif
}
inline bool isNan(float value)
{
#if defined(BASE_CONFIG_MSVC)
    return !!_isnan(value);
#else
    return !!isnan(value);
#endif
}
inline bool isNan(double value)
{
#if defined(BASE_CONFIG_MSVC)
    return !!_isnan(value);
#else
    return !!isnan(value);
#endif
}
inline bool isNan(long double value)
{
#if defined(BASE_CONFIG_MSVC)
    return !!_isnan(value);
#else
    return !!isnan(value);
#endif
}

struct BASE_APISYM None
{
    operator short()
    {
        return SHRT_MIN;
    }
    operator unsigned short()
    {
        return USHRT_MAX;
    }
    operator int()
    {
        return INT_MIN;
    }
    operator unsigned int()
    {
        return UINT_MAX;
    }
    operator long()
    {
        return LONG_MIN;
    }
    operator unsigned long()
    {
        return ULONG_MAX;
    }
    operator long long()
    {
        return LLONG_MIN;
    }
    operator unsigned long long()
    {
        return ULLONG_MAX;
    }
    operator float()
    {
        return NAN;
    }
    operator double()
    {
        return NAN;
    }
};

BASE_APISYM bool isNone(short value);
BASE_APISYM bool isNone(unsigned short value);
BASE_APISYM bool isNone(int value);
BASE_APISYM bool isNone(unsigned int value);
BASE_APISYM bool isNone(long value);
BASE_APISYM bool isNone(unsigned long value);
BASE_APISYM bool isNone(long long value);
BASE_APISYM bool isNone(unsigned long long value);
BASE_APISYM bool isNone(float value);
BASE_APISYM bool isNone(double value);

inline bool isNone(short value)
{
    return SHRT_MIN == value;
}
inline bool isNone(unsigned short value)
{
    return USHRT_MAX == value;
}
inline bool isNone(int value)
{
    return INT_MIN == value;
}
inline bool isNone(unsigned int value)
{
    return UINT_MAX == value;
}
inline bool isNone(long value)
{
    return LONG_MIN == value;
}
inline bool isNone(unsigned long value)
{
    return ULONG_MAX == value;
}
inline bool isNone(long long value)
{
    return LLONG_MIN == value;
}
inline bool isNone(unsigned long long value)
{
    return ULLONG_MAX == value;
}
inline bool isNone(float value)
{
    return isNan(value);
}
inline bool isNone(double value)
{
    return isNan(value);
}

BASE_APISYM void MemoryAllocationError();
BASE_APISYM void *Malloc(size_t size);
BASE_APISYM void *Realloc(void *p, size_t size);

inline void *Malloc(size_t size)
{
    void *p = malloc(size);
    if (0 == p)
        MemoryAllocationError();
    return p;
}
inline void *Realloc(void *p, size_t size)
{
    p = realloc(p, size);
    if (0 == p)
        MemoryAllocationError();
    return p;
}

}
}

#endif // BASE_IMPL_DEFINES_HPP_INCLUDED
