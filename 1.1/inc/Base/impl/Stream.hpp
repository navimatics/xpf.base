/*
 * Base/impl/Stream.hpp
 *
 * Copyright 2010-2013 Navimatics Corporation. All rights reserved.
 */

#ifndef BASE_IMPL_STREAM_HPP_INCLUDED
#define BASE_IMPL_STREAM_HPP_INCLUDED

#include <Base/impl/Defines.hpp>
#include <Base/impl/Object.hpp>

namespace Base {
namespace impl {

class BASE_APISYM Stream : public Object
{
public:
    virtual ssize_t read(void *buf, size_t size);
    virtual ssize_t write(const void *buf, size_t size);
    virtual ssize_t seek(ssize_t offset, int whence = SEEK_SET);
    virtual ssize_t close();
};

}
}

#endif // BASE_IMPL_STREAM_HPP_INCLUDED
