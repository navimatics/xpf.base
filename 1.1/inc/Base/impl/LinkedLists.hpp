/*
 * Base/impl/LinkedLists.hpp
 *
 * Copyright 2010-2013 Navimatics Corporation. All rights reserved.
 */

#ifndef BASE_IMPL_LINKEDLISTS_HPP_INCLUDED
#define BASE_IMPL_LINKEDLISTS_HPP_INCLUDED

#include <Base/impl/Defines.hpp>

namespace Base {
namespace impl {

/*
 * Singly linked list: LIFO
 *
 * SSS is sentinel node
 *
 * Empty:
 *
 * +-----+
 * | SSS | -+
 * +-----+  |
 *    ^-----+
 *
 * Single node:
 *
 * +-----+    +-----+
 * | SSS | -> |  A  | -+
 * +-----+    +-----+  |
 *    ^----------------+
 *
 * Multiple nodes:
 *
 * +-----+    +-----+    +-----+           +-----+
 * | SSS | -> |  A  | -> |  B  | -> ... -> |  Z  | -+
 * +-----+    +-----+    +-----+           +-----+  |
 *    ^---------------------------------------------+
 */
template <typename T>
inline void slist_reset(T *node)
{
    node->next = node;
}
template <typename T>
inline bool slist_is_empty(T *node)
{
    return node->next == node;
}
template <typename T>
inline void slist_push(T *node, T *newnode)
{
    newnode->next = node->next;
    node->next = newnode;
}
template <typename T>
inline T *slist_pop(T *node)
{
    T *next = node->next;
    node->next = next->next;
    return next;
}

/*
 * Singly linked list: FIFO
 *
 * SSS is sentinel node (always at ''back->next'')
 *
 * Empty:
 *
 *  back
 *    |
 *    v
 * +-----+
 * | SSS | -+
 * +-----+  |
 *    ^-----+
 *
 * Single node:
 *
 *             back
 *               |
 *               v
 * +-----+    +-----+
 * | SSS | -> |  A  | -+
 * +-----+    +-----+  |
 *    ^----------------+
 *
 * Multiple nodes:
 *
 *                                          back
 *                                            |
 *                                            v
 * +-----+    +-----+    +-----+           +-----+
 * | SSS | -> |  A  | -> |  B  | -> ... -> |  Z  | -+
 * +-----+    +-----+    +-----+           +-----+  |
 *    ^---------------------------------------------+
 */
template <typename T>
inline void slist_back_reset(T *&back, T *node)
{
    back = node;
    node->next = node;
}
template <typename T>
inline void slist_push_back(T *&back, T *newnode)
{
    newnode->next = back->next;
    back->next = newnode;
    back = newnode;
}
template <typename T>
inline T *slist_pop_front(T *&back)
{
    T *node = back->next;
    T *next = node->next;
    node->next = next->next;
    if (back == next)
        back = node;
    return next;
}

/*
 * Doubly linked list
 *
 * SSS is sentinel node
 *
 * Empty:
 *
 * +----v
 * |  +-----+
 * |  |     | -+
 * |  | SSS |  |
 * +- |     |  |
 *    +-----+  |
 *        ^----+
 *
 * Single node:
 *
 * +---------------v
 * |  +-----+    +-----+
 * |  |     | -> |     | -+
 * |  | SSS |    |  A  |  |
 * +- |     | <- |     |  |
 *    +-----+    +-----+  |
 *        ^---------------+
 *
 * Multiple nodes:
 *
 * +--------------------------------------------v
 * |  +-----+    +-----+    +-----+           +-----+
 * |  |     | -> |     | -> |     | -> ... -> |     | -+
 * |  | SSS |    |  A  |    |  B  |           |  Z  |  |
 * +- |     | <- |     | <- |     | <- ... <- |     |  |
 *    +-----+    +-----+    +-----+           +-----+  |
 *        ^--------------------------------------------+
 */
template <typename T>
inline void dlist_reset(T *node)
{
    node->next = node->prev = node;
}
template <typename T>
inline bool dlist_is_empty(T *node)
{
    return node->next == node;
}
template <typename T>
inline void dlist_insert_before(T *node, T *newnode)
{
    T *prev = node->prev;
    newnode->next = node;
    newnode->prev = prev;
    prev->next = newnode;
    node->prev = newnode;
}
template <typename T>
inline void dlist_insert_after(T *node, T *newnode)
{
    T *next = node->next;
    newnode->next = next;
    newnode->prev = node;
    node->next = newnode;
    next->prev = newnode;
}
template <typename T>
inline void dlist_remove(T *node)
{
    T *next = node->next;
    T *prev = node->prev;
    prev->next = next;
    next->prev = prev;
}
template <typename T>
inline void dlist_push_front(T *node, T *newnode)
{
    dlist_insert_before(node->next, newnode);
}
template <typename T>
inline void dlist_push_back(T *node, T *newnode)
{
    dlist_insert_after(node->prev, newnode);
}
template <typename T>
inline T *dlist_pop_front(T *node)
{
    T *next = node->next;
    dlist_remove(next);
    return next;
}
template <typename T>
inline T *dlist_pop_back(T *node)
{
    T *prev = node->prev;
    dlist_remove(prev);
    return prev;
}

}
}

#endif // BASE_IMPL_LINKEDLISTS_HPP_INCLUDED
