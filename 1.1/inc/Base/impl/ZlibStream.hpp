/*
 * Base/impl/ZlibStream.hpp
 *
 * Copyright 2010-2013 Navimatics Corporation. All rights reserved.
 */

#ifndef BASE_IMPL_ZLIBSTREAM_HPP_INCLUDED
#define BASE_IMPL_ZLIBSTREAM_HPP_INCLUDED

#include <Base/impl/Defines.hpp>
#include <Base/impl/Stream.hpp>

namespace Base {
namespace impl {

class BASE_APISYM ZlibStream : public Stream
{
public:
    ZlibStream(Stream *stream, const char *mode);
        /* mode:
         * 'r': read mode; 'w': write mode
         * 'b': ignored
         * '0': decompress mode; '1'-'9': compress at level
         *      default: compress at '9'
         * 'g': gzip mode; 'z': zlib mode
         *      default: gzip mode when '1'-'9', autodetect when '0'
         */
    ~ZlibStream();
    ssize_t read(void *buf, size_t size);
    ssize_t write(const void *buf, size_t size);
    ssize_t close();
private:
    void init();
private:
    Stream *_stream;
    int _mode:8, _head:8, _clevel:8;
    bool _eof:1, _eos:1, _err:1;
    void *_iobuf;
    void *_zp;
};

}
}

#endif // BASE_IMPL_ZLIBSTREAM_HPP_INCLUDED
