/*
 * Base/impl/FileSystemMonitor.hpp
 *
 * Copyright 2010-2015 Navimatics Corporation. All rights reserved.
 */

#ifndef BASE_IMPL_FILESYSTEMMONITOR_HPP_INCLUDED
#define BASE_IMPL_FILESYSTEMMONITOR_HPP_INCLUDED

#include <Base/impl/Defines.hpp>
#include <Base/impl/Delegate.hpp>
#include <Base/impl/Object.hpp>
#include <Base/impl/OrderedSet.hpp>
#include <Base/impl/Thread.hpp>

namespace Base {
namespace impl {

struct BASE_APISYM FileSystemMonitorEvent
{
    enum
    {
        /*
         * The actions below MUST be interpreted as follows:
         *     Rescan
         *         The event consumer MUST rescan the path. If the path specifies a directory,
         *         the event consumer MUST rescan the directory and all its subdirectories
         *         recursively.
         *     Create, Remove, Modify
         *         The event consumer MUST treat these actions only as hints as to what happened.
         *         The event consumer MUST (l)stat the specified path to determine what the current
         *         state is.
         */
        Rescan = 0x0010,
        Create = 0x0001,
        Remove = 0x0002,
        Modify = 0x0004,
    };
    unsigned action;
    const char *path;
};
class BASE_APISYM FileSystemMonitor : public Object
{
public:
    typedef Delegate<void (FileSystemMonitorEvent *events)> Delegate;
        /* events is a CArray, but any strings within are not always CString's */
public:
    static const char *errorDomain();
    FileSystemMonitor(Delegate delegate, Thread *eventThread = 0);
    ~FileSystemMonitor();
    void addPath(const char *path);
    void removePath(const char *path);
    bool start();
    void stop();
    bool isStopped();
private:
    Delegate _delegate;
    Thread *_eventThread;
    OrderedSet *_paths;
    Thread *_thread;
};

}
}

#endif // BASE_IMPL_FILESYSTEMMONITOR_HPP_INCLUDED
