/*
 * Base/impl/OrderedMap.hpp
 *
 * Copyright 2010-2013 Navimatics Corporation. All rights reserved.
 */

#ifndef BASE_IMPL_ORDEREDMAP_HPP_INCLUDED
#define BASE_IMPL_ORDEREDMAP_HPP_INCLUDED

#include <Base/impl/Defines.hpp>
#include <Base/impl/Iterable.hpp>
#include <Base/impl/Map.hpp>
#include <Base/impl/Object.hpp>
#include <Base/impl/ObjectCodec.hpp>
#include <Base/impl/Ref.hpp>
#include <map>

namespace Base {
namespace impl {

class BASE_APISYM OrderedMap : public Object, public Iterable<Object *>
{
public:
    OrderedMap(ObjectComparer *comparer = 0);
    void autocollectObjects(bool ac);
    size_t count();
    Object *object(Object *key);
    bool getObject(Object *key, Object *&obj);
    void setObject(Object *key, Object *obj);
    void removeObject(Object *key);
    void removeAllObjects();
    void addObjects(OrderedMap *map);
    Object **keys();
    Object **objects();
    void getKeysAndObjects(Object **&keybuf, Object **&objbuf);
    const char *strrepr();
    /* Iterable<Object *> */
    void *iterate(void *state, Object **bufp[2]);
private:
    typedef std::map<Ref<Object>, Ref<Object>, Object_less_t> map_t;
    struct Iterator : public Object
    {
        Iterator(map_t &map) : p(map.begin()), q(map.end())
        {
        }
        map_t::iterator p, q;
    };
private:
    Ref<Object> _comparerRef;
    map_t _map;
    bool _nac;
    /* ObjectCodec use */
    Map *__map();
    void __setMap(Map *map);
    ObjectCodecDeclare();
};

template <typename K, typename V>
class BASE_APISYM GenericOrderedMap : public OrderedMap
{
public:
    V *object(K *key)
    {
        return (V *)inherited::object(key);
    }
    bool getObject(K *key, V *&obj)
    {
        return inherited::getObject(key, (Object *&)obj);
    }
    void setObject(K *key, V *obj)
    {
        inherited::setObject(key, obj);
    }
    void removeObject(K *key)
    {
        inherited::removeObject(key);
    }
    void addObjects(GenericOrderedMap<K, V> *map)
    {
        inherited::addObjects(map);
    }
    K **keys()
    {
        return (K **)inherited::keys();
    }
    V **objects()
    {
        return (V **)inherited::objects();
    }
    void getKeysAndObjects(K **&keybuf, V **&objbuf)
    {
        inherited::getKeysAndObjects((Object **&)keybuf, (Object **&)objbuf);
    }
private:
    typedef OrderedMap inherited;
};

}
}

#endif // BASE_IMPL_ORDEREDMAP_HPP_INCLUDED
