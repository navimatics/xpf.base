/*
 * Base/impl/TextParser.hpp
 *
 * Copyright 2010-2013 Navimatics Corporation. All rights reserved.
 */

#ifndef BASE_IMPL_TEXTPARSER_HPP_INCLUDED
#define BASE_IMPL_TEXTPARSER_HPP_INCLUDED

#include <Base/impl/Defines.hpp>
#include <Base/impl/Object.hpp>
#include <Base/impl/Stream.hpp>

namespace Base {
namespace impl {

class BASE_APISYM TextParser : public Object
{
public:
    static const char *errorDomain();
    TextParser(Stream *stream = 0, size_t bufsiz = 16384);
    ~TextParser();
    virtual void reset();
    virtual ssize_t parse(const void *buf, size_t size);
    virtual ssize_t parse();
protected:
    virtual const char *_errorDomain();
    virtual ssize_t readBuffer(void *buf, size_t size);
    virtual void parseBuffer(bool eof);
    void raise();
    void raise(const char *format, ...);
    bool _testnext(ssize_t i);
    char _peeknext(ssize_t i);
    char peeknext(ssize_t i);
    void skipnext(ssize_t i);
    bool _testchar();
    char _peekchar();
    char peekchar();
    void skipchar();
    void expect(const char *s, size_t len);
    char *_charptr();
    bool _testptr(const char *p);
private:
    int _parseBuffer(bool eof);
protected:
    Stream *_stream;
    char *_bufp, *_nextp, *_freep, *_endp;
    jmp_buf _raisebuf;
    const char *_raisemsg;
};

inline bool TextParser::_testnext(ssize_t i)
{
    return _nextp + i < _freep;
}
inline char TextParser::_peeknext(ssize_t i)
{
    return _nextp[i];
}
inline char TextParser::peeknext(ssize_t i)
{
    if (_nextp + i >= _freep)
        raise();
    return _nextp[i];
}
inline void TextParser::skipnext(ssize_t i)
{
    _nextp += i;
}
inline bool TextParser::_testchar()
{
    return _nextp < _freep;
}
inline char TextParser::_peekchar()
{
    return *_nextp;
}
inline char TextParser::peekchar()
{
    if (_nextp >= _freep)
        raise();
    return *_nextp;
}
inline void TextParser::skipchar()
{
    _nextp++;
}
inline char *TextParser::_charptr()
{
    return _nextp;
}
inline bool TextParser::_testptr(const char *p)
{
    return p < _freep;
}

}
}

#endif // BASE_IMPL_TEXTPARSER_HPP_INCLUDED
