/*
 * Base/impl/JsonWriter.hpp
 *
 * Copyright 2010-2013 Navimatics Corporation. All rights reserved.
 */

#ifndef BASE_IMPL_JSONWRITER_HPP_INCLUDED
#define BASE_IMPL_JSONWRITER_HPP_INCLUDED

#include <Base/impl/Defines.hpp>
#include <Base/impl/Object.hpp>
#include <Base/impl/Stream.hpp>
#include <Base/impl/JsonReader.hpp>

namespace Base {
namespace impl {

class BASE_APISYM JsonWriter : public Object
{
public:
    enum
    {
        InfinityExtension               = 0x00000001,
        StringEscapeExtension           = 0x00000002,
        _ObjectCodecExtension           = 0x00000004,
        ObjectCodecExtensions           =
            InfinityExtension | StringEscapeExtension | _ObjectCodecExtension,
        FloatNoneCompatibility          = 0x00010000,
    };
public:
    JsonWriter(Stream *stream = 0, bool pretty = false);
    ~JsonWriter();
    bool writeRaw(const char *chars, size_t len = -1);
    bool writeStartArray();
    bool writeEndArray();
    bool writeStartObject();
    bool writeEndObject();
    bool writeNullValue();
    bool writeBooleanValue(bool B);
    bool writeIntegerValue(long l);
    bool writeRealValue(double d);
    bool writeText(const char *text, size_t len = -1);
    bool _writeSpecialText(const char *text, size_t len = -1);
    bool write(JsonEvent *event);
    bool finished();
    unsigned flags(unsigned mask);
    void setFlags(unsigned mask, unsigned value);
protected:
    virtual ssize_t writeBuffer(const void *buf, size_t size);
private:
    void _pushstate(int state);
    void _popstate();
    int _topstate();
    void _topstate(int state);
    void _writeDelim();
    void _writeLine();
private:
    Stream *_stream;
    int *_stack, *_stackp, *_stackendp;
    bool _finished:1;
    bool _specialText:1;
    bool _pretty:1;
    unsigned _flags;
};

}
}

#endif // BASE_IMPL_JSONWRITER_HPP_INCLUDED
