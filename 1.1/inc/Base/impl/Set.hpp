/*
 * Base/impl/Set.hpp
 *
 * Copyright 2010-2013 Navimatics Corporation. All rights reserved.
 */

#ifndef BASE_IMPL_SET_HPP_INCLUDED
#define BASE_IMPL_SET_HPP_INCLUDED

#include <Base/impl/Defines.hpp>
#include <Base/impl/Array.hpp>
#include <Base/impl/Iterable.hpp>
#include <Base/impl/Object.hpp>
#include <Base/impl/ObjectCodec.hpp>
#include <Base/impl/Ref.hpp>
#include <Base/impl/hash_set.hpp>

namespace Base {
namespace impl {

class BASE_APISYM Set : public Object, public Iterable<Object *>
{
public:
    Set(ObjectComparer *comparer = 0);
    void autocollectObjects(bool ac);
    size_t count();
    bool containsObject(Object *obj);
    Object *anyObject();
    void addObject(Object *obj);
    void removeObject(Object *obj);
    void removeAllObjects();
    void addObjects(Set *set);
    Object **objects();
    const char *strrepr();
    /* Iterable<Object *> */
    void *iterate(void *state, Object **bufp[2]);
private:
    typedef hash_set<Ref<Object>, Object_hash_t, Object_equal_t> set_t;
    struct Iterator : public Object
    {
        Iterator(set_t &set) : p(set.begin()), q(set.end())
        {
        }
        set_t::iterator p, q;
    };
private:
    Ref<Object> _comparerRef;
    set_t _set;
    bool _nac;
    /* ObjectCodec use */
    Array *__array();
    void __setArray(Array *array);
    ObjectCodecDeclare();
};

template <typename T>
class BASE_APISYM GenericSet : public Set
{
public:
    bool containsObject(T *obj)
    {
        return inherited::containsObject(obj);
    }
    T *anyObject()
    {
        return (T *)inherited::anyObject();
    }
    void addObject(T *obj)
    {
        inherited::addObject(obj);
    }
    void removeObject(T *obj)
    {
        inherited::removeObject(obj);
    }
    T **objects()
    {
        return (T **)inherited::objects();
    }
private:
    typedef Set inherited;
};

}
}

#endif // BASE_IMPL_SET_HPP_INCLUDED
