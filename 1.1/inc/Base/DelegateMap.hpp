/*
 * Base/DelegateMap.hpp
 *
 * Copyright 2010-2013 Navimatics Corporation. All rights reserved.
 */

#ifndef BASE_DELEGATEMAP_HPP_INCLUDED
#define BASE_DELEGATEMAP_HPP_INCLUDED

#include <Base/Defines.hpp>
#include <Base/Delegate.hpp>
#include <Base/Object.hpp>
#include <Base/impl/DelegateMap.hpp>

namespace Base {

using Base::impl::DelegateMap;
using Base::impl::GenericDelegateMap;

}

#endif // BASE_DELEGATEMAP_HPP_INCLUDED
