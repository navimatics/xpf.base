/*
 * Base/CString.hpp
 *
 * Copyright 2010-2013 Navimatics Corporation. All rights reserved.
 */
/**
 * Definitions for a string class that encapsulates native C strings and related functions.
 *
 * Base::CString
 *     This class encapsulates a native C string.
 *
 *     This encapsulation is done in such a way that one can easily move from a Base::CString to
 *     the encapsulated C string and vice-versa easily. This is achieved by careful memory-layout
 *     of a Base::CString; a Base::CString and its encapsulated C string occupy the same memory.
 *
 *         vptr         <-- start of Base::CString
 *         _length
 *         _metric
 *         "text\0"     <-- start of encapsulated C string ("text")
 *
 *     Because of the special memory layout of Base::CString's objects, new Base::CString's
 *     are always created using the create() static methods rather than operator new. For the same
 *     reasons Base::CString objects cannot be extended.
 *
 *     The basic methods of Base::CString are length() and chars(). There is also a slice()
 *     method that can be used to return a substring of a Base::CString as a new Base::CString.
 *
 *     Base::CString's can be immutable or mutable. By default Base::CString's start life as
 *     immutable. Immutable Base::CString's can freely be used in containers that require
 *     hashing, etc.
 *
 *     Mutable Base::CString's can be created through the use of the _concat() method. [Note:
 *     they can now also be created directly using the createWithCapacity() methods.] Mutable
 *     Base::CString's are used to quickly create a string by efficiently concatenating it from
 *     individual substrings. The _concat() method will always return a mutable Base::CString that
 *     is the concatenation of the acted upon Base::CString and the C string in its call arguments;
 *     this mutable Base::CString may also contain some free space so that subsequent concatenations
 *     can be performed without additional memory operations (malloc/free). If a new Base::CString
 *     was created as a result of the concatenation (for example, if the acted upon Base::CString
 *     is an immutable one, or if the length of the concatenated strings exceeds the size of the
 *     acted upon mutable Base::CString), the acted upon Base::CString IS RELEASED.
 *
 *     The _concat() method allows for the following usage pattern without memory management
 *     concerns:
 *
 *         Base::CString *str = Base::CString::create("some text");
 *             // str points to immutable Base::CString "some text"
 *         str = str->_concat(" again");
 *             // str now points to mutable Base::CString "some text again"
 *             // the original Base::CString has been released by _concat()
 *
 *     One should exercise caution when using mutable Base::CString's in containers.
 */

#ifndef BASE_CSTRING_HPP_INCLUDED
#define BASE_CSTRING_HPP_INCLUDED

#include <Base/Defines.hpp>
#include <Base/Object.hpp>
#include <Base/impl/CString.hpp>

namespace Base {

using Base::impl::CString;
using Base::impl::cstring;
using Base::impl::cstringWithCapacity;
using Base::impl::cstringf;
using Base::impl::cstr_new;
using Base::impl::cstr_newWithCapacity;
using Base::impl::cstr_newf;
using Base::impl::cstr_assign;
using Base::impl::cstr_assign_copy;
using Base::impl::cstr_obj;
using Base::impl::cstr_retain;
using Base::impl::cstr_release;
using Base::impl::cstr_autorelease;
using Base::impl::cstr_autocollect;
using Base::impl::cstr_length;
using Base::impl::cstr_slice;
using Base::impl::cstr_concat;

using Base::impl::FnmNoEscape;
using Base::impl::FnmPathname;
using Base::impl::FnmPeriod;
using Base::impl::FnmLeadingDir;
using Base::impl::FnmCaseFold;
using Base::impl::FnmCurlyBraces;
using Base::impl::cstr_fnmatch;

using Base::impl::cstr_path_realpath;
using Base::impl::cstr_path_normalize;
using Base::impl::cstr_path_concat;
using Base::impl::cstr_path_basename;
using Base::impl::cstr_path_extension;
using Base::impl::cstr_path_setBasename;
using Base::impl::cstr_path_setExtension;

using Base::impl::cstr_uri_parts;
using Base::impl::cstr_uri_parseraw;
using Base::impl::cstr_uri_composeraw;
using Base::impl::cstr_uri_parse;
using Base::impl::cstr_uri_compose;
using Base::impl::cstr_uri_normalize;
using Base::impl::cstr_uri_concat;
using Base::impl::cstr_uri_escape;
using Base::impl::cstr_uri_unescape;
using Base::impl::cstr_uri_query_extract;
using Base::impl::cstr_uri_query_concat;

using Base::impl::TransientCString;

}

#endif // BASE_CSTRING_HPP_INCLUDED
