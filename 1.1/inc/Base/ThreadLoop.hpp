/*
 * Base/ThreadLoop.hpp
 *
 * Copyright 2010-2013 Navimatics Corporation. All rights reserved.
 */

#ifndef BASE_THREADLOOP_HPP_INCLUDED
#define BASE_THREADLOOP_HPP_INCLUDED

#include <Base/Defines.hpp>
#include <Base/Delegate.hpp>
#include <Base/Thread.hpp>
#include <Base/impl/ThreadLoop.hpp>

namespace Base {

using Base::impl::ThreadLoopInit;
using Base::impl::ThreadLoopRun;
using Base::impl::ThreadLoopStop;
using Base::impl::ThreadLoopInvoke;
using Base::impl::ThreadLoopCancelInvoke;
using Base::impl::ThreadLoopSetTimer;
using Base::impl::ThreadLoopCancelTimer;

}

#endif // BASE_THREADLOOP_HPP_INCLUDED
