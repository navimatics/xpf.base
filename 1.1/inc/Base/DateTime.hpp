/*
 * Base/DateTime.hpp
 *
 * Copyright 2010-2013 Navimatics Corporation. All rights reserved.
 */

#ifndef BASE_DATETIME_HPP_INCLUDED
#define BASE_DATETIME_HPP_INCLUDED

#include <Base/Defines.hpp>
#include <Base/Object.hpp>
#include <Base/impl/DateTime.hpp>

namespace Base {

using Base::impl::DateTime;

}

#endif // BASE_DATETIME_HPP_INCLUDED
