LOCAL_PATH      := $(call my-dir)/ProjectRoot/src

include $(CLEAR_VARS)

LOCAL_MODULE    := Base
LOCAL_SRC_FILES := $(patsubst %, Base/impl/%, \
    ApplicationInfo.cpp \
    Array.cpp \
    CArray.cpp \
    CString.cpp \
    CStringFnmatch.cpp \
    CStringPath.cpp \
    CStringUri.cpp \
    CacheMap.cpp \
    DateTime.cpp \
    Defines.cpp \
    DelegateMap.cpp \
    Environment.cpp \
    Error.cpp \
    ErrorFilteredLogger.cpp \
    FileStream.cpp \
    IndexMap.cpp \
    IndexSet.cpp \
    JsonParser.cpp \
    JsonReader.cpp \
    JsonWriter.cpp \
    Jvm.cpp \
    JvmPeerMapping.cpp \
    Log.cpp \
    Map.cpp \
    MemoryStream.cpp \
    Object.cpp \
    ObjectCodec.cpp \
    ObjectReader.cpp \
    ObjectUtilJson.cpp \
    ObjectUtilNative.cpp \
    ObjectWriter.cpp \
    OrderedMap.cpp \
    OrderedSet.cpp \
    RadixTree.cpp \
    Random.cpp \
    Set.cpp \
    Snprintf.cpp \
    StdioStream.cpp \
    Stream.cpp \
    Syn.cpp \
    Synchronization.cpp \
    SystemStream.cpp \
    TextParser.cpp \
    Thread.cpp \
    Tls.cpp \
    TypeInfo.cpp \
    UString.cpp \
    UStringConversions.cpp \
    Uuid.cpp \
    Value.cpp \
    ZipFileStream.cpp \
    ZlibStream.cpp \
    mblen.cpp \
    )
LOCAL_SRC_FILES += $(patsubst %, Base/jimpl/%, \
    DateTime.cpp \
    LibMain.cpp \
    NativeArray.cpp \
    NativeMap.cpp \
    NativeObject.cpp \
    NativeObjectUtil.cpp \
    NativeSet.cpp \
    NativeString.cpp \
    )
LOCAL_SRC_FILES += $(patsubst %, ../ext/zlib/contrib/minizip/%, \
    ioapi.c \
    unzip.c \
    zip.c \
    )
LOCAL_SRC_FILES += $(patsubst %, ../ext/trio/%, \
    trio.c \
    )
LOCAL_SRC_FILES += $(patsubst %, ../ext/TinyMT/tinymt/%, \
    tinymt32.c \
    tinymt64.c \
    )
LOCAL_C_INCLUDES:= \
    $(LOCAL_PATH)/../inc \
    $(LOCAL_PATH)/../ext/trio \
    $(LOCAL_PATH)/../ext/zlib/contrib/minizip \
    $(LOCAL_PATH)/../ext/TinyMT/tinymt
LOCAL_LDLIBS    := -lz -llog -ldl
LOCAL_CFLAGS    := -DBASE_CONFIG_INTERNAL -DTRIO_SNPRINTF_ONLY -DTRIO_PUBLIC="__attribute__ ((visibility(\"default\")))"

include $(BUILD_SHARED_LIBRARY)
