/*
 * Base/FileSystemMonitorTest.cpp
 *
 * Copyright 2010-2015 Navimatics Corporation. All rights reserved.
 */

#include <Base/FileSystemMonitor.hpp>
#include <Base/Atomic.hpp>
#include <Base/Error.hpp>
#include <Base/FileStream.hpp>
#include <Base/Thread.hpp>
#include <Base/ThreadLoop.hpp>
#include <gtest/gtest.h>

static Base::Thread *_currentThread;
static unsigned _pointA;
static void FileSystemMonitorTest_MonitorThreadEvent(Base::FileSystemMonitorEvent *events)
{
    ASSERT_NE(Base::Thread::currentThread(), _currentThread);
    Base::atomic_inc(_pointA);
}
static void FileSystemMonitorTest_OwnThreadEvent(Base::FileSystemMonitorEvent *events)
{
    ASSERT_EQ(Base::Thread::currentThread(), _currentThread);
    Base::atomic_inc(_pointA);
    Base::ThreadLoopStop();
}
static void FileSystemMonitorTest_Stop(Base::Object *obj)
{
    Base::ThreadLoopStop();
}
TEST(FileSystemMonitorTest, Error)
{
    mark_and_collect
    {
        Base::FileSystemMonitor *monitor =
            new Base::FileSystemMonitor(make_delegate(FileSystemMonitorTest_MonitorThreadEvent));
        ASSERT_TRUE(monitor->isStopped());
        bool res = monitor->start();
        ASSERT_FALSE(res);
        ASSERT_STREQ(Base::FileSystemMonitor::errorDomain(), Base::Error::lastError()->domain());
        ASSERT_TRUE(monitor->isStopped());
        monitor->release();
    }
}
TEST(FileSystemMonitorTest, MonitorThread)
{
    mark_and_collect
    {
        _currentThread = Base::Thread::currentThread();
        _pointA = 0;
        char root[L_tmpnam]; tmpnam(root);
        Base::FileStream *stream;
        stream = Base::FileStream::create(Base::cstr_path_concat(root, "x"), "wb/");
        stream->write("hello", 5);
        stream->close();
        stream->release();
        Base::FileSystemMonitor *monitor =
            new Base::FileSystemMonitor(make_delegate(FileSystemMonitorTest_MonitorThreadEvent));
        monitor->addPath(root);
        ASSERT_TRUE(monitor->isStopped());
        bool res = monitor->start();
        ASSERT_TRUE(res);
        ASSERT_EQ(0, Base::Error::lastError());
        ASSERT_FALSE(monitor->isStopped());
        stream = Base::FileStream::create(Base::cstr_path_concat(root, "y"), "wb/");
        stream->write("hello", 5);
        stream->close();
        stream->release();
        while (0 == _pointA)
            Base::Thread::sleep(100);
        monitor->stop();
        ASSERT_TRUE(monitor->isStopped());
        unsigned pointA = _pointA;
        monitor->release();
        remove(Base::cstr_path_concat(root, "x"));
        remove(Base::cstr_path_concat(root, "y"));
        rmdir(root);
        Base::Thread::sleep(300);
        ASSERT_EQ(pointA, _pointA);
    }
}
TEST(FileSystemMonitorTest, OwnThread)
{
    mark_and_collect
    {
        Base::ThreadLoopInit();
        _currentThread = Base::Thread::currentThread();
        _pointA = 0;
        char root[L_tmpnam]; tmpnam(root);
        Base::FileStream *stream;
        stream = Base::FileStream::create(Base::cstr_path_concat(root, "x"), "wb/");
        stream->write("hello", 5);
        stream->close();
        stream->release();
        Base::FileSystemMonitor *monitor =
            new Base::FileSystemMonitor(make_delegate(FileSystemMonitorTest_OwnThreadEvent),
            Base::Thread::currentThread());
        monitor->addPath(root);
        ASSERT_TRUE(monitor->isStopped());
        bool res = monitor->start();
        ASSERT_TRUE(res);
        ASSERT_EQ(0, Base::Error::lastError());
        ASSERT_FALSE(monitor->isStopped());
        stream = Base::FileStream::create(Base::cstr_path_concat(root, "y"), "wb/");
        stream->write("hello", 5);
        stream->close();
        stream->release();
        while (0 == _pointA)
            Base::ThreadLoopRun();
        monitor->stop();
        ASSERT_TRUE(monitor->isStopped());
        unsigned pointA = _pointA;
        monitor->release();
        remove(Base::cstr_path_concat(root, "x"));
        remove(Base::cstr_path_concat(root, "y"));
        rmdir(root);
        void *timer = Base::ThreadLoopSetTimer(300, FileSystemMonitorTest_Stop, 0);
        Base::ThreadLoopRun();
        Base::ThreadLoopCancelTimer(timer);
        ASSERT_EQ(pointA, _pointA);
    }
}
