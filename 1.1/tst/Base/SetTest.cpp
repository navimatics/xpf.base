/*
 * Base/SetTest.cpp
 *
 * Copyright 2010-2013 Navimatics Corporation. All rights reserved.
 */

#include <Base/Set.hpp>
#include <Base/CArray.hpp>
#include <gtest/gtest.h>
#include "TestObject.hpp"

TEST(SetTest, AddRemoveAll)
{
    ASSERT_EQ(0, TestObject::count);
    Base::Object::Mark();
    Base::Set *set = new (Base::collect) Base::Set;
    for (size_t i = 0; i < 100; i++)
    {
        TestObject *o = new (Base::collect) TestObject;
        set->addObject(o);
    }
    ASSERT_EQ((size_t)100, set->count());
    set->removeAllObjects();
    ASSERT_EQ((size_t)0, set->count());
    for (size_t i = 0; i < 100; i++)
    {
        TestObject *o = new (Base::collect) TestObject;
        set->addObject(o);
    }
    ASSERT_EQ((size_t)100, set->count());
    Base::Object::Collect();
    ASSERT_EQ(0, TestObject::count);
}
TEST(SetTest, ContainsAny)
{
    ASSERT_EQ(0, TestObject::count);
    Base::Object::Mark();
    Base::Set *set = new (Base::collect) Base::Set;
    TestObject *o = new (Base::collect) TestObject;
    set->addObject(o);
    ASSERT_EQ((size_t)1, set->count());
    ASSERT_TRUE(set->containsObject(o));
    ASSERT_EQ(o, set->anyObject());
    set->removeAllObjects();
    ASSERT_EQ((size_t)0, set->count());
    ASSERT_FALSE(set->containsObject(o));
    ASSERT_EQ(0, set->anyObject());
    TestObject *o2 = new (Base::collect) TestObject;
    set->addObject(o2);
    ASSERT_TRUE(set->containsObject(o2));
    ASSERT_FALSE(set->containsObject(o));
    Base::Object::Collect();
    ASSERT_EQ(0, TestObject::count);
}
TEST(SetTest, AddRemove)
{
    ASSERT_EQ(0, TestObject::count);
    Base::Object::Mark();
    Base::Set *set = new (Base::collect) Base::Set;
    for (size_t i = 0; i < 100; i++)
    {
        TestObject *o = new (Base::collect) TestObject;
        set->addObject(o);
    }
    ASSERT_EQ((size_t)100, set->count());
    TestObject *o = new (Base::collect) TestObject;
    set->addObject(o);
    ASSERT_EQ((size_t)101, set->count());
    ASSERT_TRUE(set->containsObject(o));
    set->removeObject(o);
    ASSERT_EQ((size_t)100, set->count());
    ASSERT_FALSE(set->containsObject(o));
    Base::Object::Collect();
    ASSERT_EQ(0, TestObject::count);
}
TEST(SetTest, Contents)
{
    ASSERT_EQ(0, TestObject::count);
    Base::Object::Mark();
    Base::Set *set = new (Base::collect) Base::Set;
    TestObject *to[100];
    for (size_t i = 0; i < 100; i++)
    {
        to[i] = new (Base::collect) TestObject;
        set->addObject(to[i]);
    }
    ASSERT_EQ((size_t)100, set->count());
    Base::Object **objs = set->objects();
    ASSERT_EQ((size_t)100, carr_length(objs));
    for (size_t i = 0; i < 100; i++)
    {
        bool found = false;
        for (size_t j = 0; j < 100; j++)
            if (to[i] == objs[j])
            {
                found = true;
                break;
            }
        ASSERT_TRUE(found);
    }
    Base::Object::Collect();
    ASSERT_EQ(0, TestObject::count);
}

/*
 * GenericSet<T>
 */
TEST(GenericSetTest, AddRemoveAll)
{
    ASSERT_EQ(0, TestObject::count);
    Base::Object::Mark();
    Base::GenericSet<TestObject> *set = new (Base::collect) Base::GenericSet<TestObject>;
    for (size_t i = 0; i < 100; i++)
    {
        TestObject *o = new (Base::collect) TestObject;
        set->addObject(o);
    }
    ASSERT_EQ((size_t)100, set->count());
    set->removeAllObjects();
    ASSERT_EQ((size_t)0, set->count());
    for (size_t i = 0; i < 100; i++)
    {
        TestObject *o = new (Base::collect) TestObject;
        set->addObject(o);
    }
    ASSERT_EQ((size_t)100, set->count());
    Base::Object::Collect();
    ASSERT_EQ(0, TestObject::count);
}
TEST(GenericSetTest, ContainsAny)
{
    ASSERT_EQ(0, TestObject::count);
    Base::Object::Mark();
    Base::GenericSet<TestObject> *set = new (Base::collect) Base::GenericSet<TestObject>;
    TestObject *o = new (Base::collect) TestObject;
    set->addObject(o);
    ASSERT_EQ((size_t)1, set->count());
    ASSERT_TRUE(set->containsObject(o));
    ASSERT_EQ(o, set->anyObject());
    set->removeAllObjects();
    ASSERT_EQ((size_t)0, set->count());
    ASSERT_FALSE(set->containsObject(o));
    ASSERT_EQ(0, set->anyObject());
    TestObject *o2 = new (Base::collect) TestObject;
    set->addObject(o2);
    ASSERT_TRUE(set->containsObject(o2));
    ASSERT_FALSE(set->containsObject(o));
    Base::Object::Collect();
    ASSERT_EQ(0, TestObject::count);
}
TEST(GenericSetTest, AddRemove)
{
    ASSERT_EQ(0, TestObject::count);
    Base::Object::Mark();
    Base::GenericSet<TestObject> *set = new (Base::collect) Base::GenericSet<TestObject>;
    for (size_t i = 0; i < 100; i++)
    {
        TestObject *o = new (Base::collect) TestObject;
        set->addObject(o);
    }
    ASSERT_EQ((size_t)100, set->count());
    TestObject *o = new (Base::collect) TestObject;
    set->addObject(o);
    ASSERT_EQ((size_t)101, set->count());
    ASSERT_TRUE(set->containsObject(o));
    set->removeObject(o);
    ASSERT_EQ((size_t)100, set->count());
    ASSERT_FALSE(set->containsObject(o));
    Base::Object::Collect();
    ASSERT_EQ(0, TestObject::count);
}
TEST(GenericSetTest, Contents)
{
    ASSERT_EQ(0, TestObject::count);
    Base::Object::Mark();
    Base::GenericSet<TestObject> *set = new (Base::collect) Base::GenericSet<TestObject>;
    TestObject *to[100];
    for (size_t i = 0; i < 100; i++)
    {
        to[i] = new (Base::collect) TestObject;
        set->addObject(to[i]);
    }
    ASSERT_EQ((size_t)100, set->count());
    TestObject **objs = set->objects();
    ASSERT_EQ((size_t)100, carr_length(objs));
    for (size_t i = 0; i < 100; i++)
    {
        bool found = false;
        for (size_t j = 0; j < 100; j++)
            if (to[i] == objs[j])
            {
                found = true;
                break;
            }
        ASSERT_TRUE(found);
    }
    Base::Object::Collect();
    ASSERT_EQ(0, TestObject::count);
}
