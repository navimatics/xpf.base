/*
 * Base/CStringComparerTest.cpp
 *
 * Copyright 2010-2013 Navimatics Corporation. All rights reserved.
 */

#include <Base/CString.hpp>
#include <Base/Map.hpp>
#include <gtest/gtest.h>

TEST(CStringComparerTest, CString_cmp)
{
    mark_and_collect
    {
        static Base::CString *s1 = Base::CStringConstant("abc");
        static Base::CString *s2 = Base::CStringConstant("ABC");
        static Base::CString *s3 = Base::CStringConstant("abC");
        static Base::CString *s4 = Base::CStringConstant("defghi");
        static Base::CString *s5 = Base::CStringConstant("DEFGHI");
        static Base::CString *s6 = Base::CStringConstant("Defghi");
        Base::Map *map = new Base::Map;
        map->setObject(s1, s1);
        map->setObject(s2, s2);
        map->setObject(s3, s3);
        map->setObject(s4, s4);
        map->setObject(s5, s5);
        map->setObject(s6, s6);
        ASSERT_EQ(6, map->count());
        ASSERT_STREQ("abc", ((Base::CString *)map->object(s1))->chars());
        ASSERT_STREQ("ABC", ((Base::CString *)map->object(s2))->chars());
        ASSERT_STREQ("abC", ((Base::CString *)map->object(s3))->chars());
        ASSERT_STREQ("abc", ((Base::CString *)map->object(BASE_ALLOCA_OBJECT(Base::TransientCString, "abc")))->chars());
        ASSERT_STREQ("ABC", ((Base::CString *)map->object(BASE_ALLOCA_OBJECT(Base::TransientCString, "ABC")))->chars());
        ASSERT_STREQ("abC", ((Base::CString *)map->object(BASE_ALLOCA_OBJECT(Base::TransientCString, "abC")))->chars());
        ASSERT_STREQ("defghi", ((Base::CString *)map->object(s4))->chars());
        ASSERT_STREQ("DEFGHI", ((Base::CString *)map->object(s5))->chars());
        ASSERT_STREQ("Defghi", ((Base::CString *)map->object(s6))->chars());
        ASSERT_STREQ("defghi", ((Base::CString *)map->object(BASE_ALLOCA_OBJECT(Base::TransientCString, "defghi")))->chars());
        ASSERT_STREQ("DEFGHI", ((Base::CString *)map->object(BASE_ALLOCA_OBJECT(Base::TransientCString, "DEFGHI")))->chars());
        ASSERT_STREQ("Defghi", ((Base::CString *)map->object(BASE_ALLOCA_OBJECT(Base::TransientCString, "Defghi")))->chars());
        map->release();
    }
}
TEST(CStringComparerTest, CString_comparer)
{
    mark_and_collect
    {
        static Base::CString *s1 = Base::CStringConstant("abc");
        static Base::CString *s2 = Base::CStringConstant("ABC");
        static Base::CString *s3 = Base::CStringConstant("abC");
        static Base::CString *s4 = Base::CStringConstant("defghi");
        static Base::CString *s5 = Base::CStringConstant("DEFGHI");
        static Base::CString *s6 = Base::CStringConstant("Defghi");
        Base::Map *map = new Base::Map(Base::CString::comparer());
        map->setObject(s1, s1);
        map->setObject(s2, s2);
        map->setObject(s3, s3);
        map->setObject(s4, s4);
        map->setObject(s5, s5);
        map->setObject(s6, s6);
        ASSERT_EQ(6, map->count());
        ASSERT_STREQ("abc", ((Base::CString *)map->object(s1))->chars());
        ASSERT_STREQ("ABC", ((Base::CString *)map->object(s2))->chars());
        ASSERT_STREQ("abC", ((Base::CString *)map->object(s3))->chars());
        ASSERT_STREQ("abc", ((Base::CString *)map->object(BASE_ALLOCA_OBJECT(Base::TransientCString, "abc")))->chars());
        ASSERT_STREQ("ABC", ((Base::CString *)map->object(BASE_ALLOCA_OBJECT(Base::TransientCString, "ABC")))->chars());
        ASSERT_STREQ("abC", ((Base::CString *)map->object(BASE_ALLOCA_OBJECT(Base::TransientCString, "abC")))->chars());
        ASSERT_STREQ("defghi", ((Base::CString *)map->object(s4))->chars());
        ASSERT_STREQ("DEFGHI", ((Base::CString *)map->object(s5))->chars());
        ASSERT_STREQ("Defghi", ((Base::CString *)map->object(s6))->chars());
        ASSERT_STREQ("defghi", ((Base::CString *)map->object(BASE_ALLOCA_OBJECT(Base::TransientCString, "defghi")))->chars());
        ASSERT_STREQ("DEFGHI", ((Base::CString *)map->object(BASE_ALLOCA_OBJECT(Base::TransientCString, "DEFGHI")))->chars());
        ASSERT_STREQ("Defghi", ((Base::CString *)map->object(BASE_ALLOCA_OBJECT(Base::TransientCString, "Defghi")))->chars());
        map->release();
    }
}
TEST(CStringComparerTest, CString_caseInsensitiveComparer)
{
    mark_and_collect
    {
        static Base::CString *s1 = Base::CStringConstant("abc");
        static Base::CString *s2 = Base::CStringConstant("ABC");
        static Base::CString *s3 = Base::CStringConstant("abC");
        static Base::CString *s4 = Base::CStringConstant("defghi");
        static Base::CString *s5 = Base::CStringConstant("DEFGHI");
        static Base::CString *s6 = Base::CStringConstant("Defghi");
        Base::Map *map = new Base::Map(Base::CString::caseInsensitiveComparer());
        map->setObject(s1, s1);
        map->setObject(s2, s2);
        map->setObject(s3, s3);
        map->setObject(s4, s4);
        map->setObject(s5, s5);
        map->setObject(s6, s6);
        ASSERT_EQ(2, map->count());
        ASSERT_STREQ("abC", ((Base::CString *)map->object(s1))->chars());
        ASSERT_STREQ("abC", ((Base::CString *)map->object(s2))->chars());
        ASSERT_STREQ("abC", ((Base::CString *)map->object(s3))->chars());
        ASSERT_STREQ("abC", ((Base::CString *)map->object(BASE_ALLOCA_OBJECT(Base::TransientCString, "abc")))->chars());
        ASSERT_STREQ("abC", ((Base::CString *)map->object(BASE_ALLOCA_OBJECT(Base::TransientCString, "ABC")))->chars());
        ASSERT_STREQ("abC", ((Base::CString *)map->object(BASE_ALLOCA_OBJECT(Base::TransientCString, "abC")))->chars());
        ASSERT_STREQ("Defghi", ((Base::CString *)map->object(s4))->chars());
        ASSERT_STREQ("Defghi", ((Base::CString *)map->object(s5))->chars());
        ASSERT_STREQ("Defghi", ((Base::CString *)map->object(s6))->chars());
        ASSERT_STREQ("Defghi", ((Base::CString *)map->object(BASE_ALLOCA_OBJECT(Base::TransientCString, "defghi")))->chars());
        ASSERT_STREQ("Defghi", ((Base::CString *)map->object(BASE_ALLOCA_OBJECT(Base::TransientCString, "DEFGHI")))->chars());
        ASSERT_STREQ("Defghi", ((Base::CString *)map->object(BASE_ALLOCA_OBJECT(Base::TransientCString, "Defghi")))->chars());
        map->release();
    }
}
