/*
 * Base/ThreadTest.cpp
 *
 * Copyright 2010-2013 Navimatics Corporation. All rights reserved.
 */

#include <Base/Thread.hpp>
#include <Base/impl/Atomic.hpp>
#include <gtest/gtest.h>

class TestThread : public Base::Thread
{
protected:
    void run();
public:
    volatile int value;
};
void TestThread::run()
{
    value = 42;
}
void TestThread_entry(void *arg)
{
    *(volatile int *)arg = 42;
}
void TestThread_entry2(void *arg)
{
    for (;;)
    {
        int v = *(volatile int *)arg;
        if (42 == v)
            break;
    }
}

class TestThread2 : public Base::Thread
{
protected:
    void run();
public:
    volatile int value;
};
void TestThread2::run()
{
    while (!isQuitting())
        ;
    value = 42;
}
void TestThread2_entry(void *arg)
{
    while (!Base::Thread::currentThread()->isQuitting())
        ;
    *(volatile int *)arg = 42;
}

class MultiTestThread : public Base::Thread
{
public:
    MultiTestThread(int depth, int count);
protected:
    void run();
public:
    static int totalCount;
    int _depth, _count;
};
int MultiTestThread::totalCount;
MultiTestThread::MultiTestThread(int depth, int count)
{
    _depth = depth;
    _count = count;
}
void MultiTestThread::run()
{
    if (_depth > 0)
    {
        Base::Thread **threads = (Base::Thread **)alloca(_count * sizeof(Base::Thread *));
        for (int i = 0; _count > i; i++)
        {
            threads[i] = new MultiTestThread(_depth - 1, _count);
            threads[i]->start();
        }
        for (int i = 0; _count > i; i++)
        {
            threads[i]->wait();
            threads[i]->release();
        }
    }
    Base::impl::atomic_inc(totalCount);
}
static int MultiTestThread_totalCount;
void MultiTestThread_entry(void *arg)
{
    int depth = ((int *)arg)[0];
    int count = ((int *)arg)[1];
    if (depth > 0)
    {
        Base::Thread **threads = (Base::Thread **)alloca(count * sizeof(Base::Thread *));
        for (int i = 0; count > i; i++)
        {
            int args[2] = { depth - 1, count };
            threads[i] = new Base::Thread(MultiTestThread_entry, args);
            threads[i]->start();
        }
        for (int i = 0; count > i; i++)
        {
            threads[i]->wait();
            threads[i]->release();
        }
    }
    Base::impl::atomic_inc(MultiTestThread_totalCount);
}

static volatile int ThreadStartRelease_value;
TEST(ThreadTest, ThreadStartRelease)
{
    {
        ThreadStartRelease_value = 0;
        Base::Thread *thread = new Base::Thread(TestThread_entry, (void *)&ThreadStartRelease_value);
        thread->start();
        for (;;)
        {
            int v = ThreadStartRelease_value;
            if (42 == v)
                break;
        }
        thread->release();
    }
    {
        ThreadStartRelease_value = 0;
        Base::Thread *thread = new Base::Thread(TestThread_entry2, (void *)&ThreadStartRelease_value);
        thread->start();
        thread->release();
        ThreadStartRelease_value = 42;
    }
}
TEST(ThreadTest, ThreadStartWait)
{
    {
        TestThread *thread = new TestThread;
        thread->start();
        thread->wait();
        ASSERT_EQ(42, thread->value);
        thread->release();
    }
    {
        volatile int value = 0;
        Base::Thread *thread = new Base::Thread(TestThread_entry, (void *)&value);
        thread->start();
        thread->wait();
        ASSERT_EQ(42, value);
        thread->release();
    }
}
TEST(ThreadTest, ThreadStartDetach)
{
    {
        TestThread *thread = new TestThread;
        thread->start();
        thread->detach();
        for (;;)
        {
            int v = thread->value;
            if (42 == v)
                break;
        }
        thread->release();
    }
    {
        volatile int value = 0;
        Base::Thread *thread = new Base::Thread(TestThread_entry, (void *)&value);
        thread->start();
        thread->detach();
        for (;;)
        {
            int v = value;
            if (42 == v)
                break;
        }
        thread->release();
    }
}
TEST(ThreadTest, ThreadStartQuit)
{
    {
        TestThread2 *thread = new TestThread2;
        ASSERT_EQ(0, thread->value);
        thread->start();
        thread->quit();
        thread->wait();
        ASSERT_EQ(42, thread->value);
        thread->release();
    }
    {
        volatile int value = 0;
        Base::Thread *thread = new Base::Thread(TestThread2_entry, (void *)&value);
        ASSERT_EQ(0, value);
        thread->start();
        thread->quit();
        thread->wait();
        ASSERT_EQ(42, value);
        thread->release();
    }
}
TEST(ThreadTest, MultipleThreads)
{
    {
        MultiTestThread::totalCount = 0;
        MultiTestThread *thread = new MultiTestThread(2, 10);
        thread->start();
        thread->wait();
        ASSERT_EQ(10 * 10 + 10 + 1, MultiTestThread::totalCount);
        thread->release();
    }
    {
        MultiTestThread_totalCount = 0;
        int args[2] = { 2, 10 };
        Base::Thread *thread = new Base::Thread(MultiTestThread_entry, args);
        thread->start();
        thread->wait();
        ASSERT_EQ(10 * 10 + 10 + 1, MultiTestThread_totalCount);
        thread->release();
    }
}
