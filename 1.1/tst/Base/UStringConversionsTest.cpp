/*
 * Base/UStringConversionsTest.cpp
 *
 * Copyright 2010-2013 Navimatics Corporation. All rights reserved.
 */

#include <Base/UString.hpp>
#include <Base/CString.hpp>
#include <Base/FileStream.hpp>
#include <Base/MemoryStream.hpp>
#include <gtest/gtest.h>

TEST(UStringConversionsTest, roundtrip)
{
    mark_and_collect
    {
        Base::FileStream *istream = Base::FileStream::create("UTF-8-demo.txt", "rb");
        ASSERT_NE((void *)0, istream);
        Base::MemoryStream *ostream = new (Base::collect) Base::MemoryStream;
        char buf[4096];
        ssize_t bytes;
        while (0 < (bytes = istream->read(buf, sizeof buf)))
            ostream->write(buf, bytes);
        istream->release();
        const Base::unichar_t *ustr = Base::ustr_from_cstr(ostream->buffer(), ostream->size());
        const char *cstr = Base::cstr_from_ustr(ustr, Base::ustr_length(ustr));
        ASSERT_EQ(ostream->size(), Base::cstr_length(cstr));
        ASSERT_TRUE(0 == memcmp(ostream->buffer(), cstr, ostream->size()));
    }
}
