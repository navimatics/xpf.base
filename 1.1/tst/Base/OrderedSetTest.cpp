/*
 * Base/OrderedSetTest.cpp
 *
 * Copyright 2010-2013 Navimatics Corporation. All rights reserved.
 */

#include <Base/OrderedSet.hpp>
#include <Base/CArray.hpp>
#include <Base/Value.hpp>
#include <gtest/gtest.h>
#include "TestObject.hpp"

TEST(OrderedSetTest, AddRemoveAll)
{
    ASSERT_EQ(0, TestObject::count);
    Base::Object::Mark();
    Base::OrderedSet *set = new (Base::collect) Base::OrderedSet;
    for (size_t i = 0; i < 100; i++)
    {
        TestObject *o = new (Base::collect) TestObject;
        set->addObject(o);
    }
    ASSERT_EQ((size_t)100, set->count());
    set->removeAllObjects();
    ASSERT_EQ((size_t)0, set->count());
    for (size_t i = 0; i < 100; i++)
    {
        TestObject *o = new (Base::collect) TestObject;
        set->addObject(o);
    }
    ASSERT_EQ((size_t)100, set->count());
    Base::Object::Collect();
    ASSERT_EQ(0, TestObject::count);
}
TEST(OrderedSetTest, ContainsAny)
{
    ASSERT_EQ(0, TestObject::count);
    Base::Object::Mark();
    Base::OrderedSet *set = new (Base::collect) Base::OrderedSet;
    TestObject *o = new (Base::collect) TestObject;
    set->addObject(o);
    ASSERT_EQ((size_t)1, set->count());
    ASSERT_TRUE(set->containsObject(o));
    ASSERT_EQ(o, set->anyObject());
    set->removeAllObjects();
    ASSERT_EQ((size_t)0, set->count());
    ASSERT_FALSE(set->containsObject(o));
    ASSERT_EQ(0, set->anyObject());
    TestObject *o2 = new (Base::collect) TestObject;
    set->addObject(o2);
    ASSERT_TRUE(set->containsObject(o2));
    ASSERT_FALSE(set->containsObject(o));
    Base::Object::Collect();
    ASSERT_EQ(0, TestObject::count);
}
TEST(OrderedSetTest, AddRemove)
{
    ASSERT_EQ(0, TestObject::count);
    Base::Object::Mark();
    Base::OrderedSet *set = new (Base::collect) Base::OrderedSet;
    for (size_t i = 0; i < 100; i++)
    {
        TestObject *o = new (Base::collect) TestObject;
        set->addObject(o);
    }
    ASSERT_EQ((size_t)100, set->count());
    TestObject *o = new (Base::collect) TestObject;
    set->addObject(o);
    ASSERT_EQ((size_t)101, set->count());
    ASSERT_TRUE(set->containsObject(o));
    set->removeObject(o);
    ASSERT_EQ((size_t)100, set->count());
    ASSERT_FALSE(set->containsObject(o));
    Base::Object::Collect();
    ASSERT_EQ(0, TestObject::count);
}
TEST(OrderedSetTest, Contents)
{
    ASSERT_EQ(0, TestObject::count);
    Base::Object::Mark();
    Base::OrderedSet *set = new (Base::collect) Base::OrderedSet;
    TestObject *to[100];
    for (size_t i = 0; i < 100; i++)
    {
        to[i] = new (Base::collect) TestObject;
        set->addObject(to[i]);
    }
    ASSERT_EQ((size_t)100, set->count());
    Base::Object **objs = set->objects();
    ASSERT_EQ((size_t)100, carr_length(objs));
    for (size_t i = 0; i < 100; i++)
    {
        bool found = false;
        for (size_t j = 0; j < 100; j++)
            if (to[i] == objs[j])
            {
                found = true;
                break;
            }
        ASSERT_TRUE(found);
    }
    Base::Object::Collect();
    ASSERT_EQ(0, TestObject::count);
}
TEST(OrderedSetTest, OrderedContents)
{
    Base::Object::Mark();
    Base::OrderedSet *set = new (Base::collect) Base::OrderedSet;
    for (int i = 0; i < 100; i++)
    {
        Base::Value *v = Base::Value::create(i);
        v->autorelease();
        set->addObject(v);
    }
    ASSERT_EQ((size_t)100, set->count());
    Base::Object **objs = set->objects();
    ASSERT_EQ((size_t)100, carr_length(objs));
    for (ssize_t i = 0; i < 100; i++)
        ASSERT_TRUE(i == ((Base::Value *)objs[i])->integerValue());
    Base::Object::Collect();
}

/*
 * GenericOrderedSet<T>
 */
TEST(GenericOrderedSetTest, AddRemoveAll)
{
    ASSERT_EQ(0, TestObject::count);
    Base::Object::Mark();
    Base::GenericOrderedSet<TestObject> *set = new (Base::collect) Base::GenericOrderedSet<TestObject>;
    for (size_t i = 0; i < 100; i++)
    {
        TestObject *o = new (Base::collect) TestObject;
        set->addObject(o);
    }
    ASSERT_EQ((size_t)100, set->count());
    set->removeAllObjects();
    ASSERT_EQ((size_t)0, set->count());
    for (size_t i = 0; i < 100; i++)
    {
        TestObject *o = new (Base::collect) TestObject;
        set->addObject(o);
    }
    ASSERT_EQ((size_t)100, set->count());
    Base::Object::Collect();
    ASSERT_EQ(0, TestObject::count);
}
TEST(GenericOrderedSetTest, ContainsAny)
{
    ASSERT_EQ(0, TestObject::count);
    Base::Object::Mark();
    Base::GenericOrderedSet<TestObject> *set = new (Base::collect) Base::GenericOrderedSet<TestObject>;
    TestObject *o = new (Base::collect) TestObject;
    set->addObject(o);
    ASSERT_EQ((size_t)1, set->count());
    ASSERT_TRUE(set->containsObject(o));
    ASSERT_EQ(o, set->anyObject());
    set->removeAllObjects();
    ASSERT_EQ((size_t)0, set->count());
    ASSERT_FALSE(set->containsObject(o));
    ASSERT_EQ(0, set->anyObject());
    TestObject *o2 = new (Base::collect) TestObject;
    set->addObject(o2);
    ASSERT_TRUE(set->containsObject(o2));
    ASSERT_FALSE(set->containsObject(o));
    Base::Object::Collect();
    ASSERT_EQ(0, TestObject::count);
}
TEST(GenericOrderedSetTest, AddRemove)
{
    ASSERT_EQ(0, TestObject::count);
    Base::Object::Mark();
    Base::GenericOrderedSet<TestObject> *set = new (Base::collect) Base::GenericOrderedSet<TestObject>;
    for (size_t i = 0; i < 100; i++)
    {
        TestObject *o = new (Base::collect) TestObject;
        set->addObject(o);
    }
    ASSERT_EQ((size_t)100, set->count());
    TestObject *o = new (Base::collect) TestObject;
    set->addObject(o);
    ASSERT_EQ((size_t)101, set->count());
    ASSERT_TRUE(set->containsObject(o));
    set->removeObject(o);
    ASSERT_EQ((size_t)100, set->count());
    ASSERT_FALSE(set->containsObject(o));
    Base::Object::Collect();
    ASSERT_EQ(0, TestObject::count);
}
TEST(GenericOrderedSetTest, Contents)
{
    ASSERT_EQ(0, TestObject::count);
    Base::Object::Mark();
    Base::GenericOrderedSet<TestObject> *set = new (Base::collect) Base::GenericOrderedSet<TestObject>;
    TestObject *to[100];
    for (size_t i = 0; i < 100; i++)
    {
        to[i] = new (Base::collect) TestObject;
        set->addObject(to[i]);
    }
    ASSERT_EQ((size_t)100, set->count());
    TestObject **objs = set->objects();
    ASSERT_EQ((size_t)100, carr_length(objs));
    for (size_t i = 0; i < 100; i++)
    {
        bool found = false;
        for (size_t j = 0; j < 100; j++)
            if (to[i] == objs[j])
            {
                found = true;
                break;
            }
        ASSERT_TRUE(found);
    }
    Base::Object::Collect();
    ASSERT_EQ(0, TestObject::count);
}
TEST(GenericOrderedSetTest, OrderedContents)
{
    Base::Object::Mark();
    Base::GenericOrderedSet<Base::Value> *set = new (Base::collect) Base::GenericOrderedSet<Base::Value>;
    for (int i = 0; i < 100; i++)
    {
        Base::Value *v = Base::Value::create(i);
        v->autorelease();
        set->addObject(v);
    }
    ASSERT_EQ((size_t)100, set->count());
    Base::Value **objs = set->objects();
    ASSERT_EQ((size_t)100, carr_length(objs));
    for (ssize_t i = 0; i < 100; i++)
        ASSERT_TRUE(i == objs[i]->integerValue());
    Base::Object::Collect();
}
