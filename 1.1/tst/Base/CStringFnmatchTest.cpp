/*
 * Base/CStringFnmatchTest.cpp
 *
 * Copyright 2010-2013 Navimatics Corporation. All rights reserved.
 */

#include <Base/CString.hpp>
#include <gtest/gtest.h>

#if defined(BASE_CONFIG_WINDOWS) || defined(BASE_CONFIG_POSIX)
TEST(CStringFnmatchTest, TestCurlyBraces)
{
    ASSERT_TRUE(Base::cstr_fnmatch("aaa*", "aaa"));
    ASSERT_TRUE(Base::cstr_fnmatch("aaa*", "aaab"));
    ASSERT_FALSE(Base::cstr_fnmatch("aaa*", "aab"));

#if defined(BASE_CONFIG_WINDOWS)
    ASSERT_TRUE(Base::cstr_fnmatch("aa/a", "aa\\a", Base::FnmPathname));
#elif defined(BASE_CONFIG_POSIX)
    ASSERT_TRUE(Base::cstr_fnmatch("aa/a", "aa/a", Base::FnmPathname));
#endif

    ASSERT_FALSE(Base::cstr_fnmatch("aaa{}", "aaa"));
    ASSERT_TRUE(Base::cstr_fnmatch("aaa", "aaa", Base::FnmCurlyBraces));
    ASSERT_TRUE(Base::cstr_fnmatch("aaa{}", "aaa", Base::FnmCurlyBraces));
    ASSERT_FALSE(Base::cstr_fnmatch("aaa{}", "aab", Base::FnmCurlyBraces));
    ASSERT_TRUE(Base::cstr_fnmatch("{}aaa", "aaa", Base::FnmCurlyBraces));
    ASSERT_FALSE(Base::cstr_fnmatch("{}aaa", "aab", Base::FnmCurlyBraces));
    ASSERT_TRUE(Base::cstr_fnmatch("aa{}a", "aaa", Base::FnmCurlyBraces));
    ASSERT_FALSE(Base::cstr_fnmatch("aa{}a", "aab", Base::FnmCurlyBraces));
    ASSERT_TRUE(Base::cstr_fnmatch("a{}a{}a", "aaa", Base::FnmCurlyBraces));
    ASSERT_FALSE(Base::cstr_fnmatch("a{}a{}a", "aab", Base::FnmCurlyBraces));

    ASSERT_TRUE(Base::cstr_fnmatch("aa\\a", "aaa", Base::FnmCurlyBraces));
    ASSERT_FALSE(Base::cstr_fnmatch("aa\\a", "aab", Base::FnmCurlyBraces));
    ASSERT_TRUE(Base::cstr_fnmatch("aa\\a{}", "aaa", Base::FnmCurlyBraces));
    ASSERT_FALSE(Base::cstr_fnmatch("aa\\a{}", "aab", Base::FnmCurlyBraces));
    ASSERT_TRUE(Base::cstr_fnmatch("aa\\a{}", "aa\\a", Base::FnmCurlyBraces | Base::FnmNoEscape));
    ASSERT_FALSE(Base::cstr_fnmatch("aa\\a\\{\\}", "aaa", Base::FnmCurlyBraces));

    ASSERT_TRUE(Base::cstr_fnmatch("aa\\{a", "aa{a", Base::FnmCurlyBraces));
    ASSERT_TRUE(Base::cstr_fnmatch("aa{a", "aaa", Base::FnmCurlyBraces));

    ASSERT_TRUE(Base::cstr_fnmatch("a{a}a", "aaa", Base::FnmCurlyBraces));
    ASSERT_FALSE(Base::cstr_fnmatch("a{a}a", "aab", Base::FnmCurlyBraces));
    ASSERT_TRUE(Base::cstr_fnmatch("a{b,a}a", "aaa", Base::FnmCurlyBraces));
    ASSERT_FALSE(Base::cstr_fnmatch("a{b,a}a", "aab", Base::FnmCurlyBraces));
    ASSERT_TRUE(Base::cstr_fnmatch("a{c,b,a}a", "aaa", Base::FnmCurlyBraces));
    ASSERT_FALSE(Base::cstr_fnmatch("a{c,b,a}a", "aab", Base::FnmCurlyBraces));

    ASSERT_TRUE(Base::cstr_fnmatch("a{,b,c}a", "aa", Base::FnmCurlyBraces));
    ASSERT_FALSE(Base::cstr_fnmatch("a{,b,c}a", "ab", Base::FnmCurlyBraces));
    ASSERT_TRUE(Base::cstr_fnmatch("a{a,,c}a", "aa", Base::FnmCurlyBraces));
    ASSERT_FALSE(Base::cstr_fnmatch("a{a,,c}a", "ab", Base::FnmCurlyBraces));
    ASSERT_TRUE(Base::cstr_fnmatch("a{a,b,}a", "aa", Base::FnmCurlyBraces));
    ASSERT_FALSE(Base::cstr_fnmatch("a{a,b,}a", "ab", Base::FnmCurlyBraces));

    ASSERT_TRUE(Base::cstr_fnmatch("a{b,a}a{c,d}a", "abaca", Base::FnmCurlyBraces));
    ASSERT_TRUE(Base::cstr_fnmatch("a{b,a}a{c,d}a", "aaaca", Base::FnmCurlyBraces));
    ASSERT_TRUE(Base::cstr_fnmatch("a{b,a}a{c,d}a", "abada", Base::FnmCurlyBraces));
    ASSERT_TRUE(Base::cstr_fnmatch("a{b,a}a{c,d}a", "aaada", Base::FnmCurlyBraces));
    ASSERT_FALSE(Base::cstr_fnmatch("a{b,a}a{c,d}a", "aaaaa", Base::FnmCurlyBraces));
    ASSERT_FALSE(Base::cstr_fnmatch("a{b,a}a{c,d}a", "acaba", Base::FnmCurlyBraces));
    ASSERT_FALSE(Base::cstr_fnmatch("a{b,a}a{c,d}a", "adaaa", Base::FnmCurlyBraces));

    ASSERT_TRUE(Base::cstr_fnmatch("a{b{0,1},d{2,3}}a", "ab0a", Base::FnmCurlyBraces));
    ASSERT_TRUE(Base::cstr_fnmatch("a{b{0,1},d{2,3}}a", "ab1a", Base::FnmCurlyBraces));
    ASSERT_TRUE(Base::cstr_fnmatch("a{b{0,1},d{2,3}}a", "ad2a", Base::FnmCurlyBraces));
    ASSERT_TRUE(Base::cstr_fnmatch("a{b{0,1},d{2,3}}a", "ad3a", Base::FnmCurlyBraces));

    ASSERT_TRUE(Base::cstr_fnmatch("a{b{0,1},d{2,3}}a{f,g}", "ab0af", Base::FnmCurlyBraces));
    ASSERT_TRUE(Base::cstr_fnmatch("a{b{0,1},d{2,3}}a{f,g}", "ab0ag", Base::FnmCurlyBraces));

    ASSERT_TRUE(Base::cstr_fnmatch("{abc,def}", "abc", Base::FnmCurlyBraces));
    ASSERT_TRUE(Base::cstr_fnmatch("{abc,def}", "def", Base::FnmCurlyBraces));
}
#endif
