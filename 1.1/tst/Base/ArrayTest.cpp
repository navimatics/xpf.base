/*
 * Base/ArrayTest.cpp
 *
 * Copyright 2010-2013 Navimatics Corporation. All rights reserved.
 */

#include <Base/Array.hpp>
#include <Base/Value.hpp>
#include <gtest/gtest.h>
#include "TestObject.hpp"

TEST(ArrayTest, AddRemoveAll)
{
    ASSERT_EQ(0, TestObject::count);
    Base::Object::Mark();
    Base::Array *arr = new (Base::collect) Base::Array;
    for (size_t i = 0; i < 100; i++)
        arr->addObject(new (Base::collect) TestObject);
    ASSERT_EQ((size_t)100, arr->count());
    arr->removeAllObjects();
    ASSERT_EQ((size_t)0, arr->count());
    for (size_t i = 0; i < 100; i++)
        arr->addObject(new (Base::collect) TestObject);
    ASSERT_EQ((size_t)100, arr->count());
    Base::Object::Collect();
    ASSERT_EQ(0, TestObject::count);
}
TEST(ArrayTest, AddRemoveAllWithCapacity)
{
    ASSERT_EQ(0, TestObject::count);
    Base::Object::Mark();
    Base::Array *arr = new (Base::collect) Base::Array(10);
    for (size_t i = 0; i < 100; i++)
        arr->addObject(new (Base::collect) TestObject);
    ASSERT_EQ((size_t)100, arr->count());
    arr->removeAllObjects();
    ASSERT_EQ((size_t)0, arr->count());
    for (size_t i = 0; i < 100; i++)
        arr->addObject(new (Base::collect) TestObject);
    ASSERT_EQ((size_t)100, arr->count());
    Base::Object::Collect();
    ASSERT_EQ(0, TestObject::count);
}
TEST(ArrayTest, ObjectAndIndexOf)
{
    ASSERT_EQ(0, TestObject::count);
    Base::Object::Mark();
    Base::Array *arr = new (Base::collect) Base::Array;
    for (size_t i = 0; i < 100; i++)
        arr->addObject(new (Base::collect) TestObject);
    TestObject *o = new (Base::collect) TestObject;
    arr->addObject(o);
    ASSERT_EQ((size_t)101, arr->count());
    ASSERT_TRUE(0 == arr->object(101));
    ASSERT_TRUE(0 == arr->object(-102));
    ASSERT_TRUE(o == arr->object(100));
    ASSERT_TRUE(o == arr->object(-1));
    ASSERT_EQ(100, arr->indexOfObject(o));
    ASSERT_EQ(100, arr->lastIndexOfObject(o));
    ASSERT_TRUE(Base::isNone(arr->indexOfObject(new (Base::collect) TestObject)));
    ASSERT_TRUE(Base::isNone(arr->indexOfObject(0)));
    ASSERT_TRUE(Base::isNone(arr->lastIndexOfObject(new (Base::collect) TestObject)));
    ASSERT_TRUE(Base::isNone(arr->lastIndexOfObject(0)));
    arr->addObject(o);
    ASSERT_TRUE(o == arr->object(100));
    ASSERT_TRUE(o == arr->object(101));
    ASSERT_EQ(100, arr->indexOfObject(o));
    ASSERT_EQ(101, arr->lastIndexOfObject(o));
    arr->addObject(0);
    ASSERT_EQ(102, arr->indexOfObject(0));
    ASSERT_EQ(102, arr->lastIndexOfObject(0));
    ASSERT_TRUE(0 == arr->object(102));
    arr->addObject(0);
    ASSERT_TRUE(0 == arr->object(103));
    ASSERT_EQ(102, arr->indexOfObject(0));
    ASSERT_EQ(103, arr->lastIndexOfObject(0));
    Base::Object::Collect();
    ASSERT_EQ(0, TestObject::count);
}
TEST(ArrayTest, InsertReplaceRemove)
{
    ASSERT_EQ(0, TestObject::count);
    Base::Object::Mark();
    Base::Array *arr = new (Base::collect) Base::Array;
    for (size_t i = 0; i < 100; i++)
        arr->addObject(new (Base::collect) TestObject);
    TestObject *o = new (Base::collect) TestObject;
    arr->insertObject(100, o);
    ASSERT_EQ((size_t)101, arr->count());
    ASSERT_TRUE(o == arr->object(100));
    ASSERT_TRUE(o == arr->object(-1));
    ASSERT_EQ(100, arr->indexOfObject(o));
    arr->insertObject(42, o);
    ASSERT_EQ((size_t)102, arr->count());
    ASSERT_TRUE(o == arr->object(42));
    ASSERT_TRUE(o == arr->object(101));
    ASSERT_TRUE(o == arr->object(-1));
    ASSERT_EQ(42, arr->indexOfObject(o));
    arr->removeObject(42);
    ASSERT_EQ((size_t)101, arr->count());
    ASSERT_FALSE(o == arr->object(42));
    ASSERT_EQ(100, arr->indexOfObject(o));
    arr->replaceObject(100, 0);
    ASSERT_EQ((size_t)101, arr->count());
    ASSERT_TRUE(0 == arr->object(100));
    ASSERT_TRUE(0 == arr->object(-1));
    TestObject *lo = (TestObject *)arr->object(99);
    arr->removeObject(-1);
    ASSERT_EQ((size_t)100, arr->count());
    ASSERT_TRUE(lo == arr->object(-1));
    Base::Object::Collect();
    ASSERT_EQ(0, TestObject::count);
}
TEST(ArrayTest, AllInsertRemove)
{
    ASSERT_EQ(0, TestObject::count);
    Base::Object::Mark();
    Base::Array *arr = new (Base::collect) Base::Array;
    for (ssize_t i = 0; i < 100; i++)
        arr->insertObject(i, new (Base::collect) TestObject);
    for (ssize_t i = 99; i >= 0; i--)
        arr->removeObject(i);
    Base::Object::Collect();
    ASSERT_EQ(0, TestObject::count);
}
TEST(ArrayTest, Contents)
{
    ASSERT_EQ(0, TestObject::count);
    Base::Object::Mark();
    Base::Array *arr = new (Base::collect) Base::Array;
    for (ssize_t i = 0; i < 100; i++)
        arr->addObject(new (Base::collect) TestObject);
    ASSERT_EQ((size_t)100, arr->count());
    Base::Object **carr = arr->objects();
    for (ssize_t i = 0, n = arr->count(); i < n; i++)
        ASSERT_EQ(i, ((TestObject *)carr[i])->objid);
    carr = arr->objects();
    for (ssize_t i = 0, n = arr->count(); i < n; i++)
        ASSERT_EQ(i, ((TestObject *)carr[i])->objid);
    TestObject *o = new (Base::collect) TestObject;
    arr->insertObject(42, o);
    ASSERT_EQ((size_t)101, arr->count());
    carr = arr->objects();
    for (ssize_t i = 0, n = 42; i < n; i++)
        ASSERT_EQ(i, ((TestObject *)carr[i])->objid);
    ASSERT_EQ(100, ((TestObject *)carr[42])->objid);
    for (ssize_t i = 43, n = arr->count(); i < n; i++)
        ASSERT_EQ(i - 1, ((TestObject *)carr[i])->objid);
    arr->removeObject(42);
    ASSERT_EQ((size_t)100, arr->count());
    carr = arr->objects();
    for (ssize_t i = 0, n = arr->count(); i < n; i++)
        ASSERT_EQ(i, ((TestObject *)carr[i])->objid);
    Base::Object::Collect();
    ASSERT_EQ(0, TestObject::count);
}
TEST(ArrayTest, MoveAll)
{
    ASSERT_EQ(0, TestObject::count);
    Base::Object::Mark();
    Base::Array *arr = new (Base::collect) Base::Array;
    for (ssize_t i = 0; i < 100; i++)
        arr->addObject(new (Base::collect) TestObject);
    ASSERT_EQ((size_t)100, arr->count());
    Base::Object **carr = arr->objects();
    for (ssize_t i = 0, n = arr->count(); i < n; i++)
        ASSERT_EQ(i, ((TestObject *)carr[i])->objid);
    for (ssize_t i = 0; i < 100; i++)
        arr->moveObject(99 - i, i);
    for (ssize_t i = 0, n = arr->count(); i < n; i++)
        ASSERT_EQ(i, ((TestObject *)carr[i])->objid);
    Base::Object::Collect();
    ASSERT_EQ(0, TestObject::count);
}
TEST(ArrayTest, AddObjects)
{
    mark_and_collect
    {
        Base::Object *objs1[] =
        {
            Base::Value::create(0)->autorelease(),
            Base::Value::create(1)->autorelease(),
            Base::Value::create(2)->autorelease(),
        };
        Base::Object *objs2[] =
        {
            Base::Value::create(3)->autorelease(),
            Base::Value::create(4)->autorelease(),
        };
        Base::Array *arr = new (Base::collect) Base::Array;
        arr->addObjects(objs1, NELEMS(objs1));
        arr->addObjects(0, 2);
        arr->addObjects(objs2, NELEMS(objs2));
        arr->addObjects(0, 0);
        ASSERT_EQ(7U, arr->count());
        ASSERT_EQ(0, ((Base::Value *)arr->object(0))->integerValue());
        ASSERT_EQ(1, ((Base::Value *)arr->object(1))->integerValue());
        ASSERT_EQ(2, ((Base::Value *)arr->object(2))->integerValue());
        ASSERT_EQ((void *)0, arr->object(3));
        ASSERT_EQ((void *)0, arr->object(4));
        ASSERT_EQ(3, ((Base::Value *)arr->object(5))->integerValue());
        ASSERT_EQ(4, ((Base::Value *)arr->object(6))->integerValue());
    }
}
TEST(ArrayTest, InsertObjects)
{
    mark_and_collect
    {
        Base::Object *objs1[] =
        {
            Base::Value::create(0)->autorelease(),
            Base::Value::create(1)->autorelease(),
            Base::Value::create(2)->autorelease(),
        };
        Base::Object *objs2[] =
        {
            Base::Value::create(3)->autorelease(),
            Base::Value::create(4)->autorelease(),
        };
        Base::Array *arr = new (Base::collect) Base::Array;
        arr->insertObjects(0, objs2, NELEMS(objs2));
        arr->insertObjects(0, 0, 2);
        arr->insertObjects(2, objs1, NELEMS(objs1));
        arr->insertObjects(5, 0, 2);
        arr->insertObjects(9, 0, 2);
        arr->insertObjects(0, 0, 0);
        ASSERT_EQ(11U, arr->count());
        ASSERT_EQ((void *)0, arr->object(0));
        ASSERT_EQ((void *)0, arr->object(1));
        ASSERT_EQ(0, ((Base::Value *)arr->object(2))->integerValue());
        ASSERT_EQ(1, ((Base::Value *)arr->object(3))->integerValue());
        ASSERT_EQ(2, ((Base::Value *)arr->object(4))->integerValue());
        ASSERT_EQ((void *)0, arr->object(5));
        ASSERT_EQ((void *)0, arr->object(6));
        ASSERT_EQ(3, ((Base::Value *)arr->object(7))->integerValue());
        ASSERT_EQ(4, ((Base::Value *)arr->object(8))->integerValue());
        ASSERT_EQ((void *)0, arr->object(9));
        ASSERT_EQ((void *)0, arr->object(10));
    }
}
TEST(ArrayTest, ReplaceObjects)
{
    mark_and_collect
    {
        Base::Object *objs1[] =
        {
            Base::Value::create(0)->autorelease(),
            Base::Value::create(1)->autorelease(),
            Base::Value::create(2)->autorelease(),
        };
        Base::Object *objs2[] =
        {
            Base::Value::create(3)->autorelease(),
            Base::Value::create(4)->autorelease(),
        };
        Base::Array *arr = new (Base::collect) Base::Array;
        arr->addObjects(objs1, NELEMS(objs1));
        arr->addObjects(objs1, NELEMS(objs1));
        arr->replaceObjects(5, objs2, 2);
        arr->replaceObjects(1, objs2, 2);
        arr->replaceObjects(0, 0, 2);
        arr->replaceObjects(5, 0, 0);
        ASSERT_EQ(6U, arr->count());
        ASSERT_EQ((void *)0, arr->object(0));
        ASSERT_EQ((void *)0, arr->object(1));
        ASSERT_EQ(4, ((Base::Value *)arr->object(2))->integerValue());
        ASSERT_EQ(0, ((Base::Value *)arr->object(3))->integerValue());
        ASSERT_EQ(1, ((Base::Value *)arr->object(4))->integerValue());
        ASSERT_EQ(3, ((Base::Value *)arr->object(5))->integerValue());
    }
}
TEST(ArrayTest, MoveObjects)
{
    mark_and_collect
    {
        Base::Object *objs[] =
        {
            Base::Value::create(0)->autorelease(),
            Base::Value::create(1)->autorelease(),
            Base::Value::create(2)->autorelease(),
            Base::Value::create(3)->autorelease(),
            Base::Value::create(4)->autorelease(),
            Base::Value::create(5)->autorelease(),
            Base::Value::create(6)->autorelease(),
            Base::Value::create(7)->autorelease(),
            Base::Value::create(8)->autorelease(),
            Base::Value::create(9)->autorelease(),
        };
        Base::Array *arr = new (Base::collect) Base::Array;
        arr->addObjects(objs, NELEMS(objs));
        arr->moveObjects(0, 9, 2);
        arr->moveObjects(9, 1, 2);
        arr->moveObjects(1, 4, 4);
        arr->moveObjects(0, 0, 2);
        arr->moveObjects(0, 1, 0);
        ASSERT_EQ(10U, arr->count());
        ASSERT_EQ(9, ((Base::Value *)arr->object(0))->integerValue());
        ASSERT_EQ(4, ((Base::Value *)arr->object(1))->integerValue());
        ASSERT_EQ(5, ((Base::Value *)arr->object(2))->integerValue());
        ASSERT_EQ(6, ((Base::Value *)arr->object(3))->integerValue());
        ASSERT_EQ(7, ((Base::Value *)arr->object(4))->integerValue());
        ASSERT_EQ(1, ((Base::Value *)arr->object(5))->integerValue());
        ASSERT_EQ(2, ((Base::Value *)arr->object(6))->integerValue());
        ASSERT_EQ(3, ((Base::Value *)arr->object(7))->integerValue());
        ASSERT_EQ(8, ((Base::Value *)arr->object(8))->integerValue());
        ASSERT_EQ(0, ((Base::Value *)arr->object(9))->integerValue());
    }
}
TEST(ArrayTest, RemoveObjects)
{
    mark_and_collect
    {
        Base::Object *objs1[] =
        {
            Base::Value::create(0)->autorelease(),
            Base::Value::create(1)->autorelease(),
            Base::Value::create(2)->autorelease(),
        };
        Base::Object *objs2[] =
        {
            Base::Value::create(3)->autorelease(),
            Base::Value::create(4)->autorelease(),
        };
        Base::Array *arr = new (Base::collect) Base::Array;
        arr->addObjects(objs1, NELEMS(objs1));
        arr->addObjects(objs1, NELEMS(objs1));
        arr->removeObjects(5, 2);
        arr->removeObjects(1, 2);
        arr->removeObjects(0, 0);
        ASSERT_EQ(3U, arr->count());
        ASSERT_EQ(0, ((Base::Value *)arr->object(0))->integerValue());
        ASSERT_EQ(0, ((Base::Value *)arr->object(1))->integerValue());
        ASSERT_EQ(1, ((Base::Value *)arr->object(2))->integerValue());
        arr->addObjects(objs2, NELEMS(objs2));
        arr->removeObjects(1, 3);
        ASSERT_EQ(2U, arr->count());
        ASSERT_EQ(0, ((Base::Value *)arr->object(0))->integerValue());
        ASSERT_EQ(4, ((Base::Value *)arr->object(1))->integerValue());
    }
}

/*
 * GenericArray<T>
 */
TEST(GenericArrayTest, AddRemoveAll)
{
    ASSERT_EQ(0, TestObject::count);
    Base::Object::Mark();
    Base::GenericArray<TestObject> *arr = new (Base::collect) Base::GenericArray<TestObject>;
    for (size_t i = 0; i < 100; i++)
        arr->addObject(new (Base::collect) TestObject);
    ASSERT_EQ((size_t)100, arr->count());
    arr->removeAllObjects();
    ASSERT_EQ((size_t)0, arr->count());
    for (size_t i = 0; i < 100; i++)
        arr->addObject(new (Base::collect) TestObject);
    ASSERT_EQ((size_t)100, arr->count());
    Base::Object::Collect();
    ASSERT_EQ(0, TestObject::count);
}
TEST(GenericArrayTest, AddRemoveAllWithCapacity)
{
    ASSERT_EQ(0, TestObject::count);
    Base::Object::Mark();
    Base::GenericArray<TestObject> *arr = new (Base::collect) Base::GenericArray<TestObject>(10);
    for (size_t i = 0; i < 100; i++)
        arr->addObject(new (Base::collect) TestObject);
    ASSERT_EQ((size_t)100, arr->count());
    arr->removeAllObjects();
    ASSERT_EQ((size_t)0, arr->count());
    for (size_t i = 0; i < 100; i++)
        arr->addObject(new (Base::collect) TestObject);
    ASSERT_EQ((size_t)100, arr->count());
    Base::Object::Collect();
    ASSERT_EQ(0, TestObject::count);
}
TEST(GenericArrayTest, ObjectAndIndexOf)
{
    ASSERT_EQ(0, TestObject::count);
    Base::Object::Mark();
    Base::GenericArray<TestObject> *arr = new (Base::collect) Base::GenericArray<TestObject>;
    for (size_t i = 0; i < 100; i++)
        arr->addObject(new (Base::collect) TestObject);
    TestObject *o = new (Base::collect) TestObject;
    arr->addObject(o);
    ASSERT_EQ((size_t)101, arr->count());
    ASSERT_TRUE(0 == arr->object(101));
    ASSERT_TRUE(0 == arr->object(-102));
    ASSERT_TRUE(o == arr->object(100));
    ASSERT_TRUE(o == arr->object(-1));
    ASSERT_EQ(100, arr->indexOfObject(o));
    ASSERT_EQ(100, arr->lastIndexOfObject(o));
    ASSERT_TRUE(Base::isNone(arr->indexOfObject(new (Base::collect) TestObject)));
    ASSERT_TRUE(Base::isNone(arr->indexOfObject(0)));
    ASSERT_TRUE(Base::isNone(arr->lastIndexOfObject(new (Base::collect) TestObject)));
    ASSERT_TRUE(Base::isNone(arr->lastIndexOfObject(0)));
    arr->addObject(o);
    ASSERT_TRUE(o == arr->object(100));
    ASSERT_TRUE(o == arr->object(101));
    ASSERT_EQ(100, arr->indexOfObject(o));
    ASSERT_EQ(101, arr->lastIndexOfObject(o));
    arr->addObject(0);
    ASSERT_EQ(102, arr->indexOfObject(0));
    ASSERT_EQ(102, arr->lastIndexOfObject(0));
    ASSERT_TRUE(0 == arr->object(102));
    arr->addObject(0);
    ASSERT_TRUE(0 == arr->object(103));
    ASSERT_EQ(102, arr->indexOfObject(0));
    ASSERT_EQ(103, arr->lastIndexOfObject(0));
    Base::Object::Collect();
    ASSERT_EQ(0, TestObject::count);
}
TEST(GenericArrayTest, InsertReplaceRemove)
{
    ASSERT_EQ(0, TestObject::count);
    Base::Object::Mark();
    Base::GenericArray<TestObject> *arr = new (Base::collect) Base::GenericArray<TestObject>;
    for (size_t i = 0; i < 100; i++)
        arr->addObject(new (Base::collect) TestObject);
    TestObject *o = new (Base::collect) TestObject;
    arr->insertObject(100, o);
    ASSERT_EQ((size_t)101, arr->count());
    ASSERT_TRUE(o == arr->object(100));
    ASSERT_TRUE(o == arr->object(-1));
    ASSERT_EQ(100, arr->indexOfObject(o));
    arr->insertObject(42, o);
    ASSERT_EQ((size_t)102, arr->count());
    ASSERT_TRUE(o == arr->object(42));
    ASSERT_TRUE(o == arr->object(101));
    ASSERT_TRUE(o == arr->object(-1));
    ASSERT_EQ(42, arr->indexOfObject(o));
    arr->removeObject(42);
    ASSERT_EQ((size_t)101, arr->count());
    ASSERT_FALSE(o == arr->object(42));
    ASSERT_EQ(100, arr->indexOfObject(o));
    arr->replaceObject(100, 0);
    ASSERT_EQ((size_t)101, arr->count());
    ASSERT_TRUE(0 == arr->object(100));
    ASSERT_TRUE(0 == arr->object(-1));
    TestObject *lo = arr->object(99);
    arr->removeObject(-1);
    ASSERT_EQ((size_t)100, arr->count());
    ASSERT_TRUE(lo == arr->object(-1));
    Base::Object::Collect();
    ASSERT_EQ(0, TestObject::count);
}
TEST(GenericArrayTest, AllInsertRemove)
{
    ASSERT_EQ(0, TestObject::count);
    Base::Object::Mark();
    Base::GenericArray<TestObject> *arr = new (Base::collect) Base::GenericArray<TestObject>;
    for (ssize_t i = 0; i < 100; i++)
        arr->insertObject(i, new (Base::collect) TestObject);
    for (ssize_t i = 99; i >= 0; i--)
        arr->removeObject(i);
    Base::Object::Collect();
    ASSERT_EQ(0, TestObject::count);
}
TEST(GenericArrayTest, Contents)
{
    ASSERT_EQ(0, TestObject::count);
    Base::Object::Mark();
    Base::GenericArray<TestObject> *arr = new (Base::collect) Base::GenericArray<TestObject>;
    for (ssize_t i = 0; i < 100; i++)
        arr->addObject(new (Base::collect) TestObject);
    ASSERT_EQ((size_t)100, arr->count());
    TestObject **carr = arr->objects();
    for (ssize_t i = 0, n = arr->count(); i < n; i++)
        ASSERT_EQ(i, carr[i]->objid);
    carr = arr->objects();
    for (ssize_t i = 0, n = arr->count(); i < n; i++)
        ASSERT_EQ(i, carr[i]->objid);
    TestObject *o = new (Base::collect) TestObject;
    arr->insertObject(42, o);
    ASSERT_EQ((size_t)101, arr->count());
    carr = arr->objects();
    for (ssize_t i = 0, n = 42; i < n; i++)
        ASSERT_EQ(i, carr[i]->objid);
    ASSERT_EQ(100, carr[42]->objid);
    for (ssize_t i = 43, n = arr->count(); i < n; i++)
        ASSERT_EQ(i - 1, carr[i]->objid);
    arr->removeObject(42);
    ASSERT_EQ((size_t)100, arr->count());
    carr = arr->objects();
    for (ssize_t i = 0, n = arr->count(); i < n; i++)
        ASSERT_EQ(i, carr[i]->objid);
    Base::Object::Collect();
    ASSERT_EQ(0, TestObject::count);
}
TEST(GenericArrayTest, MoveAll)
{
    ASSERT_EQ(0, TestObject::count);
    Base::Object::Mark();
    Base::GenericArray<TestObject> *arr = new (Base::collect) Base::GenericArray<TestObject>;
    for (ssize_t i = 0; i < 100; i++)
        arr->addObject(new (Base::collect) TestObject);
    ASSERT_EQ((size_t)100, arr->count());
    TestObject **carr = arr->objects();
    for (ssize_t i = 0, n = arr->count(); i < n; i++)
        ASSERT_EQ(i, carr[i]->objid);
    for (ssize_t i = 0; i < 100; i++)
        arr->moveObject(99 - i, i);
    for (ssize_t i = 0, n = arr->count(); i < n; i++)
        ASSERT_EQ(i, carr[i]->objid);
    Base::Object::Collect();
    ASSERT_EQ(0, TestObject::count);
}
