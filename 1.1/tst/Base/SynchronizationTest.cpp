/*
 * Base/SynchronizationTest.cpp
 *
 * Copyright 2010-2015 Navimatics Corporation. All rights reserved.
 */
/*
 * Portions of this file have been adapted from
 * https://chromium.googlesource.com/v8/v8.git/+/refs/heads/4.2.69/test/unittests/base/platform/semaphore-unittest.cc
 *
 * The following copyright notice appears at the top of this file:
 *
 * Copyright 2014 the V8 project authors. All rights reserved.
 * Use of this source code is governed by a BSD-style license that can be
 * found in the LICENSE file.
 */

#include <Base/Synchronization.hpp>
#include <Base/Thread.hpp>
#include <gtest/gtest.h>

namespace SynchronizationTest
{
/* BEGIN V8:4.2.69/test/unittests/base/platform/semaphore-unittest.cc */

static const char kAlphabet[] = "XKOAD";
static const size_t kAlphabetSize = sizeof(kAlphabet) - 1;
static const size_t kBufferSize = 987;  // GCD(buffer size, alphabet size) = 1
static const size_t kDataSize = kBufferSize * kAlphabetSize * 10;

class ProducerThread : public Base::Thread
{
public:
    ProducerThread(char *buffer, Base::Semaphore* free_space, Base::Semaphore* used_space)
    {
        buffer_ = buffer;
        free_space_ = free_space;
        used_space_ = used_space;
    }
    void run()
    {
        for (size_t n = 0; n < kDataSize; ++n)
        {
            free_space_->wait();
            buffer_[n % kBufferSize] = kAlphabet[n % kAlphabetSize];
            used_space_->notify();
        }
    }
private:
    char *buffer_;
    Base::Semaphore *free_space_;
    Base::Semaphore *used_space_;
};
class ConsumerThread : public Base::Thread
{
public:
    ConsumerThread(const char *buffer, Base::Semaphore *free_space, Base::Semaphore *used_space)
    {
        buffer_ = buffer;
        free_space_ = free_space;
        used_space_ = used_space;
    }

    void run()
    {
        for (size_t n = 0; n < kDataSize; ++n)
        {
            used_space_->wait();
            EXPECT_EQ(kAlphabet[n % kAlphabetSize], buffer_[n % kBufferSize]);
            free_space_->notify();
        }
    }
private:
    const char *buffer_;
    Base::Semaphore *free_space_;
    Base::Semaphore *used_space_;
};
class WaitAndSignalThread : public Base::Thread
{
public:
    WaitAndSignalThread(Base::Semaphore *semaphore)
    {
        semaphore_ = semaphore;
    }
    void run()
    {
        for (int n = 0; n < 100; ++n)
        {
            semaphore_->wait();
            semaphore_->notify();
        }
    }
private:
    Base::Semaphore *semaphore_;
};

/* END V8:4.2.69/test/unittests/base/platform/semaphore-unittest.cc */
}

TEST(SynchronizationTest, SemaphoreProducerConsumer)
{
    mark_and_collect
    {
/* BEGIN V8:4.2.69/test/unittests/base/platform/semaphore-unittest.cc */
        char buffer[SynchronizationTest::kBufferSize];
        memset(buffer, 0, sizeof(buffer));
        Base::Semaphore *free_space = new (Base::collect) Base::Semaphore(SynchronizationTest::kBufferSize);
        Base::Semaphore *used_space = new (Base::collect) Base::Semaphore;
        SynchronizationTest::ProducerThread *producer_thread =
            new (Base::collect) SynchronizationTest::ProducerThread(buffer, free_space, used_space);
        SynchronizationTest::ConsumerThread *consumer_thread =
            new (Base::collect) SynchronizationTest::ConsumerThread(buffer, free_space, used_space);
        producer_thread->start();
        consumer_thread->start();
        producer_thread->wait();
        consumer_thread->wait();
/* END V8:4.2.69/test/unittests/base/platform/semaphore-unittest.cc */
    }
}
TEST(SynchronizationTest, SemaphoreWaitAndSignal)
{
    mark_and_collect
    {
/* BEGIN V8:4.2.69/test/unittests/base/platform/semaphore-unittest.cc */
        Base::Semaphore *semaphore = new (Base::collect) Base::Semaphore;
        SynchronizationTest::WaitAndSignalThread *t1 =
            new (Base::collect) SynchronizationTest::WaitAndSignalThread(semaphore);
        SynchronizationTest::WaitAndSignalThread *t2 =
            new (Base::collect) SynchronizationTest::WaitAndSignalThread(semaphore);
        t1->start();
        t2->start();
        // Make something available.
        semaphore->notify();
        t1->wait();
        t2->wait();
        semaphore->wait();
/* END V8:4.2.69/test/unittests/base/platform/semaphore-unittest.cc */
    }
}
