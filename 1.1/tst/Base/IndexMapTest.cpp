/*
 * Base/IndexMapTest.cpp
 *
 * Copyright 2010-2013 Navimatics Corporation. All rights reserved.
 */

#include <Base/IndexMap.hpp>
#include <Base/CArray.hpp>
#include <Base/Value.hpp>
#include <gtest/gtest.h>
#include "TestObject.hpp"

TEST(IndexMapTest, AddRemoveAll)
{
    ASSERT_EQ(0, TestObject::count);
    Base::Object::Mark();
    Base::IndexMap *map = new (Base::collect) Base::IndexMap;
    for (size_t i = 0; i < 100; i++)
    {
        TestObject *o = new (Base::collect) TestObject;
        map->setObject(o->objid, o);
    }
    ASSERT_EQ((size_t)100, map->count());
    map->removeAllObjects();
    ASSERT_EQ((size_t)0, map->count());
    for (size_t i = 0; i < 100; i++)
    {
        TestObject *o = new (Base::collect) TestObject;
        map->setObject(o->objid, o);
    }
    ASSERT_EQ((size_t)100, map->count());
    Base::Object::Collect();
    ASSERT_EQ(0, TestObject::count);
}
TEST(IndexMapTest, ObjectAndGetObject)
{
    ASSERT_EQ(0, TestObject::count);
    Base::Object::Mark();
    Base::IndexMap *map = new (Base::collect) Base::IndexMap;
    for (size_t i = 0; i < 100; i++)
    {
        TestObject *o = new (Base::collect) TestObject;
        map->setObject(o->objid, o);
    }
    ASSERT_EQ((size_t)100, map->count());
    TestObject *o = new (Base::collect) TestObject;
    map->setObject(o->objid, o);
    ASSERT_EQ((size_t)101, map->count());
    ASSERT_TRUE(o == map->object(o->objid));
    Base::Object *go;
    ASSERT_TRUE(map->getObject(o->objid, go));
    ASSERT_TRUE(o == go);
    Base::Object::Collect();
    ASSERT_EQ(0, TestObject::count);
}
TEST(IndexMapTest, AddReplaceRemove)
{
    ASSERT_EQ(0, TestObject::count);
    Base::Object::Mark();
    Base::IndexMap *map = new (Base::collect) Base::IndexMap;
    for (size_t i = 0; i < 100; i++)
    {
        TestObject *o = new (Base::collect) TestObject;
        map->setObject(o->objid, o);
    }
    ASSERT_EQ((size_t)100, map->count());
    TestObject *o = new (Base::collect) TestObject;
    map->setObject(o->objid, o);
    ASSERT_EQ((size_t)101, map->count());
    ASSERT_TRUE(o == map->object(o->objid));
    TestObject *o2 = new (Base::collect) TestObject;
    map->setObject(o->objid, o2);
    ASSERT_EQ((size_t)101, map->count());
    ASSERT_TRUE(o2 == map->object(o->objid));
    map->removeObject(o->objid);
    ASSERT_EQ((size_t)100, map->count());
    ASSERT_TRUE(0 == map->object(o->objid));
    Base::Object *go;
    ASSERT_FALSE(map->getObject(o->objid, go));
    ASSERT_TRUE(0 == go);
    Base::Object::Collect();
    ASSERT_EQ(0, TestObject::count);
}
TEST(IndexMapTest, AddObjects)
{
    ASSERT_EQ(0, TestObject::count);
    Base::Object::Mark();
    TestObject *objbuf[110];
    for (size_t i = 0; i < 100; i++)
        objbuf[i] = new (Base::collect) TestObject;
    Base::IndexMap *map1 = new (Base::collect) Base::IndexMap;
    for (size_t i = 0; i < 100; i++)
    {
        TestObject *o = objbuf[i];
        map1->setObject(o->objid, o);
    }
    Base::IndexMap *map2 = new (Base::collect) Base::IndexMap;
    for (size_t i = 0; i < 10; i++)
    {
        TestObject *o = objbuf[i];
        map2->setObject(o->objid, new (Base::collect) TestObject);
    }
    for (size_t i = 100; i < 110; i++)
    {
        TestObject *o = new (Base::collect) TestObject;
        objbuf[i] = o;
        map2->setObject(o->objid, o);
    }
    ASSERT_EQ((size_t)100, map1->count());
    ASSERT_EQ((size_t)20, map2->count());
    map1->addObjects(map2);
    ASSERT_EQ((size_t)110, map1->count());
    for (size_t i = 0; i < 10; i++)
    {
        TestObject *o = objbuf[i];
        ASSERT_TRUE(map1->object(o->objid) == map2->object(o->objid));
    }
    for (size_t i = 100; i < 110; i++)
    {
        TestObject *o = objbuf[i];
        ASSERT_TRUE(map1->object(o->objid) == map2->object(o->objid));
    }
    Base::Object::Collect();
    ASSERT_EQ(0, TestObject::count);
}
TEST(IndexMapTest, Contents)
{
    ASSERT_EQ(0, TestObject::count);
    Base::Object::Mark();
    Base::IndexMap *map = new (Base::collect) Base::IndexMap;
    for (size_t i = 0; i < 100; i++)
    {
        TestObject *o = new (Base::collect) TestObject;
        map->setObject(o->objid, o);
    }
    ASSERT_EQ((size_t)100, map->count());
    Base::index_t *idxs = map->indexes();
    Base::Object **objs = map->objects();
    ASSERT_EQ((size_t)100, Base::carr_length(idxs));
    ASSERT_EQ((size_t)100, Base::carr_length(objs));
    for (size_t i = 0; i < 100; i++)
        ASSERT_TRUE(idxs[i] == (Base::index_t)((TestObject *)objs[i])->objid);
    map->getIndexesAndObjects(idxs, objs);
    ASSERT_EQ((size_t)100, Base::carr_length(idxs));
    ASSERT_EQ((size_t)100, Base::carr_length(objs));
    for (size_t i = 0; i < 100; i++)
        ASSERT_TRUE(idxs[i] == (Base::index_t)((TestObject *)objs[i])->objid);
    Base::Object::Collect();
    ASSERT_EQ(0, TestObject::count);
}
TEST(IndexMapTest, OrderedContents)
{
    Base::Object::Mark();
    Base::IndexMap *map = new (Base::collect) Base::IndexMap;
    for (int i = 0; i < 100; i++)
    {
        Base::Value *v = Base::Value::create(i);
        v->autorelease();
        map->setObject(v->integerValue(), v);
    }
    ASSERT_EQ((size_t)100, map->count());
    Base::index_t *idxs = map->indexes();
    Base::Object **objs = map->objects();
    ASSERT_EQ((size_t)100, Base::carr_length(idxs));
    ASSERT_EQ((size_t)100, Base::carr_length(objs));
    for (size_t i = 0; i < 100; i++)
    {
        ASSERT_TRUE(i == idxs[i]);
        ASSERT_TRUE(i == (Base::index_t)((Base::Value *)objs[i])->integerValue());
    }
    map->getIndexesAndObjects(idxs, objs);
    ASSERT_EQ((size_t)100, Base::carr_length(idxs));
    ASSERT_EQ((size_t)100, Base::carr_length(objs));
    for (size_t i = 0; i < 100; i++)
    {
        ASSERT_TRUE(i == idxs[i]);
        ASSERT_TRUE(i == (Base::index_t)((Base::Value *)objs[i])->integerValue());
    }
    Base::Object::Collect();
}

/*
 * GenericIndexMap<T>
 */
TEST(GenericIndexMapTest, AddRemoveAll)
{
    ASSERT_EQ(0, TestObject::count);
    Base::Object::Mark();
    Base::GenericIndexMap<TestObject> *map = new (Base::collect) Base::GenericIndexMap<TestObject>;
    for (size_t i = 0; i < 100; i++)
    {
        TestObject *o = new (Base::collect) TestObject;
        map->setObject(o->objid, o);
    }
    ASSERT_EQ((size_t)100, map->count());
    map->removeAllObjects();
    ASSERT_EQ((size_t)0, map->count());
    for (size_t i = 0; i < 100; i++)
    {
        TestObject *o = new (Base::collect) TestObject;
        map->setObject(o->objid, o);
    }
    ASSERT_EQ((size_t)100, map->count());
    Base::Object::Collect();
    ASSERT_EQ(0, TestObject::count);
}
TEST(GenericIndexMapTest, ObjectAndGetObject)
{
    ASSERT_EQ(0, TestObject::count);
    Base::Object::Mark();
    Base::GenericIndexMap<TestObject> *map = new (Base::collect) Base::GenericIndexMap<TestObject>;
    for (size_t i = 0; i < 100; i++)
    {
        TestObject *o = new (Base::collect) TestObject;
        map->setObject(o->objid, o);
    }
    ASSERT_EQ((size_t)100, map->count());
    TestObject *o = new (Base::collect) TestObject;
    map->setObject(o->objid, o);
    ASSERT_EQ((size_t)101, map->count());
    ASSERT_TRUE(o == map->object(o->objid));
    TestObject *go;
    ASSERT_TRUE(map->getObject(o->objid, go));
    ASSERT_TRUE(o == go);
    Base::Object::Collect();
    ASSERT_EQ(0, TestObject::count);
}
TEST(GenericIndexMapTest, AddReplaceRemove)
{
    ASSERT_EQ(0, TestObject::count);
    Base::Object::Mark();
    Base::GenericIndexMap<TestObject> *map = new (Base::collect) Base::GenericIndexMap<TestObject>;
    for (size_t i = 0; i < 100; i++)
    {
        TestObject *o = new (Base::collect) TestObject;
        map->setObject(o->objid, o);
    }
    ASSERT_EQ((size_t)100, map->count());
    TestObject *o = new (Base::collect) TestObject;
    map->setObject(o->objid, o);
    ASSERT_EQ((size_t)101, map->count());
    ASSERT_TRUE(o == map->object(o->objid));
    TestObject *o2 = new (Base::collect) TestObject;
    map->setObject(o->objid, o2);
    ASSERT_EQ((size_t)101, map->count());
    ASSERT_TRUE(o2 == map->object(o->objid));
    map->removeObject(o->objid);
    ASSERT_EQ((size_t)100, map->count());
    ASSERT_TRUE(0 == map->object(o->objid));
    TestObject *go;
    ASSERT_FALSE(map->getObject(o->objid, go));
    ASSERT_TRUE(0 == go);
    Base::Object::Collect();
    ASSERT_EQ(0, TestObject::count);
}
TEST(GenericIndexMapTest, AddObjects)
{
    ASSERT_EQ(0, TestObject::count);
    Base::Object::Mark();
    TestObject *objbuf[110];
    for (size_t i = 0; i < 100; i++)
        objbuf[i] = new (Base::collect) TestObject;
    Base::GenericIndexMap<TestObject> *map1 = new (Base::collect) Base::GenericIndexMap<TestObject>;
    for (size_t i = 0; i < 100; i++)
    {
        TestObject *o = objbuf[i];
        map1->setObject(o->objid, o);
    }
    Base::GenericIndexMap<TestObject> *map2 = new (Base::collect) Base::GenericIndexMap<TestObject>;
    for (size_t i = 0; i < 10; i++)
    {
        TestObject *o = objbuf[i];
        map2->setObject(o->objid, new (Base::collect) TestObject);
    }
    for (size_t i = 100; i < 110; i++)
    {
        TestObject *o = new (Base::collect) TestObject;
        objbuf[i] = o;
        map2->setObject(o->objid, o);
    }
    ASSERT_EQ((size_t)100, map1->count());
    ASSERT_EQ((size_t)20, map2->count());
    map1->addObjects(map2);
    ASSERT_EQ((size_t)110, map1->count());
    for (size_t i = 0; i < 10; i++)
    {
        TestObject *o = objbuf[i];
        ASSERT_TRUE(map1->object(o->objid) == map2->object(o->objid));
    }
    for (size_t i = 100; i < 110; i++)
    {
        TestObject *o = objbuf[i];
        ASSERT_TRUE(map1->object(o->objid) == map2->object(o->objid));
    }
    Base::Object::Collect();
    ASSERT_EQ(0, TestObject::count);
}
TEST(GenericIndexMapTest, Contents)
{
    ASSERT_EQ(0, TestObject::count);
    Base::Object::Mark();
    Base::GenericIndexMap<TestObject> *map = new (Base::collect) Base::GenericIndexMap<TestObject>;
    for (size_t i = 0; i < 100; i++)
    {
        TestObject *o = new (Base::collect) TestObject;
        map->setObject(o->objid, o);
    }
    ASSERT_EQ((size_t)100, map->count());
    Base::index_t *idxs = map->indexes();
    TestObject **objs = map->objects();
    ASSERT_EQ((size_t)100, Base::carr_length(idxs));
    ASSERT_EQ((size_t)100, Base::carr_length(objs));
    for (size_t i = 0; i < 100; i++)
        ASSERT_TRUE(idxs[i] == (Base::index_t)objs[i]->objid);
    map->getIndexesAndObjects(idxs, objs);
    ASSERT_EQ((size_t)100, Base::carr_length(idxs));
    ASSERT_EQ((size_t)100, Base::carr_length(objs));
    for (size_t i = 0; i < 100; i++)
        ASSERT_TRUE(idxs[i] == (Base::index_t)objs[i]->objid);
    Base::Object::Collect();
    ASSERT_EQ(0, TestObject::count);
}
TEST(GenericIndexMapTest, OrderedContents)
{
    Base::Object::Mark();
    Base::GenericIndexMap<Base::Value> *map = new (Base::collect) Base::GenericIndexMap<Base::Value>;
    for (int i = 0; i < 100; i++)
    {
        Base::Value *v = Base::Value::create(i);
        v->autorelease();
        map->setObject(v->integerValue(), v);
    }
    ASSERT_EQ((size_t)100, map->count());
    Base::index_t *idxs = map->indexes();
    Base::Value **objs = map->objects();
    ASSERT_EQ((size_t)100, Base::carr_length(idxs));
    ASSERT_EQ((size_t)100, Base::carr_length(objs));
    for (size_t i = 0; i < 100; i++)
    {
        ASSERT_TRUE(i == idxs[i]);
        ASSERT_TRUE(i == (Base::index_t)objs[i]->integerValue());
    }
    map->getIndexesAndObjects(idxs, objs);
    ASSERT_EQ((size_t)100, Base::carr_length(idxs));
    ASSERT_EQ((size_t)100, Base::carr_length(objs));
    for (size_t i = 0; i < 100; i++)
    {
        ASSERT_TRUE(i == idxs[i]);
        ASSERT_TRUE(i == (Base::index_t)objs[i]->integerValue());
    }
    Base::Object::Collect();
}
