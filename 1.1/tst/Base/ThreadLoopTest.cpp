/*
 * Base/ThreadLoopTest.cpp
 *
 * Copyright 2010-2013 Navimatics Corporation. All rights reserved.
 */

#include <Base/ThreadLoop.hpp>
#include <gtest/gtest.h>

class ThreadLoopTestObject : public Base::Object
{
public:
    int flag;
};

static int thread_flag;
static void thread_invoke(Base::Object *o)
{
    ThreadLoopTestObject *obj = (ThreadLoopTestObject *)o;
    thread_flag = obj->flag;
    Base::ThreadLoopStop();
}
static void thread_run(void *arg)
{
    Base::Thread *thread = (Base::Thread *)arg;
    ThreadLoopTestObject *obj = new ThreadLoopTestObject;
    obj->threadsafe();
    obj->flag = 42;
    EXPECT_TRUE(Base::ThreadLoopInvoke(thread, thread_invoke, obj));
    obj->release();
}
TEST(ThreadLoopTest, Invoke)
{
    Base::ThreadLoopInit();
    for (size_t i = 0; 100 > i; i++)
    {
        thread_flag = 0;
        ASSERT_EQ(0, thread_flag);
        Base::Thread *thread = new Base::Thread(thread_run, Base::Thread::currentThread());
        thread->start();
        thread->detach();
        thread->release();
        Base::ThreadLoopRun();
        ASSERT_EQ(42, thread_flag);
    }
}

static int thread_invoke_count_max = 100;
static int thread_invoke_count;
static void thread_invoke_many(Base::Object *o)
{
    thread_invoke_count++;
}
static void thread_invoke_stop(Base::Object *o)
{
    Base::ThreadLoopStop();
}
static void thread_run_many(void *arg)
{
    Base::Thread *thread = (Base::Thread *)arg;
    for (size_t i = 0; thread_invoke_count_max > i; i++)
        EXPECT_TRUE(Base::ThreadLoopInvoke(thread, thread_invoke_many, 0));
    EXPECT_TRUE(Base::ThreadLoopInvoke(thread, thread_invoke_stop, 0));
}
TEST(ThreadLoopTest, InvokeMany)
{
    thread_invoke_count = 0;
    Base::ThreadLoopInit();
    Base::Thread *thread = new Base::Thread(thread_run_many, Base::Thread::currentThread());
    thread->start();
    thread->detach();
    thread->release();
    Base::ThreadLoopRun();
    ASSERT_EQ(thread_invoke_count_max, thread_invoke_count);
}
TEST(ThreadLoopTest, CancelInvoke)
{
    thread_invoke_count = 0;
    Base::ThreadLoopInit();
    for (size_t i = 0; thread_invoke_count_max > i; i++)
        ASSERT_TRUE(Base::ThreadLoopInvoke(Base::Thread::currentThread(), thread_invoke_many, 0));
    ASSERT_TRUE(Base::ThreadLoopCancelInvoke(Base::Thread::currentThread(), thread_invoke_many));
    for (size_t i = 0; thread_invoke_count_max > i; i++)
        ASSERT_TRUE(Base::ThreadLoopInvoke(Base::Thread::currentThread(), thread_invoke_many, 0));
    ASSERT_TRUE(Base::ThreadLoopInvoke(Base::Thread::currentThread(), thread_invoke_stop, 0));
    Base::ThreadLoopRun();
    ASSERT_EQ(thread_invoke_count_max, thread_invoke_count);
}

static int thread_timer_flag;
static void thread_timer_invoke(Base::Object *o)
{
    ThreadLoopTestObject *obj = (ThreadLoopTestObject *)o;
    thread_timer_flag = obj->flag;
    Base::ThreadLoopStop();
}
TEST(ThreadLoopTest, Timer)
{
    ThreadLoopTestObject *obj = new ThreadLoopTestObject;
    obj->flag = 42;
    void *timer = Base::ThreadLoopSetTimer(500, thread_timer_invoke, obj);
    ASSERT_NE((void *)0, timer);
    Base::ThreadLoopRun();
    ASSERT_TRUE(Base::ThreadLoopCancelTimer(timer));
    obj->release();
    ASSERT_EQ(42, thread_timer_flag);
}
