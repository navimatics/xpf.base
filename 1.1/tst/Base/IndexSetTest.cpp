/*
 * Base/IndexSetTest.cpp
 *
 * Copyright 2010-2013 Navimatics Corporation. All rights reserved.
 */

#include <Base/IndexSet.hpp>
#include <Base/CArray.hpp>
#include <gtest/gtest.h>

TEST(IndexSetTest, AddRemoveAll)
{
    Base::Object::Mark();
    Base::IndexSet *set = new (Base::collect) Base::IndexSet;
    for (size_t i = 0; i < 100; i++)
        set->addIndex(i);
    ASSERT_EQ((size_t)100, set->count());
    set->removeAllIndexes();
    ASSERT_EQ((size_t)0, set->count());
    for (size_t i = 0; i < 100; i++)
    {
        set->addIndex(i);
        set->addIndex(i);
    }
    ASSERT_EQ((size_t)100, set->count());
    Base::Object::Collect();
}
TEST(IndexSetTest, ContainsAny)
{
    Base::Object::Mark();
    Base::IndexSet *set = new (Base::collect) Base::IndexSet;
    set->addIndex(42);
    ASSERT_EQ((size_t)1, set->count());
    ASSERT_TRUE(set->containsIndex(42));
    ASSERT_EQ(42U, set->anyIndex());
    set->removeAllIndexes();
    ASSERT_EQ((size_t)0, set->count());
    ASSERT_FALSE(set->containsIndex(42));
    ASSERT_TRUE(Base::isNone(set->anyIndex()));
    set->addIndex(43);
    ASSERT_TRUE(set->containsIndex(43));
    ASSERT_FALSE(set->containsIndex(42));
    Base::Object::Collect();
}
TEST(IndexSetTest, AddRemove)
{
    Base::Object::Mark();
    Base::IndexSet *set = new (Base::collect) Base::IndexSet;
    for (size_t i = 0; i < 100; i++)
        set->addIndex(i);
    ASSERT_EQ((size_t)100, set->count());
    set->addIndex(100);
    ASSERT_EQ((size_t)101, set->count());
    ASSERT_TRUE(set->containsIndex(100));
    set->removeIndex(100);
    ASSERT_FALSE(set->containsIndex(100));
    ASSERT_EQ((size_t)100, set->count());
    ASSERT_TRUE(set->containsIndex(42));
    set->addIndex(42);
    ASSERT_EQ((size_t)100, set->count());
    ASSERT_TRUE(set->containsIndex(42));
    set->removeIndex(42);
    ASSERT_EQ((size_t)99, set->count());
    ASSERT_FALSE(set->containsIndex(42));
    for (size_t i = 0; i < 100; i++)
        set->removeIndex(i);
    ASSERT_EQ((size_t)0, set->count());
    Base::Object::Collect();
}
TEST(IndexSetTest, Contents)
{
    Base::Object::Mark();
    Base::IndexSet *set = new (Base::collect) Base::IndexSet;
    Base::index_t to[100];
    for (size_t i = 0; i < 100; i++)
    {
        to[i] = i;
        set->addIndex(to[i]);
    }
    ASSERT_EQ((size_t)100, set->count());
    Base::index_t *idxs = set->indexes();
    ASSERT_EQ((size_t)100, Base::carr_length(idxs));
    for (size_t i = 0; i < 100; i++)
    {
        bool found = false;
        for (size_t j = 0; j < 100; j++)
            if (to[i] == idxs[j])
            {
                found = true;
                break;
            }
        ASSERT_TRUE(found);
    }
    Base::Object::Collect();
}
TEST(IndexSetTest, OrderedContents)
{
    Base::Object::Mark();
    Base::IndexSet *set = new (Base::collect) Base::IndexSet;
    for (size_t i = 0; i < 100; i++)
        set->addIndex(i);
    ASSERT_EQ((size_t)100, set->count());
    Base::index_t *idxs = set->indexes();
    ASSERT_EQ((size_t)100, Base::carr_length(idxs));
    for (size_t i = 0; i < 100; i++)
        ASSERT_TRUE(i == idxs[i]);
    Base::Object::Collect();
}
