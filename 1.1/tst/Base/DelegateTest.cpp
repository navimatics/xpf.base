/*
 * Base/DelegateTest.cpp
 *
 * Copyright 2010-2013 Navimatics Corporation. All rights reserved.
 */

#include <Base/Delegate.hpp>
#include <Base/Object.hpp>
#include <gtest/gtest.h>

static int counter = 0;

static void void_stub_void(void *)
{
    counter++;
}
static void void_stub_int(void *, int)
{
    counter++;
}
static int int_stub_int_int(void *, int, int)
{
    return counter++;
}
TEST(DelegateTest, Basic0)
{
    /* signature compile-time tests */
    {
        Base::Delegate<void ()> d1;
        Base::Delegate<Base::Delegate<void ()>::signature> d2;
        d1 = d2;
    }
    {
        Base::Delegate<int (int)> d1;
        Base::Delegate<Base::Delegate<int (int)>::signature> d2;
        d1 = d2;
    }
    {
        Base::Delegate<int (int, int)> d1;
        Base::Delegate<Base::Delegate<int (int, int)>::signature> d2;
        d1 = d2;
    }

    bool res;

    Base::Delegate<void ()> d1;
    res = d1;
    ASSERT_FALSE(res);
    res = !d1;
    ASSERT_TRUE(res);
    ASSERT_TRUE(d1 == 0);
    ASSERT_TRUE(0 == d1);
    ASSERT_FALSE(d1 != 0);
    ASSERT_FALSE(0 != d1);

    Base::Delegate<void ()> d2;
    res = d2;
    ASSERT_FALSE(res);
    res = !d2;
    ASSERT_TRUE(res);
    ASSERT_TRUE(d2 == 0);
    ASSERT_TRUE(0 == d2);
    ASSERT_FALSE(d2 != 0);
    ASSERT_FALSE(0 != d2);

    ASSERT_TRUE(d1 == d2);
    ASSERT_FALSE(d1 != d2);

    Base::Delegate<void ()> d3(&void_stub_void);
    res = d3;
    ASSERT_TRUE(res);
    res = !d3;
    ASSERT_FALSE(res);
    ASSERT_FALSE(d3 == 0);
    ASSERT_FALSE(0 == d3);
    ASSERT_TRUE(d3 != 0);
    ASSERT_TRUE(0 != d3);

    ASSERT_FALSE(d1 == d3);
    ASSERT_TRUE(d1 != d3);

    Base::Delegate<void ()> d4(d3);
    ASSERT_TRUE(d3 == d4);
    d2 = d3;
    ASSERT_TRUE(d2 == d4);
}
TEST(DelegateTest, Basic1)
{
    bool res;

    Base::Delegate<void (int)> d1;
    res = d1;
    ASSERT_FALSE(res);
    res = !d1;
    ASSERT_TRUE(res);
    ASSERT_TRUE(d1 == 0);
    ASSERT_TRUE(0 == d1);
    ASSERT_FALSE(d1 != 0);
    ASSERT_FALSE(0 != d1);

    Base::Delegate<void (int)> d2;
    res = d2;
    ASSERT_FALSE(res);
    res = !d2;
    ASSERT_TRUE(res);
    ASSERT_TRUE(d2 == 0);
    ASSERT_TRUE(0 == d2);
    ASSERT_FALSE(d2 != 0);
    ASSERT_FALSE(0 != d2);

    ASSERT_TRUE(d1 == d2);
    ASSERT_FALSE(d1 != d2);

    Base::Delegate<void (int)> d3(&void_stub_int);
    res = d3;
    ASSERT_TRUE(res);
    res = !d3;
    ASSERT_FALSE(res);
    ASSERT_FALSE(d3 == 0);
    ASSERT_FALSE(0 == d3);
    ASSERT_TRUE(d3 != 0);
    ASSERT_TRUE(0 != d3);

    ASSERT_FALSE(d1 == d3);
    ASSERT_TRUE(d1 != d3);

    Base::Delegate<void (int)> d4(d3);
    ASSERT_TRUE(d3 == d4);
    d2 = d3;
    ASSERT_TRUE(d2 == d4);
}
TEST(DelegateTest, Basic2)
{
    bool res;

    Base::Delegate<int (int, int)> d1;
    res = d1;
    ASSERT_FALSE(res);
    res = !d1;
    ASSERT_TRUE(res);
    ASSERT_TRUE(d1 == 0);
    ASSERT_TRUE(0 == d1);
    ASSERT_FALSE(d1 != 0);
    ASSERT_FALSE(0 != d1);

    Base::Delegate<int (int, int)> d2;
    res = d2;
    ASSERT_FALSE(res);
    res = !d2;
    ASSERT_TRUE(res);
    ASSERT_TRUE(d2 == 0);
    ASSERT_TRUE(0 == d2);
    ASSERT_FALSE(d2 != 0);
    ASSERT_FALSE(0 != d2);

    ASSERT_TRUE(d1 == d2);
    ASSERT_FALSE(d1 != d2);

    Base::Delegate<int (int, int)> d3(&int_stub_int_int);
    res = d3;
    ASSERT_TRUE(res);
    res = !d3;
    ASSERT_FALSE(res);
    ASSERT_FALSE(d3 == 0);
    ASSERT_FALSE(0 == d3);
    ASSERT_TRUE(d3 != 0);
    ASSERT_TRUE(0 != d3);

    ASSERT_FALSE(d1 == d3);
    ASSERT_TRUE(d1 != d3);

    Base::Delegate<int (int, int)> d4(d3);
    ASSERT_TRUE(d3 == d4);
    d2 = d3;
    ASSERT_TRUE(d2 == d4);
}

int DelegateTest_int_func_void()
{
    return counter++;
}
int DelegateTest_int_func_int(int i)
{
    EXPECT_TRUE(42 == i);
    return counter++;
}
TEST(DelegateTest, Function0)
{
    Base::Delegate<int ()> d1 = make_delegate(&DelegateTest_int_func_void);
    Base::Delegate<int ()> d2 = make_delegate(&DelegateTest_int_func_void);
    ASSERT_TRUE(d1 == d2);

    counter = 42;
    ASSERT_EQ(42, d1());
    ASSERT_EQ(43, d1());
    ASSERT_EQ(44, d2());
    ASSERT_EQ(45, d2());
}
TEST(DelegateTest, Function1)
{
    Base::Delegate<int (int)> d1 = make_delegate(&DelegateTest_int_func_int);
    Base::Delegate<int (int)> d2 = make_delegate(&DelegateTest_int_func_int);
    ASSERT_TRUE(d1 == d2);

    counter = 42;
    ASSERT_EQ(42, d1(42));
    ASSERT_EQ(43, d1(42));
    ASSERT_EQ(44, d2(42));
    ASSERT_EQ(45, d2(42));
}

struct DelegateTester
{
    int int_meth_void()
    {
        EXPECT_TRUE(4242 == data);
        return counter++;
    }
    int int_meth_int(int i)
    {
        EXPECT_TRUE(4242 == data);
        EXPECT_TRUE(42 == i);
        return counter++;
    }
    int int_meth_void_const() const
    {
        EXPECT_TRUE(4141 == data);
        return counter++;
    }
    int int_meth_int_const(int i) const
    {
        EXPECT_TRUE(4141 == data);
        EXPECT_TRUE(41 == i);
        return counter++;
    }
    int data;
};
TEST(DelegateTest, Method0)
{
    DelegateTester tester;
    tester.data = 4242;
    Base::Delegate<int ()> d1 = make_delegate(&DelegateTester::int_meth_void, &tester);
    Base::Delegate<int ()> d2 = make_delegate(&DelegateTester::int_meth_void, &tester);
    ASSERT_TRUE(d1 == d2);

    counter = 42;
    ASSERT_EQ(42, d1());
    ASSERT_EQ(43, d1());
    ASSERT_EQ(44, d2());
    ASSERT_EQ(45, d2());
}
TEST(DelegateTest, Method1)
{
    DelegateTester tester;
    tester.data = 4242;
    Base::Delegate<int (int)> d1 = make_delegate(&DelegateTester::int_meth_int, &tester);
    Base::Delegate<int (int)> d2 = make_delegate(&DelegateTester::int_meth_int, &tester);
    ASSERT_TRUE(d1 == d2);

    counter = 42;
    ASSERT_EQ(42, d1(42));
    ASSERT_EQ(43, d1(42));
    ASSERT_EQ(44, d2(42));
    ASSERT_EQ(45, d2(42));
}
TEST(DelegateTest, ConstMethod0)
{
    DelegateTester tester;
    tester.data = 4141;
    Base::Delegate<int ()> d1 = make_delegate(&DelegateTester::int_meth_void_const, &tester);
    Base::Delegate<int ()> d2 = make_delegate(&DelegateTester::int_meth_void_const, &tester);
    ASSERT_TRUE(d1 == d2);

    counter = 42;
    ASSERT_EQ(42, d1());
    ASSERT_EQ(43, d1());
    ASSERT_EQ(44, d2());
    ASSERT_EQ(45, d2());
}
TEST(DelegateTest, ConstMethod1)
{
    DelegateTester tester;
    tester.data = 4141;
    Base::Delegate<int (int)> d1 = make_delegate(&DelegateTester::int_meth_int_const, &tester);
    Base::Delegate<int (int)> d2 = make_delegate(&DelegateTester::int_meth_int_const, &tester);
    ASSERT_TRUE(d1 == d2);

    counter = 42;
    ASSERT_EQ(42, d1(41));
    ASSERT_EQ(43, d1(41));
    ASSERT_EQ(44, d2(41));
    ASSERT_EQ(45, d2(41));
}

Base::Delegate<size_t ()> getDelegateComparison1Delegate(Base::Object *obj);
Base::Delegate<size_t ()> getDelegateComparison2Delegate(Base::Object *obj);
TEST(DelegateTest, ComparisonTest)
{
    Base::Object *obj = new Base::Object;
    Base::Delegate<size_t ()> del1 = getDelegateComparison1Delegate(obj);
    Base::Delegate<size_t ()> del2 = getDelegateComparison2Delegate(obj);
    ASSERT_TRUE(del1 == del2);
    ASSERT_FALSE(del1 != del2);
    ASSERT_TRUE(del1() == del2());

    Base::Object *anotherObj = new Base::Object;
    del1 = getDelegateComparison1Delegate(obj);
    del2 = getDelegateComparison1Delegate(anotherObj);
    ASSERT_FALSE(del1 == del2);
    ASSERT_TRUE(del1 != del2);
    ASSERT_FALSE(del1() == del2());

    anotherObj->release();
    obj->release();
}
