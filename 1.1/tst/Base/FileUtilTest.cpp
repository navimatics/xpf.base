/*
 * Base/FileUtilTest.cpp
 *
 * Copyright 2010-2013 Navimatics Corporation. All rights reserved.
 */

#include <Base/FileUtil.hpp>
#include <Base/ApplicationInfo.hpp>
#include <Base/CArray.hpp>
#include <Base/Object.hpp>
#include <gtest/gtest.h>

#if defined(BASE_CONFIG_WINDOWS)
#define EXE                             ".EXE"
#elif defined(BASE_CONFIG_POSIX)
#define EXE                             ""
#endif

TEST(FileUtilTest, Basic)
{
    mark_and_collect
    {
        const char *appdir = Base::GetApplicationResourceDirectory();
        const char **paths;
        bool found;
        paths = Base::listdir(appdir);
        found = false;
        for (const char **p = paths, **endp = p + Base::carr_length(p); endp > p; p++)
        {
            if (0 == strcmp(*p, "BaseTests" EXE))
            {
                found = true;
                break;
            }
        }
        ASSERT_TRUE(found);
        paths = Base::listdir(appdir, "BaseTests*");
        found = false;
        for (const char **p = paths, **endp = p + Base::carr_length(p); endp > p; p++)
        {
            if (0 == strcmp(*p, "BaseTests" EXE))
            {
                found = true;
                break;
            }
        }
        ASSERT_TRUE(found);
        paths = Base::listdir(appdir, "DoesNotExist*");
        found = false;
        for (const char **p = paths, **endp = p + Base::carr_length(p); endp > p; p++)
        {
            if (0 == strcmp(*p, "BaseTests" EXE))
            {
                found = true;
                break;
            }
        }
        ASSERT_FALSE(found);
    }
}
